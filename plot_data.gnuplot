if (!exists("input") || input eq "") {
    input = "data.txt"
}

if (!exists("out_name") || out_name eq "") {
    out_name = input
}

if (!exists("width") || width eq "") {
    width = "800"
}

if (!exists("height") || height eq "") {
    height = "600"
}

if (!exists("terminal") || terminal eq "") {
    terminal = "png"
}

if (terminal eq "png") {
    set terminal png size @width,@height
    set output out_name.".png"
}
else {
    set terminal canvas standalone mousing
    set output out_name.".html"
}

set style fill solid

#stats input using 4:5 '%lf, %lf, %lf, [%lf,%lf]' nooutput
#min_y = STATS_min_x
#max_y = STATS_max_y
#max_abs = (abs(min_y) > abs(max_y) ? abs(min_y) : abs(max_y))
#extend = max_abs * 0.2

#set yrange [min_y - extend : max_y + extend] noextend

#plot input using 2:3:2:3:4:5 '%lf, %lf, %lf, [%lf,%lf]' with boxxyerrorbars,\
#     '' using (0.5*($2+$3)):(0.5*($4+$5)) '%lf, %lf, %lf, [%lf,%lf]' with lines title "Title:".columnhead(10)

#plot input using 2:4 '%lf, %lf, %lf, [%lf,%lf]' with lines title "Test:".columnhead(2)

stats input using 4:5 nooutput

min_y = STATS_min_x
max_y = STATS_max_y
max_abs = (abs(min_y) > abs(max_y) ? abs(min_y) : abs(max_y))
extend = max_abs * 0.2

set yrange [min_y - extend : max_y + extend] noextend

plot for [IDX=0:STATS_blocks-1] \
    input \
    index IDX \
    using 2:3:2:3:4:5 \
    with boxxyerrorbars \
    title columnheader(1)
