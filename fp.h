#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include <stdbool.h>
#include <math.h>
#include <quadmath.h>

typedef __float128 quad;

/* -------------------------------------------------------------------------- */
/* Copied from 
   http://stackoverflow.com/questions/20175148/64bit-random-number-between-a-range */
/* -------------------------------------------------------------------------- */

uint64_t urand64() 
{
  uint64_t hi = rand();
  uint64_t md = rand();
  uint64_t lo = rand();
  return (hi << 42) + (md << 21) + lo;
}

bool is_power_of_2(uint64_t x) 
{
  return x == x & -x;
}

uint64_t unsigned_uniform_random(uint64_t low, uint64_t high) {
  static const uint64_t M = ~(uint64_t)0; 
  uint64_t range = high - low;
  uint64_t to_exclude = is_power_of_2(range) ? 0
                                             : M % range + 1;
  uint64_t res;
  // Eliminate 'to_exclude' possible values from consideration.
  while ((res = urand64()) < to_exclude) {}
  return low + res % range;
}

// Requires b > a
uint64_t urand64_range(uint64_t a, uint64_t b)
{
  uint64_t range = b - a;
  return a + urand64() % range;
}


uint32_t urand32()
{
  uint32_t hi = rand() & 0xFFFF;
  uint32_t lo = rand() & 0xFFFF;
  return (hi << 16) + lo;
}

// Requires b > a
uint32_t urand32_range(uint32_t a, uint32_t b)
{
  uint32_t range = b - a;
  return a + urand32() % range;
}


/* -------------------------------------------------------------------------- */
/* Float <-> ordinal conversions (32 bits)                                    */
/* (Strictly speaking, not portable due to the strict aliasing rule)          */
/* -------------------------------------------------------------------------- */

typedef union {
  float f;
  uint32_t i;
} Float32;

// Converts a floating-point number to the corresponding raw ordinal
// (does not preserve order between positive and negative numbers)
uint32_t to_raw_ordinal_32(float f)
{
  Float32 v = { .f = f };
  return v.i;
}

// Converts a floating-point number to the corresponding ordinal
uint32_t to_ordinal_32(float f)
{
  Float32 v = { .f = f };
  uint32_t i = v.i;

  if (i & 0x80000000) {
    return 0xFFFFFFFF - i;
  }
  else {
    return i | 0x80000000;
  }
}

// Converts a raw ordinal to the corresponding floating-point number
float from_raw_ordinal_32(uint32_t i)
{
  Float32 v = { .i = i };
  return v.f;
}

// Converts an ordinal to the corresponding floating-point number
float from_ordinal_32(uint32_t i)
{
  if (i & 0x80000000) {
    Float32 v = { .i = i & ~0x80000000 };
    return v.f;
  }
  else {
    Float32 v = { .i = 0xFFFFFFFF - i };
    return v.f;
  }
}

// ULP
float ulp_32(float x)
{
  uint32_t r;
  Float32 v = { .f = x };
  
  uint32_t exp = (v.i >> 23) & 0xFF;

  if (exp > 23) {
    r = (exp - 23) << 23;
  }
  else if (exp == 0) {
    return 0x1p-149f;
  }
  else {
    uint32_t t = exp - 1;
    r = 1 << t;
  }

  Float32 out = { .i = r };
  return out.f;
}

/* -------------------------------------------------------------------------- */
/* Float <-> ordinal conversions (64 bits)                                    */
/* (Strictly speaking, not portable due to the strict aliasing rule)          */
/* -------------------------------------------------------------------------- */

typedef union {
  double f;
  uint64_t i;
} Float64;

// Converts a floating-point number to the corresponding raw ordinal
// (does not preserve order between positive and negative numbers)
uint64_t to_raw_ordinal_64(double f)
{
  Float64 v = { .f = f };
  return v.i;
}

// Converts a floating-point number to the corresponding ordinal
uint64_t to_ordinal_64(double f)
{
  Float64 v = { .f = f };
  uint64_t i = v.i;

  if (i & 0x8000000000000000ull) {
    return 0xFFFFFFFFFFFFFFFFull - i;
  }
  else {
    return i | 0x8000000000000000ull;
  }
}

// Converts a raw ordinal to the corresponding floating-point number
double from_raw_ordinal_64(uint64_t i)
{
  Float64 v = { .i = i };
  return v.f;
}

// Converts an ordinal to the corresponding floating-point number
double from_ordinal_64(uint64_t i)
{
  if (i & 0x8000000000000000ull) {
    Float64 v = { .i = i & ~0x8000000000000000ull };
    return v.f;
  }
  else {
    Float64 v = { .i = 0xFFFFFFFFFFFFFFFFull - i };
    return v.f;
  }
}

// ULP
double ulp_64(double x)
{
  uint64_t r;
  Float64 v = { .f = x };
  
  uint64_t exp = (v.i >> 52) & 0x7FF;

  if (exp > 52) {
    r = (exp - 52) << 52;
  }
  else if (exp == 0) {
    return 0x1p-1074;
  }
  else {
    uint64_t t = exp - 1;
    //    printf("t = %d\n", (int)t);
    r = 1ull << t;
    //printf("r = %lld\n", (long long)r);
  }

  Float64 out = { .i = r };
  return out.f;
}

/* -------------------------------------------------------------------------- */
/* Error computation                                                          */
/* -------------------------------------------------------------------------- */


// Absolute error 32
double abs_error_32(float low, double high)
{
  double r = (double)low;
  return fabs(r - high);
}

// Relative error 32
double rel_error_32(float low, double high)
{
  double r = (double)low;
  double d = r - high;

  if (d == 0.0) {
    return 0.0;
  }
  else {
    return fabs(d / high);
  }
}

// ULP error 32
double ulp_error_32(float low, double high)
{
  double r = (double)low;
  double d = r - high;

  if (d == 0.0) {
    return 0.0;
  }
  else {
    return fabs(d / (double)ulp_32((float)high));
  }
}

// Absolute error 64
quad abs_error_64(double low, quad high)
{
  quad r = (quad)low;
  return fabsq(r - high);
}

// Relative error 64
quad rel_error_64(double low, quad high)
{
  quad r = (quad)low;
  quad d = r - high;

  if (d == 0.0q) {
    return 0.0q;
  }
  else {
    return fabsq(d / high);
  }
}

// ULP error 64
quad ulp_error_64(double low, quad high)
{
  quad r = (quad)low;
  quad d = r - high;

  if (d == 0.0q) {
    return 0.0q;
  }
  else {
    return fabsq(d / (quad)ulp_64((double)high));
  }
}

