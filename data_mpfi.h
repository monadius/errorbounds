#ifndef __DATA_MPFI__
#define __DATA_MPFI__

#include "mp_common.h"

typedef struct t_data_args {
    double a, b;
    int segments;
} data_args;

void data_simple(FILE *out,
                 const F_MPFI f_mpfi,
                 const data_args args);

void data_adaptive(FILE *out,
                   const F_MPFI f_mpfi,
                   const data_args args);

#endif