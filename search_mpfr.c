#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "search_mpfr.h"

/* -------------------------------------------------------------------------- */
/* Copied from 
   http://stackoverflow.com/questions/20175148/64bit-random-number-between-a-range */
/* -------------------------------------------------------------------------- */

// TODO: a better random number generator is required
uint64_t urand64()
{
    uint64_t hi = rand();
    uint64_t md = rand();
    uint64_t lo = rand();
    return (hi << 42) + (md << 21) + lo;
}

int is_power_of_2(uint64_t x)
{
    return x == (x & -x);
}

uint64_t unsigned_uniform_random(uint64_t low, uint64_t high)
{
    static const uint64_t M = ~(uint64_t)0;
    uint64_t range = high - low;
    uint64_t to_exclude = is_power_of_2(range) ? 0
                                               : M % range + 1;
    uint64_t res;
    // Eliminate 'to_exclude' possible values from consideration.
    while ((res = urand64()) < to_exclude)
    {
    }
    return low + res % range;
}

// Requires b > a
uint64_t urand64_range(uint64_t a, uint64_t b)
{
    uint64_t range = b - a;
    return a + urand64() % range;
}

uint32_t urand32()
{
    uint32_t hi = rand() & 0xFFFF;
    uint32_t lo = rand() & 0xFFFF;
    return (hi << 16) + lo;
}

// Requires b > a
uint32_t urand32_range(uint32_t a, uint32_t b)
{
    uint32_t range = b - a;
    return a + urand32() % range;
}

// TODO: a better random number generator is required
// real rand_real()
// {
//     return (real)rand() / REAL_RAND_MAX;
// }

// real rand_real_range(real a, real b)
// {
//     return a + rand_real() * (b - a);
// }
void rand_range_mpfr(gmp_randstate_t state, mpfr_ptr rop, mpfr_srcptr a, mpfr_srcptr d)
{
    mpfr_urandom(rop, state, MPFR_RNDN);
    mpfr_mul(rop, rop, d, MPFR_RNDN);
    mpfr_add(rop, rop, a, MPFR_RNDN);
}

/* -------------------------------------------------------------------------- */
/* Float <-> ordinal conversions (32 bits)                                    */
/* (Strictly speaking, not portable due to the strict aliasing rule)          */
/* -------------------------------------------------------------------------- */

typedef union {
    float f;
    uint32_t i;
} Float32;

// Converts a floating-point number to the corresponding raw ordinal
// (does not preserve order between positive and negative numbers)
uint32_t to_raw_ordinal_32(float f)
{
    Float32 v = {.f = f};
    return v.i;
}

// Converts a floating-point number to the corresponding ordinal
uint32_t to_ordinal_32(float f)
{
    Float32 v = {.f = f};
    uint32_t i = v.i;

    if (i & 0x80000000)
    {
        return 0xFFFFFFFF - i;
    }
    else
    {
        return i | 0x80000000;
    }
}

// Converts a raw ordinal to the corresponding floating-point number
float from_raw_ordinal_32(uint32_t i)
{
    Float32 v = {.i = i};
    return v.f;
}

// Converts an ordinal to the corresponding floating-point number
float from_ordinal_32(uint32_t i)
{
    if (i & 0x80000000)
    {
        Float32 v = {.i = i & ~0x80000000};
        return v.f;
    }
    else
    {
        Float32 v = {.i = 0xFFFFFFFF - i};
        return v.f;
    }
}

// ULP
float ulp_32(float x)
{
    uint32_t r;
    Float32 v = {.f = x};

    uint32_t exp = (v.i >> 23) & 0xFF;

    if (exp > 23)
    {
        r = (exp - 23) << 23;
    }
    else if (exp == 0)
    {
        return 0x1p-149f;
    }
    else
    {
        uint32_t t = exp - 1;
        r = 1 << t;
    }

    Float32 out = {.i = r};
    return out.f;
}

/* -------------------------------------------------------------------------- */
/* Float <-> ordinal conversions (64 bits)                                    */
/* (Strictly speaking, not portable due to the strict aliasing rule)          */
/* -------------------------------------------------------------------------- */

typedef union {
    double f;
    uint64_t i;
} Float64;

// Converts a floating-point number to the corresponding raw ordinal
// (does not preserve order between positive and negative numbers)
uint64_t to_raw_ordinal_64(double f)
{
    Float64 v = {.f = f};
    return v.i;
}

// Converts a floating-point number to the corresponding ordinal
uint64_t to_ordinal_64(double f)
{
    Float64 v = {.f = f};
    uint64_t i = v.i;

    if (i & 0x8000000000000000ull)
    {
        return 0xFFFFFFFFFFFFFFFFull - i;
    }
    else
    {
        return i | 0x8000000000000000ull;
    }
}

// Converts a raw ordinal to the corresponding floating-point number
double from_raw_ordinal_64(uint64_t i)
{
    Float64 v = {.i = i};
    return v.f;
}

// Converts an ordinal to the corresponding floating-point number
double from_ordinal_64(uint64_t i)
{
    if (i & 0x8000000000000000ull)
    {
        Float64 v = {.i = i & ~0x8000000000000000ull};
        return v.f;
    }
    else
    {
        Float64 v = {.i = 0xFFFFFFFFFFFFFFFFull - i};
        return v.f;
    }
}

// ULP
double ulp_64(double x)
{
    uint64_t r;
    Float64 v = {.f = x};

    uint64_t exp = (v.i >> 52) & 0x7FF;

    if (exp > 52)
    {
        r = (exp - 52) << 52;
    }
    else if (exp == 0)
    {
        return 0x1p-1074;
    }
    else
    {
        uint64_t t = exp - 1;
        //    printf("t = %d\n", (int)t);
        r = 1ull << t;
        //printf("r = %lld\n", (long long)r);
    }

    Float64 out = {.i = r};
    return out.f;
}

/* -------------------------------------------------------------------------- */
/* Error computation                                                          */
/* -------------------------------------------------------------------------- */

// Absolute error 32
double abs_error_32(float low, double high)
{
    double r = (double)low;
    return fabs(r - high);
}

// Relative error 32
double rel_error_32(float low, double high)
{
    double r = (double)low;
    double d = r - high;

    if (d == 0.0)
    {
        return 0.0;
    }
    else
    {
        return fabs(d / high);
    }
}

// ULP error 32
double ulp_error_32_old(float low, double high)
{
    double r = (double)low;
    double d = r - high;

    if (d == 0.0)
    {
        return 0.0;
    }
    else
    {
        return fabs(d / (double)ulp_32((float)high));
    }
}

// ULP error 32
double ulp_error_32(float low, double high)
{
    double r = (double)low;
    double d = r - high;

    if (d == 0.0)
    {
        return 0.0;
    }
    else
    {
        int k = -149;
        if (high != 0.0) {
            frexp(high, &k);
            k = (k >= -125 ? k : -125) - 24;
        }

        return fabs(d / ldexp(1.0, k));
    }
}

// Absolute error MPFR
int abs_error_mpfr_mpfr(mpfr_ptr rop, mpfr_srcptr low, mpfr_srcptr high)
{
    int t = mpfr_sub(rop, low, high, MPFR_RNDN);
    mpfr_abs(rop, rop, MPFR_RNDN);
    return t;
}

int abs_error_mpfr(mpfr_ptr rop, double low, mpfr_srcptr high)
{
    int t = mpfr_d_sub(rop, low, high, MPFR_RNDN);
    mpfr_abs(rop, rop, MPFR_RNDN);
    return t;
}

// Relative error MPFR
int rel_error_mpfr_mpfr(mpfr_ptr rop, mpfr_srcptr low, mpfr_srcptr high)
{
    int t = mpfr_sub(rop, low, high, MPFR_RNDN);
    if (mpfr_zero_p(rop) == 0) {
        // non-zero rop
        mpfr_div(rop, rop, high, MPFR_RNDN);
    }
    mpfr_abs(rop, rop, MPFR_RNDN);
    return t;
}

int rel_error_mpfr(mpfr_ptr rop, double low, mpfr_srcptr high)
{
    int t = mpfr_d_sub(rop, low, high, MPFR_RNDN);

    if (mpfr_zero_p(rop) == 0)
    {
        // non-zero rop
        mpfr_div(rop, rop, high, MPFR_RNDN);
    }

    mpfr_abs(rop, rop, MPFR_RNDN);
    return t;
}

// Goldberg's ULP error MPFR
int goldberg_ulp_error_mpfr(mpfr_ptr rop, double low, mpfr_srcptr high)
{
    int t = mpfr_d_sub(rop, low, high, MPFR_RNDN);

    if (mpfr_zero_p(rop) == 0)
    {
        // non-zero rop
        mpfr_div_d(rop, rop, ulp_64(low), MPFR_RNDN);
    }

    mpfr_abs(rop, rop, MPFR_RNDN);
    return t;
}

// ULP error MPFR
int ulp_error_mpfr_mpfr(mpfr_ptr rop, mpfr_srcptr low, mpfr_srcptr high) {
    int t = mpfr_sub(rop, low, high, MPFR_RNDN);
    if (mpfr_zero_p(rop) == 0) {
        // non-zero rop
        mpfr_exp_t k = -1074;
        if (mpfr_zero_p(high) == 0) {
            // non-zero high
            mpfr_exp_t e = mpfr_get_exp(high) - 1;
            k = (e >= -1022 ? e : -1022) - 52;
        }
        mpfr_set_exp(rop, mpfr_get_exp(rop) - k);
    }
    mpfr_abs(rop, rop, MPFR_RNDN);
    return t;
}

int ulp_error_mpfr(mpfr_ptr rop, double low, mpfr_srcptr high) {
    int t = mpfr_d_sub(rop, low, high, MPFR_RNDN);
    if (mpfr_zero_p(rop) == 0) {
        // non-zero rop
        mpfr_exp_t k = -1074;
        if (mpfr_zero_p(high) == 0) {
            // non-zero high
            mpfr_exp_t e = mpfr_get_exp(high) - 1;
            k = (e >= -1022 ? e : -1022) - 52;
        }
        mpfr_set_exp(rop, mpfr_get_exp(rop) - k);
    }
    mpfr_abs(rop, rop, MPFR_RNDN);
    return t;
}


/* -------------------------------------------------------------------------- */
/* Exhaustive test for 32-bit values                                          */
/* -------------------------------------------------------------------------- */

void exhaustive_test_32(FILE *out, const F_32 f_32, const F_64 f_64,
                        const search_args args)
{
    const float a = (float)args.a;
    const float b = (float)args.b;

    assert(args.segments > 0);
    assert(b >= a);
    assert(f_32 && f_64);

    const uint32_t ia = to_ordinal_32(a);
    const uint32_t ib = to_ordinal_32(b);
    const float step = (b - a) / args.segments;
    uint32_t k = (ib - ia) / args.segments;

    if (k == 0)
    {
        k = 1;
    }

    double total_max_abs = -INFINITY;
    double total_max_rel = -INFINITY;
    double total_max_ulp = -INFINITY;
    float arg_total_abs = 0.0;
    float arg_total_rel = 0.0;
    float arg_total_ulp = 0.0;

    printf("exhaustive_test_32: testing from %f to %f...\n", a, b);
    printf("  segments = %d\n", args.segments);

    if (args.scale == SCALE_EXP)
    {
        printf("  exponential scale\n");
    }
    else
    {
        printf("  uniform scale\n");
    }

    uint32_t start = ia;

    for (int i = 0; i < args.segments && start <= ib; i++)
    {
        double max_abs = -INFINITY;
        double max_rel = -INFINITY;
        double max_ulp = -INFINITY;
        float arg_abs = 0.0;
        float arg_rel = 0.0;
        float arg_ulp = 0.0;

        // TODO: make sure that no overflows occur
        uint32_t end;
        if (args.scale == SCALE_EXP)
        {
            end = start + k;
        }
        else
        {
            float t = a + (i + 1) * step;
            end = to_ordinal_32(t) + 1;
        }

        if (i == args.segments - 1)
        {
            end = ib + 1;
        }

        if (end > ib + 1 || end <= start)
        {
            end = ib + 1;
        }

        for (uint32_t p = start; p < end; p++)
        {
            float x = from_ordinal_32(p);
            float low = f_32(x);
            double high = f_64((double)x);

            double abs = abs_error_32(low, high);
            double rel = rel_error_32(low, high);
            double ulp = ulp_error_32(low, high);

            if (abs > max_abs)
            {
                max_abs = abs;
                arg_abs = x;
            }

            if (rel > max_rel)
            {
                max_rel = rel;
                arg_rel = x;
            }

            if (ulp > max_ulp)
            {
                max_ulp = ulp;
                arg_ulp = x;
            }
        }

        fprintf(out, "%d, %e, %e, %e, %e, %e\n",
                i + 1, 
                from_ordinal_32(start), from_ordinal_32(end - 1),
                (double)max_abs, (double)max_rel, (double)max_ulp);

        fprintf(stderr, "\r%d/%d         ", i + 1, args.segments);

        if (max_abs > total_max_abs)
        {
            total_max_abs = max_abs;
            arg_total_abs = arg_abs;
        }

        if (max_rel > total_max_rel)
        {
            total_max_rel = max_rel;
            arg_total_rel = arg_rel;
        }

        if (max_ulp > total_max_ulp)
        {
            total_max_ulp = max_ulp;
            arg_total_ulp = arg_ulp;
        }

        start = end;
    }

    printf("done\n\n");

    printf("max_abs = %e [%a]\t at %e [%a]\n",
           total_max_abs, total_max_abs, arg_total_abs, arg_total_abs);
    printf("max_rel = %e [%a]\t at %e [%a]\n",
           total_max_rel, total_max_rel, arg_total_rel, arg_total_rel);
    printf("max_ulp = %e [%a]\t at %e [%a]\n\n",
           total_max_ulp, total_max_ulp, arg_total_ulp, arg_total_ulp);
}

/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values                                              */
/* -------------------------------------------------------------------------- */

void random_test_64(FILE *out, const F_64 f_64, const F_MPFR f_high,
                    const search_args args)
{
    const double a = args.a;
    const double b = args.b;

    assert(args.segments > 0);
    assert(args.samples > 0);
    assert(b >= a);
    assert(f_64 && f_high);

    const uint64_t ia = to_ordinal_64(a);
    const uint64_t ib = to_ordinal_64(b);
    uint64_t k = (ib - ia) / args.segments;

    const double step = (b - a) / args.segments;

    if (k == 0)
    {
        k = 1;
    }

    const int exhaustive_flag = (k <= args.samples);

    mpfr_t f_op, f_rop;
    mpfr_t r_abs, max_abs, total_max_abs;
    mpfr_t r_rel, max_rel, total_max_rel;
    mpfr_t r_ulp, max_ulp, total_max_ulp;
    mpfr_inits(f_op, f_rop, NULL);
    mpfr_inits(r_abs, max_abs, total_max_abs, NULL);
    mpfr_inits(r_rel, max_rel, total_max_rel, NULL);
    mpfr_inits(r_ulp, max_ulp, total_max_ulp, NULL);
    mpfr_set_inf(total_max_abs, -1);
    mpfr_set_inf(total_max_rel, -1);
    mpfr_set_inf(total_max_ulp, -1);

    double arg_total_abs = 0.0;
    double arg_total_rel = 0.0;
    double arg_total_ulp = 0.0;

    printf("random_test_64: testing from %f to %f...\n", a, b);
    printf("  segments = %d\n  samples = %d\n", args.segments, args.samples);

    if (exhaustive_flag)
    {
        printf(" performing an exhaustive search\n");
    }

    if (args.scale == SCALE_EXP)
    {
        printf("  exponential scale\n");
    }
    else
    {
        printf("  uniform scale\n");
    }

    uint64_t start = ia;

    for (int i = 0; i < args.segments && start <= ib; i++)
    {
        mpfr_set_inf(max_abs, -1);
        mpfr_set_inf(max_rel, -1);
        mpfr_set_inf(max_ulp, -1);
        double arg_abs = 0.0;
        double arg_rel = 0.0;
        double arg_ulp = 0.0;

        // TODO: make sure that no overflows occur
        uint64_t end;
        if (args.scale == SCALE_EXP)
        {
            end = start + k;
        }
        else
        {
            double t = a + (i + 1) * step;
            end = to_ordinal_64(t) + 1;
        }

        if (i == args.segments - 1)
        {
            end = ib + 1;
        }

        if (end > ib + 1 || end <= start)
        {
            end = ib + 1;
        }

        if (exhaustive_flag)
        {
            // Exhaustive search
            for (uint64_t p = start; p < end; p++)
            {
                double x = from_ordinal_64(p);
                double low = f_64(x);
                mpfr_set_d(f_op, x, MPFR_RNDN);
                f_high(f_rop, f_op);

                abs_error_mpfr(r_abs, low, f_rop);
                rel_error_mpfr(r_rel, low, f_rop);
                ulp_error_mpfr(r_ulp, low, f_rop);

                if (mpfr_cmp(r_abs, max_abs) > 0)
                {
                    mpfr_set(max_abs, r_abs, MPFR_RNDN);
                    arg_abs = x;
                }

                if (mpfr_cmp(r_rel, max_rel) > 0)
                {
                    mpfr_set(max_rel, r_rel, MPFR_RNDN);
                    arg_rel = x;
                }

                if (mpfr_cmp(r_ulp, max_ulp) > 0)
                {
                    mpfr_set(max_ulp, r_ulp, MPFR_RNDN);
                    arg_ulp = x;
                }
            }
        }
        else
        {
            // Random testing
            for (int t = 0; t < args.samples; t++)
            {
                uint64_t r = urand64_range(start, end);

                double x = from_ordinal_64(r);
                double low = f_64(x);
                mpfr_set_d(f_op, x, MPFR_RNDN);
                f_high(f_rop, f_op);

                abs_error_mpfr(r_abs, low, f_rop);
                rel_error_mpfr(r_rel, low, f_rop);
                ulp_error_mpfr(r_ulp, low, f_rop);

                if (mpfr_cmp(r_abs, max_abs) > 0)
                {
                    mpfr_set(max_abs, r_abs, MPFR_RNDN);
                    arg_abs = x;
                }

                if (mpfr_cmp(r_rel, max_rel) > 0)
                {
                    mpfr_set(max_rel, r_rel, MPFR_RNDN);
                    arg_rel = x;
                }

                if (mpfr_cmp(r_ulp, max_ulp) > 0)
                {
                    mpfr_set(max_ulp, r_ulp, MPFR_RNDN);
                    arg_ulp = x;
                }
            }
        }

        mpfr_fprintf(out, "%d, %e, %e, %.5RDe, %.5RDe, %.5RDe\n",
                     i + 1, 
                     from_ordinal_64(start), from_ordinal_64(end - 1),
                     max_abs, max_rel, max_ulp);

        fprintf(stderr, "\r%d/%d         ", i + 1, args.segments);

        if (mpfr_cmp(max_abs, total_max_abs) > 0)
        {
            mpfr_set(total_max_abs, max_abs, MPFR_RNDN);
            arg_total_abs = arg_abs;
        }

        if (mpfr_cmp(max_rel, total_max_rel) > 0)
        {
            mpfr_set(total_max_rel, max_rel, MPFR_RNDN);
            arg_total_rel = arg_rel;
        }

        if (mpfr_cmp(max_ulp, total_max_ulp) > 0)
        {
            mpfr_set(total_max_ulp, max_ulp, MPFR_RNDN);
            arg_total_ulp = arg_ulp;
        }

        start = end;
    }

    printf("done\n\n");

    mpfr_printf("max_abs = %.5RDe [%Ra]\t at %e [%a]\n",
                total_max_abs, total_max_abs, arg_total_abs, arg_total_abs);
    mpfr_printf("max_rel = %.5RDe [%Ra]\t at %e [%a]\n",
                total_max_rel, total_max_rel, arg_total_rel, arg_total_rel);
    mpfr_printf("max_ulp = %.5RDe [%Ra]\t at %e [%a]\n\n",
                total_max_ulp, total_max_ulp, arg_total_ulp, arg_total_ulp);

    mpfr_clears(f_op, f_rop, NULL);
    mpfr_clears(r_abs, max_abs, total_max_abs, NULL);
    mpfr_clears(r_rel, max_rel, total_max_rel, NULL);
    mpfr_clears(r_ulp, max_ulp, total_max_ulp, NULL);
}

/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values with an MPFR low precision function          */
/* -------------------------------------------------------------------------- */

void random_test_64_mpfr(FILE *out, const F_MPFR f_low, const F_MPFR f_high,
                    const search_args args)
{
    const double a = args.a;
    const double b = args.b;

    assert(args.segments > 0);
    assert(args.samples > 0);
    assert(b >= a);
    assert(f_low && f_high);

    const uint64_t ia = to_ordinal_64(a);
    const uint64_t ib = to_ordinal_64(b);
    uint64_t k = (ib - ia) / args.segments;

    const double step = (b - a) / args.segments;

    if (k == 0)
    {
        k = 1;
    }

    const int exhaustive_flag = (k <= args.samples);

    mpfr_t f_op, f_rop_low, f_rop_high;
    mpfr_t r_abs, max_abs, total_max_abs;
    mpfr_t r_rel, max_rel, total_max_rel;
    mpfr_t r_ulp, max_ulp, total_max_ulp;
    mpfr_inits(f_op, f_rop_low, f_rop_high, NULL);
    mpfr_inits(r_abs, max_abs, total_max_abs, NULL);
    mpfr_inits(r_rel, max_rel, total_max_rel, NULL);
    mpfr_inits(r_ulp, max_ulp, total_max_ulp, NULL);
    mpfr_set_inf(total_max_abs, -1);
    mpfr_set_inf(total_max_rel, -1);
    mpfr_set_inf(total_max_ulp, -1);

    double arg_total_abs = 0.0;
    double arg_total_rel = 0.0;
    double arg_total_ulp = 0.0;

    printf("random_test_64_mpfr: testing from %f to %f...\n", a, b);
    printf("  segments = %d\n  samples = %d\n", args.segments, args.samples);

    if (exhaustive_flag)
    {
        printf(" performing an exhaustive search\n");
    }

    if (args.scale == SCALE_EXP)
    {
        printf("  exponential scale\n");
    }
    else
    {
        printf("  uniform scale\n");
    }

    uint64_t start = ia;

    for (int i = 0; i < args.segments && start <= ib; i++)
    {
        mpfr_set_inf(max_abs, -1);
        mpfr_set_inf(max_rel, -1);
        mpfr_set_inf(max_ulp, -1);
        double arg_abs = 0.0;
        double arg_rel = 0.0;
        double arg_ulp = 0.0;

        // TODO: make sure that no overflows occur
        uint64_t end;
        if (args.scale == SCALE_EXP)
        {
            end = start + k;
        }
        else
        {
            double t = a + (i + 1) * step;
            end = to_ordinal_64(t) + 1;
        }

        if (i == args.segments - 1)
        {
            end = ib + 1;
        }

        if (end > ib + 1 || end <= start)
        {
            end = ib + 1;
        }

        if (exhaustive_flag)
        {
            // Exhaustive search
            for (uint64_t p = start; p < end; p++)
            {
                double x = from_ordinal_64(p);
                mpfr_set_d(f_op, x, MPFR_RNDN);
                f_low(f_rop_low, f_op);
                f_high(f_rop_high, f_op);

                abs_error_mpfr_mpfr(r_abs, f_rop_low, f_rop_high);
                rel_error_mpfr_mpfr(r_rel, f_rop_low, f_rop_high);
                ulp_error_mpfr_mpfr(r_ulp, f_rop_low, f_rop_high);

                if (mpfr_cmp(r_abs, max_abs) > 0)
                {
                    mpfr_set(max_abs, r_abs, MPFR_RNDN);
                    arg_abs = x;
                }

                if (mpfr_cmp(r_rel, max_rel) > 0)
                {
                    mpfr_set(max_rel, r_rel, MPFR_RNDN);
                    arg_rel = x;
                }

                if (mpfr_cmp(r_ulp, max_ulp) > 0)
                {
                    mpfr_set(max_ulp, r_ulp, MPFR_RNDN);
                    arg_ulp = x;
                }
            }
        }
        else
        {
            // Random testing
            for (int t = 0; t < args.samples; t++)
            {
                uint64_t r = urand64_range(start, end);

                double x = from_ordinal_64(r);
                mpfr_set_d(f_op, x, MPFR_RNDN);
                f_low(f_rop_low, f_op);
                f_high(f_rop_high, f_op);

                abs_error_mpfr_mpfr(r_abs, f_rop_low, f_rop_high);
                rel_error_mpfr_mpfr(r_rel, f_rop_low, f_rop_high);
                ulp_error_mpfr_mpfr(r_ulp, f_rop_low, f_rop_high);

                if (mpfr_cmp(r_abs, max_abs) > 0)
                {
                    mpfr_set(max_abs, r_abs, MPFR_RNDN);
                    arg_abs = x;
                }

                if (mpfr_cmp(r_rel, max_rel) > 0)
                {
                    mpfr_set(max_rel, r_rel, MPFR_RNDN);
                    arg_rel = x;
                }

                if (mpfr_cmp(r_ulp, max_ulp) > 0)
                {
                    mpfr_set(max_ulp, r_ulp, MPFR_RNDN);
                    arg_ulp = x;
                }
            }
        }

        mpfr_fprintf(out, "%d, %e, %e, %.5RDe, %.5RDe, %.5RDe\n",
                     i + 1, 
                     from_ordinal_64(start), from_ordinal_64(end - 1),
                     max_abs, max_rel, max_ulp);

        fprintf(stderr, "\r%d/%d         ", i + 1, args.segments);

        if (mpfr_cmp(max_abs, total_max_abs) > 0)
        {
            mpfr_set(total_max_abs, max_abs, MPFR_RNDN);
            arg_total_abs = arg_abs;
        }

        if (mpfr_cmp(max_rel, total_max_rel) > 0)
        {
            mpfr_set(total_max_rel, max_rel, MPFR_RNDN);
            arg_total_rel = arg_rel;
        }

        if (mpfr_cmp(max_ulp, total_max_ulp) > 0)
        {
            mpfr_set(total_max_ulp, max_ulp, MPFR_RNDN);
            arg_total_ulp = arg_ulp;
        }

        start = end;
    }

    printf("done\n\n");

    mpfr_printf("max_abs = %.5RDe [%Ra]\t at %e [%a]\n",
                total_max_abs, total_max_abs, arg_total_abs, arg_total_abs);
    mpfr_printf("max_rel = %.5RDe [%Ra]\t at %e [%a]\n",
                total_max_rel, total_max_rel, arg_total_rel, arg_total_rel);
    mpfr_printf("max_ulp = %.5RDe [%Ra]\t at %e [%a]\n\n",
                total_max_ulp, total_max_ulp, arg_total_ulp, arg_total_ulp);

    mpfr_clears(f_op, f_rop_low, f_rop_high, NULL);
    mpfr_clears(r_abs, max_abs, total_max_abs, NULL);
    mpfr_clears(r_rel, max_rel, total_max_rel, NULL);
    mpfr_clears(r_ulp, max_ulp, total_max_ulp, NULL);
}


/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values with "real" input arguments                  */
/* -------------------------------------------------------------------------- */

void random_test_64_real(FILE *out, const F_64 f_64, const F_MPFR f_high,
                         const search_args args)
{
    const double a = args.a;
    const double b = args.b;

    assert(args.segments > 0);
    assert(args.samples > 0);
    assert(b >= a);
    assert(f_64 && f_high);

    const uint64_t ia = to_ordinal_64(a);
    const uint64_t ib = to_ordinal_64(b);
    uint64_t k = (ib - ia) / args.segments;

    const double step = (b - a) / args.segments;

    if (k == 0)
    {
        k = 1;
    }

    gmp_randstate_t r_state;
    mpfr_t f_op, f_rop;
    mpfr_t r_abs, max_abs, total_max_abs, arg_total_abs, arg_abs;
    mpfr_t r_rel, max_rel, total_max_rel, arg_total_rel, arg_rel;
    mpfr_t r_ulp, max_ulp, total_max_ulp, arg_total_ulp, arg_ulp;
    mpfr_t start_val, diff_val;

    gmp_randinit_default(r_state);
    mpfr_inits(f_op, f_rop, NULL);
    mpfr_inits(start_val, diff_val, NULL);
    mpfr_inits(r_abs, max_abs, total_max_abs, arg_total_abs, arg_abs, NULL);
    mpfr_inits(r_rel, max_rel, total_max_rel, arg_total_rel, arg_rel, NULL);
    mpfr_inits(r_ulp, max_ulp, total_max_ulp, arg_total_ulp, arg_ulp, NULL);
    mpfr_set_inf(total_max_abs, -1);
    mpfr_set_inf(total_max_rel, -1);
    mpfr_set_inf(total_max_ulp, -1);
    mpfr_set_nan(arg_total_abs);
    mpfr_set_nan(arg_total_rel);
    mpfr_set_nan(arg_total_ulp);

    printf("random_test_64_real: testing from %f to %f...\n", a, b);
    printf("  segments = %d\n  samples = %d\n", args.segments, args.samples);

    if (args.scale == SCALE_EXP)
    {
        printf("  exponential scale\n");
    }
    else
    {
        printf("  uniform scale\n");
    }

    uint64_t start = ia;

    for (int i = 0; i < args.segments && start <= ib; i++)
    {
        mpfr_set_inf(max_abs, -1);
        mpfr_set_inf(max_rel, -1);
        mpfr_set_inf(max_ulp, -1);
        mpfr_set_nan(arg_abs);
        mpfr_set_nan(arg_rel);
        mpfr_set_nan(arg_ulp);

        // TODO: make sure that no overflows occur
        uint64_t end;
        if (args.scale == SCALE_EXP)
        {
            end = start + k;
        }
        else
        {
            double t = a + (i + 1) * step;
            end = to_ordinal_64(t) + 1;
        }

        if (i == args.segments - 1)
        {
            end = ib + 1;
        }

        if (end > ib + 1 || end <= start)
        {
            end = ib + 1;
        }

        // Random testing
        mpfr_set_d(start_val, from_ordinal_64(start), MPFR_RNDU);
        mpfr_d_sub(diff_val, from_ordinal_64(end), start_val, MPFR_RNDN);

        for (int t = 0; t < args.samples; t++)
        {
            rand_range_mpfr(r_state, f_op, start_val, diff_val);

            double low = f_64(mpfr_get_d(f_op, MPFR_RNDN));
            f_high(f_rop, f_op);

            abs_error_mpfr(r_abs, low, f_rop);
            rel_error_mpfr(r_rel, low, f_rop);
            ulp_error_mpfr(r_ulp, low, f_rop);

            if (mpfr_cmp(r_abs, max_abs) > 0)
            {
                mpfr_set(max_abs, r_abs, MPFR_RNDN);
                mpfr_set(arg_abs, f_op, MPFR_RNDN);
            }

            if (mpfr_cmp(r_rel, max_rel) > 0)
            {
                mpfr_set(max_rel, r_rel, MPFR_RNDN);
                mpfr_set(arg_rel, f_op, MPFR_RNDN);
            }

            if (mpfr_cmp(r_ulp, max_ulp) > 0)
            {
                mpfr_set(max_ulp, r_ulp, MPFR_RNDN);
                mpfr_set(arg_ulp, f_op, MPFR_RNDN);
            }
        }

        mpfr_fprintf(out, "%d, %e, %e, %.5RDe, %.5RDe, %.5RDe\n",
                     i + 1, 
                     from_ordinal_64(start), from_ordinal_64(end - 1),
                     max_abs, max_rel, max_ulp);

        fprintf(stderr, "\r%d/%d         ", i + 1, args.segments);

        if (mpfr_cmp(max_abs, total_max_abs) > 0)
        {
            mpfr_set(total_max_abs, max_abs, MPFR_RNDN);
            mpfr_set(arg_total_abs, arg_abs, MPFR_RNDN);
        }

        if (mpfr_cmp(max_rel, total_max_rel) > 0)
        {
            mpfr_set(total_max_rel, max_rel, MPFR_RNDN);
            mpfr_set(arg_total_rel, arg_rel, MPFR_RNDN);
        }

        if (mpfr_cmp(max_ulp, total_max_ulp) > 0)
        {
            mpfr_set(total_max_ulp, max_ulp, MPFR_RNDN);
            mpfr_set(arg_total_ulp, arg_ulp, MPFR_RNDN);
        }

        start = end;
    }

    printf("done\n\n");

    mpfr_printf("max_abs = %.5RDe [%Ra]\t at %.5Re [%Ra]\n",
                total_max_abs, total_max_abs, arg_total_abs, arg_total_abs);
    mpfr_printf("max_rel = %.5RDe [%Ra]\t at %.5Re [%Ra]\n",
                total_max_rel, total_max_rel, arg_total_rel, arg_total_rel);
    mpfr_printf("max_ulp = %.5RDe [%Ra]\t at %.5Re [%Ra]\n\n",
                total_max_ulp, total_max_ulp, arg_total_ulp, arg_total_ulp);

    gmp_randclear(r_state);
    mpfr_clears(f_op, f_rop, NULL);
    mpfr_clears(start_val, diff_val, NULL);
    mpfr_clears(r_abs, max_abs, total_max_abs, arg_total_abs, arg_abs, NULL);
    mpfr_clears(r_rel, max_rel, total_max_rel, arg_total_rel, arg_rel, NULL);
    mpfr_clears(r_ulp, max_ulp, total_max_ulp, arg_total_ulp, arg_ulp, NULL);
}

/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values with "real" input arguments and with an MPFR */
/* low precision function                                                     */
/* -------------------------------------------------------------------------- */

void random_test_64_real_mpfr(FILE *out, const F_MPFR f_low, const F_MPFR f_high,
                         const search_args args)
{
    const double a = args.a;
    const double b = args.b;

    assert(args.segments > 0);
    assert(args.samples > 0);
    assert(b >= a);
    assert(f_low && f_high);

    const uint64_t ia = to_ordinal_64(a);
    const uint64_t ib = to_ordinal_64(b);
    uint64_t k = (ib - ia) / args.segments;

    const double step = (b - a) / args.segments;

    if (k == 0)
    {
        k = 1;
    }

    gmp_randstate_t r_state;
    mpfr_t f_op, f_rop_low, f_rop_high;
    mpfr_t r_abs, max_abs, total_max_abs, arg_total_abs, arg_abs;
    mpfr_t r_rel, max_rel, total_max_rel, arg_total_rel, arg_rel;
    mpfr_t r_ulp, max_ulp, total_max_ulp, arg_total_ulp, arg_ulp;
    mpfr_t start_val, diff_val;

    gmp_randinit_default(r_state);
    mpfr_inits(f_op, f_rop_low, f_rop_high, NULL);
    mpfr_inits(start_val, diff_val, NULL);
    mpfr_inits(r_abs, max_abs, total_max_abs, arg_total_abs, arg_abs, NULL);
    mpfr_inits(r_rel, max_rel, total_max_rel, arg_total_rel, arg_rel, NULL);
    mpfr_inits(r_ulp, max_ulp, total_max_ulp, arg_total_ulp, arg_ulp, NULL);
    mpfr_set_inf(total_max_abs, -1);
    mpfr_set_inf(total_max_rel, -1);
    mpfr_set_inf(total_max_ulp, -1);
    mpfr_set_nan(arg_total_abs);
    mpfr_set_nan(arg_total_rel);
    mpfr_set_nan(arg_total_ulp);

    printf("random_test_64_real: testing from %f to %f...\n", a, b);
    printf("  segments = %d\n  samples = %d\n", args.segments, args.samples);

    if (args.scale == SCALE_EXP)
    {
        printf("  exponential scale\n");
    }
    else
    {
        printf("  uniform scale\n");
    }

    uint64_t start = ia;

    for (int i = 0; i < args.segments && start <= ib; i++)
    {
        mpfr_set_inf(max_abs, -1);
        mpfr_set_inf(max_rel, -1);
        mpfr_set_inf(max_ulp, -1);
        mpfr_set_nan(arg_abs);
        mpfr_set_nan(arg_rel);
        mpfr_set_nan(arg_ulp);

        // TODO: make sure that no overflows occur
        uint64_t end;
        if (args.scale == SCALE_EXP)
        {
            end = start + k;
        }
        else
        {
            double t = a + (i + 1) * step;
            end = to_ordinal_64(t) + 1;
        }

        if (i == args.segments - 1)
        {
            end = ib + 1;
        }

        if (end > ib + 1 || end <= start)
        {
            end = ib + 1;
        }

        // Random testing
        mpfr_set_d(start_val, from_ordinal_64(start), MPFR_RNDU);
        mpfr_d_sub(diff_val, from_ordinal_64(end), start_val, MPFR_RNDN);

        for (int t = 0; t < args.samples; t++)
        {
            rand_range_mpfr(r_state, f_op, start_val, diff_val);

            f_low(f_rop_low, f_op);
            f_high(f_rop_high, f_op);

            abs_error_mpfr_mpfr(r_abs, f_rop_low, f_rop_high);
            rel_error_mpfr_mpfr(r_rel, f_rop_low, f_rop_high);
            ulp_error_mpfr_mpfr(r_ulp, f_rop_low, f_rop_high);

            if (mpfr_cmp(r_abs, max_abs) > 0)
            {
                mpfr_set(max_abs, r_abs, MPFR_RNDN);
                mpfr_set(arg_abs, f_op, MPFR_RNDN);
            }

            if (mpfr_cmp(r_rel, max_rel) > 0)
            {
                mpfr_set(max_rel, r_rel, MPFR_RNDN);
                mpfr_set(arg_rel, f_op, MPFR_RNDN);
            }

            if (mpfr_cmp(r_ulp, max_ulp) > 0)
            {
                mpfr_set(max_ulp, r_ulp, MPFR_RNDN);
                mpfr_set(arg_ulp, f_op, MPFR_RNDN);
            }
        }

        mpfr_fprintf(out, "%d, %e, %e, %.5RDe, %.5RDe, %.5RDe\n",
                     i + 1, 
                     from_ordinal_64(start), from_ordinal_64(end - 1),
                     max_abs, max_rel, max_ulp);

        fprintf(stderr, "\r%d/%d         ", i + 1, args.segments);

        if (mpfr_cmp(max_abs, total_max_abs) > 0)
        {
            mpfr_set(total_max_abs, max_abs, MPFR_RNDN);
            mpfr_set(arg_total_abs, arg_abs, MPFR_RNDN);
        }

        if (mpfr_cmp(max_rel, total_max_rel) > 0)
        {
            mpfr_set(total_max_rel, max_rel, MPFR_RNDN);
            mpfr_set(arg_total_rel, arg_rel, MPFR_RNDN);
        }

        if (mpfr_cmp(max_ulp, total_max_ulp) > 0)
        {
            mpfr_set(total_max_ulp, max_ulp, MPFR_RNDN);
            mpfr_set(arg_total_ulp, arg_ulp, MPFR_RNDN);
        }

        start = end;
    }

    printf("done\n\n");

    mpfr_printf("max_abs = %.5RDe [%Ra]\t at %.5Re [%Ra]\n",
                total_max_abs, total_max_abs, arg_total_abs, arg_total_abs);
    mpfr_printf("max_rel = %.5RDe [%Ra]\t at %.5Re [%Ra]\n",
                total_max_rel, total_max_rel, arg_total_rel, arg_total_rel);
    mpfr_printf("max_ulp = %.5RDe [%Ra]\t at %.5Re [%Ra]\n\n",
                total_max_ulp, total_max_ulp, arg_total_ulp, arg_total_ulp);

    gmp_randclear(r_state);
    mpfr_clears(f_op, f_rop_low, f_rop_high, NULL);
    mpfr_clears(start_val, diff_val, NULL);
    mpfr_clears(r_abs, max_abs, total_max_abs, arg_total_abs, arg_abs, NULL);
    mpfr_clears(r_rel, max_rel, total_max_rel, arg_total_rel, arg_rel, NULL);
    mpfr_clears(r_ulp, max_ulp, total_max_ulp, arg_total_ulp, arg_ulp, NULL);
}
