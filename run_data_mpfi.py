#!/usr/bin/env python

import sys
import os
import argparse
import subprocess


def run(cmd, ignore_return_codes=[], stdout=None):
    print("Running: {0}".format(" ".join(cmd)))
    ret = subprocess.call(cmd, stdout=stdout)
    if ret != 0 and ret not in ignore_return_codes:
        msg = "Return code: {0}".format(ret)
        print(msg)
        sys.exit(2)
    return ret

# Global paths

base_path = os.path.normpath(os.path.dirname(os.path.normpath(sys.argv[0])))

# Parse arguments

parser = argparse.ArgumentParser(
    description="Compiles and runs data-mpfi for the given input file")

parser.add_argument('--debug', action='store_true',
                    help="debug mode")

parser.add_argument('--plot', action='store_true',
                    help="plot the output data")

parser.add_argument('--html', action='store_true',
                    help="plot the output data on an html canvas")

parser.add_argument('--width', type=int,
                    help="plot width")

parser.add_argument('--height', type=int,
                    help="plot height")

parser.add_argument('-a', '--adaptive', action='store_true',
                    help="run the adaptive algorithm")

parser.add_argument('-o', '--out',
                    help="output file name")

parser.add_argument('-p', '--prec', type=int,
                    help="default MPFI precision")

parser.add_argument('-n', '--segments', type=int,
                    help="number of subintervals")

parser.add_argument('input',
                    help="input file")

args = parser.parse_args()

if not args.out:
    args.out = "a.txt"

if args.html:
    args.plot = True

# Compile

src_files = ["data_mpfi.c", "func.c", "data_mpfi_main.c"]
src_files = [os.path.join(base_path, src) for src in src_files]
src_files += [args.input]

compile_cmd = ["gcc", "-o", "a.out", "-O3", "-std=c99", "-I" + base_path]
compile_cmd += src_files
compile_cmd += ["-lgmp", "-lmpfr", "-lmpfi"]

run(compile_cmd)

# Run

cmd = [os.path.join(".", "a.out")]
if args.out:
    cmd += ["--out", args.out]
if args.prec:
    cmd += ["--prec", str(args.prec)]
if args.adaptive:
    cmd += ["--adaptive"]
if args.segments:
    cmd += ["--segments", str(args.segments)]

run(cmd)

# Plot

if args.plot:
    cmd = ["gnuplot", "-e", "input='{0}'".format(args.out)]
    if args.html:
        cmd += ["-e", "terminal='canvas'"]
    if args.width:
        cmd += ["-e", "width='{0}'".format(args.width)]
    if args.height:
        cmd += ["-e", "height='{0}'".format(args.height)]
    cmd += [os.path.join(base_path, "plot_data.gnuplot")]
    run(cmd)