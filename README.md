Error Plots in Racket
=====================

Overview
--------

`bound.c`: The program for exhaustive or random sampling of 1d and 2d
floating-point functions.

`functions.h`, `functions2d.h`: Definitions of functions which can be used
in `bound.c`.

`racket/data.rkt`: Racket functions for loading data produced by `bound.c`.

`racket/interval.rkt`: A simple Racket interval library based on big floats.

`racket/fptaylor1d.rkt`: 1d functions (error profiles) produced by FPTaylor.

`racket/fptaylor.rkt`: 2d functions (error profiles) produced by FPTaylor.

`racket/plot.rkt`: Racket functions for 1d error plots.

`racket/plot2d.rkt`: Racket functions for 2d error plots.


Error profile plot for an existing function
-------------------------------------------

To create an error plot, several edits of source files are necessary.
Here, an example is given for a function for which no changes
to source files are necessary. The next section gives a step by step intstruction
for adding new functions.

The example below is for the function `sqrt(x + 1) - sqrt(x)` when 
`x in [1, 1000]` and it is assumed that `x` is a floating-point number.

1) In the root directory, type

    make bound

This command will compile `bound.c` and create the executable file `bound`.
The command `bound` has many arguments. Type `./bound --help` to get a help
message with a description of possible arguments.

Examples:

    ./bound --scale=uniform -n 200 -a 1.0 -b 1000.0 -o data/sqrt_sub.txt sqrt_sub

This command splits the input interval [1, 1000] into 200
subintervals (uniformly with respect to real numbers), exhaustively
tests all single precision values inside each subinterval,
and saves error results in `data/sqrt_sub.txt`.

    ./bound --scale=uniform -d -n 200 -s 100000 -a 1.0 -b 1000.0 -o data/sqrt_sub_double.txt sqrt_sub

This command splits the input interval [1, 1000] into 200
subintervals (uniformly with respect to real numbers), randomly
tests double precision values inside each subinterval with
100000 uniform random samples, 
and saves error results in `data/sqrt_sub_double.txt`.


2) Type

    make sqrt-sub-real

This command will create the file `data/sqrt_sub_double_real.txt` with
error results.


3) Open `racket/plot.rkt` in DrRacket, press `F5` and type

    (sqrt-sub-double-real)

This command will plot test results (obtained at Step 2) along with
FPTaylor error profiles (which are precomputed and defined in
`racket/fptaylor1d.rkt`). Both absolute and relative error plots will
be created.


Adding a new function (1d case)
-------------------------------

Suppose we want to plot the error profile (both test results and FPTaylor results)
of `(x^2 - 1) / (x - 1)` when `x in [1.01, 2]`.

1) Edit `functions.h` and add the following lines

    /* ----------------------------------------------------------------------- */
    /* nonlin2                                                                 */
    /* ----------------------------------------------------------------------- */

    float nonlin2_32(float x)
    {
      return (x * x - 1.0f) / (x - 1.0f);
    }

    double nonlin2_64(double x)
    {
      return (x * x - 1.0) / (x - 1.0);
    }

    quad nonlin2_128(quad x)
    {
      return (x * x - 1.0q) / (x - 1.0q);
    }

2) Open `bound.c`, find the function `find_function` and add the following
lines after the first `if` statement:

    else if (strcmp(name, "nonlin2") == 0) {
      args->f_32 = nonlin2_32;
      args->f_64 = nonlin2_64;
      args->f_128 = nonlin2_128;
    }

3) Recompile `bound` (`make bound`).

4) Run the command

    ./bound --scale=uniform -d -r -n 200 -s 100000 -a 1.01 -b 2.0 -o data/nonlin2_double_real.txt nonlin2

5) Now you can plot test data (without FPTaylor error profiles): Open `racket/plot.rkt` in DrRacket, press `F5` and type

    (plot-data "nonlin2_double_real.txt" 'abs)
    (plot-data "nonlin2_double_real.txt" 'rel)

6) Create a file `nonlin2.txt` in `FPTaylor/benchmarks` (or in another place, but
the next commands will assume this location):

    Variables
      x in [1.01, 2];

    Expressions
      nonlin2 rnd64= (x * x - 1) / (x - 1);

7) Run FPTaylor (in `FPTaylor/benchmarks`):

    ../fptaylor -c micro2/config-abs-tol.cfg nonlin2.txt
    ../fptaylor -c micro2/config-rel-tol.cfg nonlin2.txt
	
(Make sure that you use the latest development ('develop' branch) version of FPTaylor.)

8) Open file `FPTaylor/benchmarks/tmp/abs_exact.rkt` and copy its content
to `ErrorBounds/racket/fptaylor1d.rkt`. You have the following lines:

    (define eps ...)
    (define total2 ...)
    (define (fptaylor-abs x-var) ...)

Edit these lines as follows:

    (define (fptaylor-abs-double-real-nonlin2 x)
       (define eps ...)
       (define total2 ...)
       (define (fptaylor-abs x-var) ...)
       (i+ (i* eps (fptaylor-abs x)) total2))

(All `...` should contain corresponding expressions from `abs_exact.rkt`.)

Follow the same steps for the file 
`FPTaylor/benchmarks/tmp/rel_exact.rkt`. The new function in `fptaylor1d.rkt` should be

    (define (fptaylor-rel-double-real-nonlin2 x)
       (define eps ...)
       (define total2 ...)
       (define (fptaylor-rel x-var) ...)
       (i+ (i* eps (fptaylor-rel x)) total2))

9) Add the following definition to `ErrorBounds/racket/plot.rkt`
(after the definition of `create-plot`):

    (define (nonlin2-double-real)
      ; plots
      (values
       (create-plot "nonlin2_double_real.txt" 'abs 
                1.01 2.0
                (list [list fptaylor-abs-double-real-nonlin2
                            1 "FPTaylor"])
                "nonlin2_abs_double_real.png"
                #:data-style 'rectangles
                #:data-label "Test results"
                #:legend-anchor 'top-left
                #:title "Double precision absolute error of (x^2 - 1) / (x - 1) for real x in [1.01, 2]")
       (create-plot "nonlin2_double_real.txt" 'rel
                1.01 2.0
                (list [list fptaylor-rel-double-real-nonlin2
                            1 "FPTaylor"])
                "nonlin2_rel_double_real.png"
                #:data-style 'rectangles
                #:data-label "Test results"
                #:legend-anchor 'top-left
                #:title "Double precision relative error of (x^2 - 1) / (x - 1) for real x in [1.01, 2]")))


10) The final plots can be produced by opening `plot.rkt` in DrRacket,
pressing `F5`, and typing

    (nonlin2-double-real)

Plotted images will be displayed and saved in `racket/images`.


Automatization of the process described above should be not difficult.
It is necessary to write Racket code for invoking both `bound` and `FPTaylor`
for automatically generated input files. Then it is possible to load
FPTaylor results dynamically and plot error profiles.