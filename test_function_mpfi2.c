#include <math.h>
#include "search_mpfi.h"

double low[] = {0.9};
double high[] = {1.0};
const char *f_name = "Test mpfi error data (2)";

static mpfi_t _r1;
static float f_c1;
static double d_c1;
static mpfi_t c1;

void f_init()
{
    // Init all constants and temporary variables here
    mpfi_init(_r1);
    mpfi_init(c1);
    init_constants("1/10", MPFR_RNDN, &f_c1, &d_c1, c1);
}

void f_clear()
{
    // Clear all constants and temporary variables here
    mpfi_clear(_r1);
    mpfi_clear(c1);
}

float f_32(float t)
{
    float r1 = t * f_c1;
    return r1;
}

double f_64(double t)
{
    double r1 = t * d_c1;
    return r1;
}

void f_high(mpfi_t rop, mpfi_t op)
{
    mpfi_mul(_r1, op, c1);
    mpfi_set(rop, _r1);
}