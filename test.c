#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <mpfr.h>
#include "func.h"

typedef long double quad;

const mpfr_prec_t PREC = 1000;

void init_constants(const char *rat_const_str,
                    mpfr_rnd_t rnd,
                    float *float_const, 
                    double *double_const,
                    mpfr_t mpfr_const)
{
    mpfr_exp_t old_emin, old_emax;

    old_emin = mpfr_get_emin();
    old_emax = mpfr_get_emax();

    mpq_t q;
    mpq_init(q);
    mpq_set_str(q, rat_const_str, 10);
    mpq_canonicalize(q);

    // mpfr
    mpfr_set_q(mpfr_const, q, rnd);

    // double
    MPFR_DECL_INIT(d_tmp, 53);
    mpfr_set_emin(-1073);
    mpfr_set_emax(1024);

    int t = mpfr_set_q(d_tmp, q, rnd);
    mpfr_subnormalize(d_tmp, t, rnd);
    *double_const = mpfr_get_d(d_tmp, rnd);

    // single
    MPFR_DECL_INIT(s_tmp, 24);
    mpfr_set_emin(-148);
    mpfr_set_emax(128);

    t = mpfr_set_q(s_tmp, q, rnd);
    mpfr_subnormalize(s_tmp, t, rnd);
    *float_const = mpfr_get_flt(s_tmp, rnd);

    // clean
    mpq_clear(q);
    mpfr_set_emin(old_emin);
    mpfr_set_emax(old_emax);

    // Test code
    MPFR_DECL_INIT(x, PREC);
    mpfr_set_d(x, *double_const, rnd);
    if (mpfr_cmp(x, d_tmp) != 0) {
        printf("[ERROR]: x != d_tmp\n");
    }

    mpfr_set_flt(x, *float_const, rnd);
    if (mpfr_cmp(x, s_tmp) != 0) {
        printf("[ERROR]: x != s_tmp\n");
    }
}

void test_constants()
{
    printf("\n");

    double cd;
    float cf;
    MPFR_DECL_INIT(c, PREC);

    init_constants("24249454", MPFR_RNDN, &cf, &cd, c);
    mpfr_printf("cf = %.5e\ncd = %.5e\nc = %.5Re\n", cf, cd, c);
    mpfr_printf("cf = %a\ncd = %a\nc = %Ra\n\n", cf, cd, c);

    init_constants("1/10", MPFR_RNDN, &cf, &cd, c);
    mpfr_printf("cf = %.5e\ncd = %.5e\nc = %.5Re\n", cf, cd, c);
    mpfr_printf("cf = %a\ncd = %a\nc = %Ra\n\n", cf, cd, c);

    init_constants("1/1024", MPFR_RNDN, &cf, &cd, c);
    mpfr_printf("cf = %.5e\ncd = %.5e\nc = %.5Re\n", cf, cd, c);
    mpfr_printf("cf = %a\ncd = %a\nc = %Ra\n\n", cf, cd, c);

    init_constants("1/713623846352979940529142984724747568191373312", MPFR_RNDN, &cf, &cd, c);
    mpfr_printf("cf = %.5e\ncd = %.5e\nc = %.5Re\n", cf, cd, c);
    mpfr_printf("cf = %a\ncd = %a\nc = %Ra\n\n", cf, cd, c);

    init_constants("1/713623846352979940529142984724747568191373313", MPFR_RNDN, &cf, &cd, c);
    mpfr_printf("cf = %.5e\ncd = %.5e\nc = %.5Re\n", cf, cd, c);
    mpfr_printf("cf = %a\ncd = %a\nc = %Ra\n\n", cf, cd, c);

    init_constants("1/1427247692705959881058285969449495136382746624", MPFR_RNDN, &cf, &cd, c);
    mpfr_printf("cf = %.5e\ncd = %.5e\nc = %.5Re\n", cf, cd, c);
    mpfr_printf("cf = %a\ncd = %a\nc = %Ra\n\n", cf, cd, c);

    char buf[200];

    for (int i = 0; i < 100000; i++) {
        int a = rand() % 0xFFFFF;
        int b = rand() % 0xFFFFF;
        double x = (double)a / (double)b;
        float y = (float)a / (float)b;

        snprintf(buf, sizeof(buf) / sizeof(char), "%d/%d", a, b);

        init_constants(buf, MPFR_RNDN, &cf, &cd, c);
        if (cf != y) {
            printf("[ERROR]: cf != y (a = %d, b = %d) (buf = %s)\n", a, b, buf);
            printf("y =  %.5e (%a)\n", y, y);
            printf("cf = %.5e (%a)\n", cf, cf);
        }
        if (cd != x) {
            printf("[ERROR]: cd != x (a = %d, b = %d)\n", a, b);
        }
    }
}

void rand_range_mpfr(gmp_randstate_t state, mpfr_t rop, mpfr_t a, mpfr_t d)
{
    mpfr_urandom(rop, state, MPFR_RNDN);
    mpfr_mul(rop, rop, d, MPFR_RNDN);
    mpfr_add(rop, rop, a, MPFR_RNDN);
}

void sin_error()
{
    printf("\n");

    const double t = 0x1.a03f5e8454af4p-1;
    double v = sin(t);
    mpfr_t rop, op, d, err;
    mpfr_inits(rop, op, d, err, NULL);

    mpfr_set_d(op, t, MPFR_RNDN);
    mpfr_sin(rop, op, MPFR_RNDN);

    mpfr_printf("v   = %.20e\n", v);
    mpfr_printf("rop = %.20Re\n", rop);

    mpfr_sub_d(d, rop, v, MPFR_RNDN);
    mpfr_printf("rop - v = %.5Re\n", d);
    
    mpfr_div(err, d, rop, MPFR_RNDN);
    mpfr_printf("(rop - v) / rop = %.5Re\n", err);

    mpfr_div_d(err, d, v, MPFR_RNDN);
    mpfr_printf("(rop - v) / v = %.5Re\n", err);

    mpfr_clears(rop, op, d, err, NULL);
}

void rnd_test()
{
    printf("\n");

    gmp_randstate_t state;
    gmp_randinit_default(state);

    mpfr_t a, b, d, r;
    mpfr_inits(a, b, d, r, NULL);

    mpfr_set_d(a, 0.3, MPFR_RNDN);
    mpfr_set_d(b, 0.8, MPFR_RNDN);
    mpfr_sub(d, b, a, MPFR_RNDN);

    for (int i = 0; i < 10; i++) {
        rand_range_mpfr(state, r, a, d);
        mpfr_printf("%.5Re\n", r);
    }

    mpfr_clears(a, b, d, r, NULL);
    gmp_randclear(state);
}

void test_exp()
{
    mpfr_t x;
    mpfr_inits(x, NULL);
    
    mpfr_set_d(x, 1.0, MPFR_RNDN);
    mpfr_printf("e of %.5Re = %ld\n", x, mpfr_get_exp(x));
    mpfr_set_d(x, 2.0, MPFR_RNDN);
    mpfr_printf("e of %.5Re = %ld\n", x, mpfr_get_exp(x));
    mpfr_set_d(x, -0.9, MPFR_RNDN);
    mpfr_printf("e of %.5Re = %ld\n", x, mpfr_get_exp(x));
    mpfr_set_d(x, 1.001, MPFR_RNDN);
    mpfr_printf("e of %.5Re = %ld\n", x, mpfr_get_exp(x));
    mpfr_set_d(x, -1.001, MPFR_RNDN);
    mpfr_printf("e of %.5Re = %ld\n", x, mpfr_get_exp(x));

    gmp_randstate_t state;
    gmp_randinit_default(state);
    mpfr_t a, b, d, r;
    mpfr_inits(a, b, d, r, NULL);

    mpfr_set_d(a, -5, MPFR_RNDN);
    mpfr_set_d(b, 5, MPFR_RNDN);
    mpfr_sub(d, b, a, MPFR_RNDN);

    for (int i = 0; i < 10; i++) {
        rand_range_mpfr(state, r, a, d);
        mpfr_printf("e of %.5Re = %ld\n", r, mpfr_get_exp(r));
    }

    mpfr_clears(x, NULL);
    mpfr_clears(a, b, d, r, NULL);
    gmp_randclear(state);
}

#define MPFR_TEST_d1(name, f, rop, op, val, result) { \
        mpfr_set_d(op, val, MPFR_RNDN); \
        f(rop, op); \
        mpfr_set_d(op, result, MPFR_RNDN); \
        if (mpfr_cmp(rop, op) != 0) { \
            mpfr_printf("FAILED TEST: %s, val = %a, expected = %a, result = %Ra\n", \
                        name, (double)(val), (double)(result), rop); \
        } \
    }

#define MPFR_TEST_d2(name, f, rop, op1, op2, val1, val2, result) { \
        mpfr_set_d(op1, val1, MPFR_RNDN); \
        mpfr_set_d(op2, val2, MPFR_RNDN); \
        f(rop, op1, op2); \
        mpfr_set_d(op1, result, MPFR_RNDN); \
        if (mpfr_cmp(rop, op1) != 0) { \
            mpfr_printf("FAILED TEST: %s, val1 = %a, val2 = %a, expected = %a, result = %Ra\n", \
                        name, (double)(val1), (double)(val2), (double)(result), rop); \
        } \
    }

void test_floor_power2()
{
    printf("\nTesting floor_power2\n");

    mpfr_t a, b;
    mpfr_inits(a, b, NULL);

    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 0, 0);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 1, 0.5);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 2, 1);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 1.5, 1);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 1.00001, 1);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 0.50001, 0.5);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 0.5, 0.25);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, -1, -0.5);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, -453.4345, -256);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 0x1p-100, 0x1p-101);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, 0x1.1p-1000, 0x1p-1000);
    MPFR_TEST_d1("mpfr_p2", mpfr_floor_power2, a, b, -0x1p-1073, -0x1p-1074);

    mpfr_clears(a, b, NULL);

    printf("done\n");
}

int goldberg_fix(mpfr_ptr rop, mpfr_srcptr op)
{
    return mpfr_goldberg_ulp(rop, 53, -1022, op);
}

void test_goldberg_ulp()
{
    printf("\nTesting goldberg_ulp\n");

    mpfr_t a, b;
    mpfr_inits(a, b, NULL);

    const double eps = 0x1p-52;

    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 0, 0);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 1, 1 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 2, 2 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 1.5, 1 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 1.00001, 1 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 0.50001, 0.5 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 0.5, 0.5 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, -1, -1.0 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, -453.4345, -256 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 0x1p-100, 0x1p-100 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 0x1.1p-1000, 0x1p-1000 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, -0x1p-1022, -0x1p-1022 * eps);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, 0x1p-1023, 0x1p-1074);
    MPFR_TEST_d1("mpfr_g_ulp", goldberg_fix, a, b, -0x1p-1073, -0x1p-1074);

    mpfr_clears(a, b, NULL);

    printf("done\n");
}

int sub2_fix(mpfr_ptr rop, mpfr_srcptr op1, mpfr_srcptr op2)
{
    return mpfr_sub2(rop, op1, op2, MPFR_RNDN);
}

void test_sub2()
{
    printf("\nTesting sub2\n");

    mpfr_t a, b, c;
    mpfr_inits(a, b, c, NULL);

    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, 0, 0, 0);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, 1, 1, 0);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, 1, 2, 0);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, 1, 2.5, -1.5);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, 0.1, 0.1 / 2, 0);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, -0.1, -0.1 / 2, 0);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, 0.5, 0.125, 0.375);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, -0.5, -0.125, -0.375);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, -0.5, 0.125, -0.625);
    MPFR_TEST_d2("mpfr_sub2", sub2_fix, a, b, c, 0.5, -0.125, 0.625);

    mpfr_clears(a, b, c, NULL);

    printf("done\n");
}

int main()
{
    printf("\nMPFR library: %-12s\n", mpfr_get_version());
    printf("emin = %ld\nemax = %ld\n", mpfr_get_emin(), mpfr_get_emax());

    quad t = sinl(1.0);
    quad t2 = sin(1.0);
    printf("%La\n", t);
    printf("%La\n", t2);

    printf("sizeof(quad) = %lu\n", sizeof(quad));
    printf("sizeof(mpfr_t) = %ld\n", sizeof(mpfr_t));
    printf("sizeof(mpfi_t) = %ld\n", sizeof(mpfi_t));

    mpfr_set_default_prec(PREC);

    // sin_error();
    // rnd_test();
    // test_constants();

    // test_exp();

    test_floor_power2();
    test_goldberg_ulp();
    test_sub2();

    return 0;
}