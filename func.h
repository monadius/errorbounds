#ifndef __MPFI_FUNC__
#define __MPFI_FUNC__

#include <mpfi.h>

int mpfr_floor_power2(mpfr_ptr rop, mpfr_srcptr op);

int mpfr_goldberg_ulp(mpfr_ptr rop, int prec, int e_min, mpfr_srcptr op);

int mpfr_sub2(mpfr_ptr rop, mpfr_srcptr op1, mpfr_srcptr op2, mpfr_rnd_t rnd);

void mpfi_floor_power2(mpfi_ptr rop, mpfi_srcptr op);

void mpfi_goldberg_ulp(mpfi_ptr rop, int prec, int e_min, mpfi_srcptr op);

void mpfi_max(mpfi_ptr rop, mpfi_srcptr op1, mpfi_srcptr op2);

void mpfi_min(mpfi_ptr rop, mpfi_srcptr op1, mpfi_srcptr op2);

void mpfi_sub2(mpfi_ptr rop, mpfi_srcptr op1, mpfi_srcptr op2);

void mpfi_pow_ui(mpfi_ptr rop, mpfi_srcptr op1, unsigned long int op2);

#endif