#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "data_mpfi.h"

extern int g_debug_flag;

void data_simple(FILE *out,
                 const F_MPFI f_mpfi,
                 const data_args args)
{
    assert(f_mpfi);
    assert(args.b >= args.a);
    assert(args.segments > 0);

    mpfi_t f_op, f_rop;
    mpfr_t a, b, x0, x1, step;

    mpfi_init(f_op); mpfi_init(f_rop);
    mpfr_inits(a, b, x0, x1, step, NULL);

    mpfr_set_d(a, args.a, MPFR_RNDD);
    mpfr_set_d(b, args.b, MPFR_RNDU);

    mpfr_sub(step, b, a, MPFR_RNDN);
    mpfr_div_si(step, step, args.segments, MPFR_RNDN);

    mpfr_set(x1, a, MPFR_RNDN);

    for (int i = 1; i <= args.segments; i++) {
        mpfr_set(x0, x1, MPFR_RNDN);
        if (i == args.segments) {
            mpfr_set(x1, b, MPFR_RNDN);
        }
        else {
            mpfr_mul_si(x1, step, i, MPFR_RNDN);
            mpfr_add(x1, x1, a, MPFR_RNDN);
        }

        mpfi_interv_fr(f_op, x0, x1);
        f_mpfi(f_rop, f_op);

        mpfr_fprintf(out, "%d, %.10Re, %.10Re, %.10RDe, %.10RUe\n",
                     i, x0, x1, &(f_rop->left), &(f_rop->right));
    }

    mpfi_clear(f_op); mpfi_clear(f_rop);
    mpfr_clears(a, b, x0, x1, step, NULL);
}

typedef struct {
    double diam;
    mpfi_t x;
    mpfi_t f;
} t_node;

// invariants: 0 <= n <= init_n <= capacity
typedef struct t_mpfi_unordered_array {
    int n;
    int init_n;
    int capacity;
    t_node *data;
} mpfi_array;

void init_node(t_node *node)
{
    mpfi_init(node->x);
    mpfi_init(node->f);
    node->diam = 1.0 / 0.0;
}

void clear_node(t_node *node)
{
    mpfi_clear(node->x);
    mpfi_clear(node->f);
}

void init_mpfi_array(mpfi_array *array, int n, int capacity)
{
    assert(array);
    assert(n >= 0);

    if (capacity < n) {
        capacity = n;
    }

    array->data = malloc(capacity * sizeof(array->data[0]));
    if (!array->data) {
        fprintf(stderr, "FATAL ERROR: not enough memory");
        exit(1);
    }

    array->n = n;
    array->init_n = n;
    array->capacity = capacity;

    for (int i = 0; i < n; i++) {
        init_node(&(array->data[i]));
    }
}

void clear_mpfi_array(mpfi_array *array)
{
    const int k = array->init_n;
    for (int i = 0; i < k; i++) {
        clear_node(&(array->data[i]));
    }

    free(array->data);
    array->data = NULL;
    array->n = 0;
    array->init_n = 0;
    array->capacity = 0;
}

void ensure_capacity(mpfi_array *array, int size)
{
    assert (size >= 0);

    if (array->capacity < size) {
        array->data = realloc(array->data, size * sizeof(array->data[0]));
        if (!array->data) {
            fprintf(stderr, "FATAL ERROR (ensure_capacity, %d): not enough memory", size);
            exit(1);
        }

        array->capacity = size;
    }
}

void ensure_init(mpfi_array *array, int size)
{
    assert (size >= 0);

    ensure_capacity(array, size);
    if (array->init_n < size) {
        for (int i = array->init_n; i < size; i++) {
            init_node(&(array->data[i]));
        }

        array->init_n = size;
    }
}

int compare_diameters(const void *n1, const void *n2)
{
    t_node *a = (t_node *)n1;
    t_node *b = (t_node *)n2;

    if (a->diam < b->diam) {
        return 1;
    }
    else if (a->diam == b->diam) {
        return 0;
    }
    else {
        return -1;
    }
}

int compare_intervals(const void *n1, const void *n2)
{
    t_node *a = (t_node *)n1;
    t_node *b = (t_node *)n2;

    return mpfr_cmp(&(a->x->left), &(b->x->left));
}

void data_adaptive(FILE *out,
                   const F_MPFI f_mpfi,
                   const data_args args)
{
    assert(f_mpfi);
    assert(args.b >= args.a);
    assert(args.segments > 0);

    mpfr_t tmp;
    mpfi_array intervals;

    mpfr_init(tmp);
    init_mpfi_array(&intervals, 1, 2 * args.segments);

    mpfi_interv_d(intervals.data[0].x, args.a, args.b);

    int k = 0;

    while (true) {
        // Select the median tolerance
        double tol = intervals.data[intervals.n / 2].diam;
        if (intervals.data[0].diam <= tol) {
            tol = 0;
        }

        if (g_debug_flag) {
            printf("%d: tol = %e (max_diam = %e)\n", k++, tol, intervals.data[0].diam);
        }

        int i = 0;
        const int r = intervals.n;
        for (; i < r; i++) {
            t_node *node = &intervals.data[i];
            if (node->diam <= tol) {
                break;
            }

            // Bisect the interval and evaluate the function on subintervals
            const int n = intervals.n;

            ensure_init(&intervals, n + 1);
            intervals.n += 1;
            t_node *node2 = &intervals.data[n];

            mpfi_mid(tmp, node->x);
            mpfi_interv_fr(node2->x, tmp, &(node->x->right));
            mpfi_interv_fr(node->x, &(node->x->left), tmp);

            f_mpfi(node->f, node->x);
            mpfi_diam_abs(tmp, node->f);
            double d = mpfr_get_d(tmp, MPFR_RNDU);
            if (isnan(d)) {
                d = 1.0 / 0.0;
            }
            node->diam = d;

            f_mpfi(node2->f, node2->x);
            mpfi_diam_abs(tmp, node2->f);
            d = mpfr_get_d(tmp, MPFR_RNDU);
            if (isnan(d)) {
                d = 1.0 / 0.0;
            }
            node2->diam = d;
        }

        if (i == 0 || intervals.n >= args.segments) {
            break;
        }

        // Sort intervals from the largest diam to the smallest diam
        qsort(intervals.data, intervals.n, sizeof(intervals.data[0]), compare_diameters);
    }

    qsort(intervals.data, intervals.n, sizeof(intervals.data[0]), compare_intervals);

    for (int i = 0; i < intervals.n; i++) {
        t_node *node = &intervals.data[i];
        mpfr_fprintf(out, "%d, %.10Re, %.10Re, ", i, &(node->x->left), &(node->x->right));
        mpfr_fprintf(out, "%.10RDe, %.10RUe\n", &(node->f->left), &(node->f->right));
    }

    mpfr_clear(tmp);
    clear_mpfi_array(&intervals);
}