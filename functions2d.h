#include <math.h>
#include <quadmath.h>

/* -------------------------------------------------------------------------- */
/* doppler tests                                                              */
/* -------------------------------------------------------------------------- */

float a11_32(float u, float T)
{
  float r1 = 0.6f * T;
  float r2 = 331.4f + r1;
  float r3 = r2 + u;
  return 1.0f / r3;
}

double a11_64(double u, double T)
{
  double r1 = 0.6d * T;
  double r2 = 331.4d + r1;
  double r3 = r2 + u;
  return 1.0d / r3;
}

quad a11_128(quad u, quad T)
{
  quad r1 = 0.6q * T;
  quad r2 = 331.4q + r1;
  quad r3 = r2 + u;
  return 1.0q / r3;
}

/* -------------------------------------------------------------------------- */
/* pid1                                                                       */
/* -------------------------------------------------------------------------- */

float pid1_32(float c, float m)
{
  const float kp = 9.4514;
  const float ki = 0.69006;
  const float kd = 2.8454;
  const float invdt = 5.0;
  const float dt = 0.2;
  const float eold = 0;
  const float i0 = 0;

  float e = c - m;
  float p = kp * e;
  float i = i0 + ki * dt * e;
  float d = kd * invdt * (e - eold);
  float r = p + i + d;

  return r;
}

double pid1_64(double c, double m)
{
  const double kp = 9.4514;
  const double ki = 0.69006;
  const double kd = 2.8454;
  const double invdt = 5.0;
  const double dt = 0.2;
  const double eold = 0;
  const double i0 = 0;

  double e = c - m;
  double p = kp * e;
  double i = i0 + ki * dt * e;
  double d = kd * invdt * (e - eold);
  double r = p + i + d;

  return r;
}

/* -------------------------------------------------------------------------- */
/* pid2                                                                       */
/* -------------------------------------------------------------------------- */

float pid2_32(float c, float m)
{
  const float kp = 9.4514;
  const float ki = 0.69006;
  const float kd = 2.8454;
  const float invdt = 5.0;
  const float dt = 0.2;
  const float i0 = 0;
  const float eold = 0;

  float c1 = kp + kd * invdt;
  float c2 = ki * dt;
  float c3 = kd * invdt;
  float e = c - m;
  float i = i0 + c2 * e;
  float R = (c1*e) - (c3*eold);
  float r = R + i;

  return r;
}

double pid2_64(double c, double m)
{
  const double kp = 9.4514;
  const double ki = 0.69006;
  const double kd = 2.8454;
  const double invdt = 5.0;
  const double dt = 0.2;
  const double i0 = 0;
  const double eold = 0;

  double c1 = kp + kd * invdt;
  double c2 = ki * dt;
  double c3 = kd * invdt;
  double e = c - m;
  double i = i0 + c2 * e;
  double R = (c1*e) - (c3*eold);
  double r = R + i;

  return r;
}

/* -------------------------------------------------------------------------- */
/* sub                                                                        */
/* -------------------------------------------------------------------------- */

float sub_32(float x, float y)
{
  return x - y;
}

double sub_64(double x, double y)
{
  return x - y;
}

quad sub_128(quad x, quad y)
{
  return x - y;
}

/* -------------------------------------------------------------------------- */
/* hypot                                                                      */
/* -------------------------------------------------------------------------- */

float hypot_32(float x, float y)
{
  return sqrtf(x * x + y * y);
}

double hypot_64(double x, double y)
{
  return sqrt(x * x + y * y);
}

quad hypot_128(quad x, quad y)
{
  return sqrtq(x * x + y * y);
}

/* -------------------------------------------------------------------------- */
/* jet                                                                        */
/* -------------------------------------------------------------------------- */

float jet_32(float x1, float x2)
{
  float t = (3*x1*x1 + 2*x2 - x1);
  float r = x1 + ((2*x1*(t/(x1*x1 + 1)) *
	       (t/(x1*x1 + 1) - 3) + x1*x1*(4*(t/(x1*x1 + 1))-6))*
	       (x1*x1 + 1) + 3*x1*x1*(t/(x1*x1 + 1)) + x1*x1*x1 + x1 +
	       3*((3*x1*x1 + 2*x2 - x1)/(x1*x1 + 1)));
  return r;
}

double jet_64(double x1, double x2)
{
  double t = (3*x1*x1 + 2*x2 - x1);
  double r = x1 + ((2*x1*(t/(x1*x1 + 1)) *
	       (t/(x1*x1 + 1) - 3) + x1*x1*(4*(t/(x1*x1 + 1))-6))*
	       (x1*x1 + 1) + 3*x1*x1*(t/(x1*x1 + 1)) + x1*x1*x1 + x1 +
	       3*((3*x1*x1 + 2*x2 - x1)/(x1*x1 + 1)));
  return r;
}

quad jet_128(quad x1, quad x2)
{
  quad t = (3*x1*x1 + 2*x2 - x1);
  quad r = x1 + ((2*x1*(t/(x1*x1 + 1)) *
	       (t/(x1*x1 + 1) - 3) + x1*x1*(4*(t/(x1*x1 + 1))-6))*
	       (x1*x1 + 1) + 3*x1*x1*(t/(x1*x1 + 1)) + x1*x1*x1 + x1 +
	       3*((3*x1*x1 + 2*x2 - x1)/(x1*x1 + 1)));
  return r;
}
