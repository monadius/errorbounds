#include <math.h>
#include "search_mpfr.h"

double low[] = {0};
double high[] = {1.0};
const char *f_name = "Test mpfr error data";

static mpfr_t _r1;

void f_init()
{
    // Init all constants and temporary variables here
    mpfr_init(_r1);
}

void f_clear()
{
    // Clear all constants and temporary variables here
    mpfr_clear(_r1);
}

float f_32(float t)
{
    float r1 = sinf(t);
    return r1;
}

double f_64(double t)
{
    double r1 = sin(t);
    return r1;
}

void f_high(mpfr_t rop, mpfr_t op)
{
    mpfr_sin(_r1, op, MPFR_RNDN);
    mpfr_set(rop, _r1, MPFR_RNDN);
}