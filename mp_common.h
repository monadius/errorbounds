#ifndef __MP_COMMON__
#define __MP_COMMON__

#include <stdio.h>
#include <math.h>
#include <mpfr.h>
#include <mpfi.h>

typedef float (* F_32)(float);
typedef double (* F_64)(double);
typedef void (* F_MPFR)(mpfr_ptr rop, mpfr_srcptr op);
typedef void (* F_MPFI)(mpfi_ptr rop, mpfi_srcptr op);

void init_constants(const char *rat_const_str, 
                    mpfr_rnd_t rnd,
                    float *float_const, 
                    double *double_const,
                    mpfr_ptr mpfr_const,
                    mpfi_ptr mpfi_const);

#endif