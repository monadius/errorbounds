SCALE ?= uniform
EXE=./bound --scale=$(SCALE)
EXE_SINGLE=$(EXE) -n 200
EXE_DOUBLE=$(EXE) -d -n 200 -s 100000
EXE_SINGLE_2D=$(EXE) -n 40,40 -s 1000000
EXE_DOUBLE_2D=$(EXE) -d -n 40,40 -s 100000
EXE_DOUBLE_REAL=$(EXE) -d -r -n 200 -s 100000
EXE_DOUBLE_2D_REAL=$(EXE) -d -r -n 40,40 -s 100000

all: bound

bound: bound.c functions.h
	gcc -o bound -O3 -std=c99 bound.c -lm -lquadmath

data-mpfi: data_mpfi.c func.c data_mpfi_main.c test_data_mpfi.c
	gcc -o data-mpfi -O3 -std=c99 data_mpfi.c func.c data_mpfi_main.c\
		test_data_mpfi.c -lmpfi -lmpfr -lgmp

search: search.c search_main.c
	gcc -o test -O3 -std=c99 search.c search_main.c test_function2.c -lmpfr -lgmp
	#test_function.c

search_mpfr: mp_common.c search_mpfr.c search_mp_main.c test_function_mpfr2.c
	gcc -o search_mpfr -O3 -std=c99 search_mpfr.c search_mp_main.c\
		test_function_mpfr2.c -lmpfr -lgmp

search_mpfi: mp_common.c search_mpfi.c search_mp_main.c test_function_mpfi2.c
	gcc -o search_mpfi -O3 -std=c99 -DUSE_MPFI search_mpfi.c search_mp_main.c\
		test_function_mpfi2.c -lmpfi -lmpfr -lgmp

test: test.c func.c
	gcc -o test -O3 -std=c99 test.c func.c -lmpfi -lmpfr -lgmp

bound-asm: bound.c
	gcc -S -o bound.s -O3 -std=c99 bound.c

a11:
	$(EXE_DOUBLE_2D_REAL) -a -100.0,-30.0 -b 100.0,50.0 -o data/a11.txt a11

a12:
	$(EXE_DOUBLE_REAL) -a -30.0 -b 50.0 -o data/a12.txt a12

a13:
	$(EXE_DOUBLE_REAL) -a 1.0 -b 100.0 -o data/a13.txt a13

a14:
	$(EXE_DOUBLE_REAL) -a 1.0 -b 100.0 -o data/a14.txt a14

a15:
	$(EXE_DOUBLE_REAL) -a -30.0 -b 50.0 -o data/a15.txt a15

a16:
	$(EXE_DOUBLE_REAL) -a 1.0 -b 2.0 -o data/a16.txt a16

a17:
	$(EXE_DOUBLE_REAL) -a -30.0 -b 50.0 -o data/a17.txt a17

a18:
	$(EXE_DOUBLE_REAL) -a -30.0 -b 50.0 -o data/a18.txt a18

a19:
	$(EXE_DOUBLE) -a -30.0 -b 50.0 -o data/a19.txt a19

a20:
	$(EXE_DOUBLE) -a -30.0 -b 50.0 -o data/a20.txt a20

pid1:
	$(EXE_SINGLE_2D) -a -100.0,-100.0 -b 100.0,100.0 -o data/pid1.txt pid1

pid2:
	$(EXE_SINGLE_2D) -a -100.0,-100.0 -b 100.0,100.0 -o data/pid2.txt pid2

px4_1:
	$(EXE) -n 1000 -a -32768 -b 32767 -o data/px4_1.txt px4_1

px4_1_math:
	$(EXE) -n 1000 -a -32768 -b 32767 -o data/px4_1_math.txt px4_1_math

px4_1_int:
	$(EXE) -n 1000 -a -32768 -b 32767 -o data/px4_1_int.txt px4_1_int

px4_1_const:
	$(EXE) -n 1000 -a -32768 -b 32767 -o data/px4_1_const.txt px4_1_const

px4_1_const_int:
	$(EXE) -n 1000 -a -32768 -b 32767 -o data/px4_1_const_int.txt px4_1_const_int


k_log1pf:
	$(EXE_SINGLE) -a -0.1 -b 0.1 -o data/k_log1pf.txt k_log1pf

k_sin1:
	$(EXE_DOUBLE) -a -0.785 -b 0.785 -o data/k_sin1.txt k_sin1

nonlin1-real:
	$(EXE_DOUBLE_REAL) -a 1 -b 999 -o data/nonlin1_double_real.txt nonlin1

sqrt-sub-real:
	$(EXE_DOUBLE_REAL) -a 1.0 -b 1000.0 -o data/sqrt_sub_double_real.txt sqrt_sub

sqrt-add-real:
	$(EXE_DOUBLE_REAL) -a 1.0 -b 1000.0 -o data/sqrt_add_double_real.txt sqrt_add

kahan-exp-real:
	$(EXE_DOUBLE_REAL) -a 0.01 -b 0.5 -o data/kahan_exp_double_real.txt kahan_exp

kahan-exp-log-real:
	$(EXE_DOUBLE_REAL) -a 0.01 -b 0.5 -o data/kahan_exp_log_double_real.txt kahan_exp_log

jet-real:
	$(EXE_DOUBLE_2D_REAL) -a -5.0,-20.0 -b 5.0,5.0 -o data/jet_double_real.txt jet

sqroot:
	$(EXE_SINGLE) -a 0 -b 1 -o data/sqroot.txt sqroot
	$(EXE_DOUBLE) -a 0 -b 1 -o data/sqroot_double.txt sqroot

sine3:
	$(EXE_SINGLE) -a -2 -b 2 -o data/sine3.txt sine3
	$(EXE_DOUBLE) -a -2 -b 2 -o data/sine3_double.txt sine3

predPrey:
	$(EXE_DOUBLE_REAL) -a 0.1 -b 0.3 -o data/predPrey_double_real.txt predPrey

sine3-terms:
	$(EXE_SINGLE) -a 0.5 -b 3 -o data/sine3_all.txt sine3
	$(EXE_SINGLE) -a 0.5 -b 3 -o data/sine3_term1.txt sine3_term1
	$(EXE_SINGLE) -a 0.5 -b 3 -o data/sine3_term2.txt sine3_term2

sub:
	$(EXE_DOUBLE_2D) -a 0.0,0.0 -b 2.0,2.0 -o data/sub_double.txt sub

sub32:
	$(EXE_SINGLE_2D) -a 0.0,0.0 -b 2.0,2.0 -o data/sub.txt sub

hypot:
	$(EXE_DOUBLE_2D) -a 1.0,1.0 -b 100.0,100.0 -o data/hypot_double.txt hypot

hypot32:
	$(EXE_SINGLE_2D) -a 1.0,1.0 -b 100.0,100.0 -o data/hypot.txt hypot

jet:
	$(EXE_DOUBLE_2D) -a -5.0,-20.0 -b 5.0,5.0 -o data/jet_double.txt jet

jet32:
	$(EXE_SINGLE_2D) -a -5.0,-20.0 -b 5.0,5.0 -o data/jet.txt jet

exp:
	$(EXE_SINGLE) -a 0.0 -b 2.0 -o data/exp.txt exp
	$(EXE_DOUBLE) -a 0.0 -b 2.0 -o data/exp_double.txt exp

log:
	$(EXE_SINGLE) -a 0.01 -b 2.0 -o data/log.txt log
	$(EXE_DOUBLE) -a 0.01 -b 2.0 -o data/log_double.txt log

sin:
	$(EXE_SINGLE) -a 0.0 -b 6.0 -o data/sin.txt sin
	$(EXE_DOUBLE) -a 0.0 -b 6.0 -o data/sin_double.txt sin

cos:
	$(EXE_SINGLE) -a 0.0 -b 6.0 -o data/cos.txt cos
	$(EXE_DOUBLE) -a 0.0 -b 6.0 -o data/cos_double.txt cos

add1:
	$(EXE_SINGLE) -a 0.5 -b 1000.0 -o data/add1.txt add1
	$(EXE_DOUBLE) -a 0.5 -b 1000.0 -o data/add1_double.txt add1
	$(EXE_DOUBLE_REAL) -a 0.5 -b 1000.0 -o data/add1_double_real.txt add1

sqrt:
	$(EXE_SINGLE) -a 0.5 -b 1000.0 -o data/sqrt.txt sqrt
	$(EXE_DOUBLE) -a 0.5 -b 1000.0 -o data/sqrt_double.txt sqrt

sqrt-1:
	$(EXE_SINGLE) -a 0.5 -b 1000.0 -o data/sqrt_1.txt sqrt_1
	$(EXE_DOUBLE) -a 0.5 -b 1000.0 -o data/sqrt_1_double.txt sqrt_1

sqrt-sub:
	$(EXE_SINGLE) -a 1.0 -b 1000.0 -o data/sqrt_sub.txt sqrt_sub
	$(EXE_DOUBLE) -a 1.0 -b 1000.0 -o data/sqrt_sub_double.txt sqrt_sub

sqrt-add:
	$(EXE_SINGLE) -a 1.0 -b 1000.0 -o data/sqrt_add.txt sqrt_add
	$(EXE_DOUBLE) -a 1.0 -b 1000.0 -o data/sqrt_add_double.txt sqrt_add

kahan-exp:
	$(EXE_SINGLE) -a 0.01 -b 0.5 -o data/kahan_exp.txt kahan_exp
	$(EXE_DOUBLE) -a 0.01 -b 0.5 -o data/kahan_exp_double.txt kahan_exp

kahan-exp-log:
	$(EXE_SINGLE) -a 0.01 -b 0.5 -o data/kahan_exp_log.txt kahan_exp_log
	$(EXE_DOUBLE) -a 0.01 -b 0.5 -o data/kahan_exp_log_double.txt kahan_exp_log

kahan-exp-series:
	$(EXE_SINGLE) -a 0.01 -b 0.1 -o data/kahan_exp_series.txt kahan_exp_series

kahan-exp-series-4:
	$(EXE_SINGLE) -a 0.01 -b 0.1 -o data/kahan_exp_series_4.txt kahan_exp_series_4

kahan-exp-series-3-cmp:
	$(EXE_SINGLE) -a 0.01 -b 2.0 -o data/kahan_exp_series_3_cmp.txt kahan_exp_series_3_cmp

kahan-exp-series-4-cmp:
	$(EXE_SINGLE) -a 0.01 -b 2.0 -o data/kahan_exp_series_4_cmp.txt kahan_exp_series_4_cmp


clean:
	rm -f *~ bound
	find . -type f -name '*~' -delete

