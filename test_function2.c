#include <math.h>
#include <mpfr.h>
#include "search.h"

double low = -3;
double high = 3;

float f_32(float t)
{
    float r1 = sinf(t);
    return r1;
}

double f_64(double t)
{
    double r1 = sin(t);
    return r1;
}

real f_real(real t)
{
    MPFR_DECL_INIT(a, 100);
    mpfr_set_ld(a, t, MPFR_RNDN);
    mpfr_sin(a, a, MPFR_RNDN);

    real r1 = mpfr_get_ld(a, MPFR_RNDN);
//    real r1 = sinl(t);
    return r1;
}