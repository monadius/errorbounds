#include <getopt.h>
#include <stdlib.h>

#ifdef USE_MPFI
    #include "search_mpfi.h"
#else
    #include "search_mpfr.h"
#endif

extern const double low[], high[];
extern const char *f_name;

float f_32(float);
double f_64(double);
void f_init();
void f_clear();

#ifdef USE_MPFI
    void f_high(mpfi_ptr, mpfi_srcptr);
#else
    void f_low(mpfr_ptr, mpfr_srcptr);
    void f_high(mpfr_ptr, mpfr_srcptr);
#endif

int main(int argc, char *argv[])
{
    int exp_scale_flag = 0;
    int double_precision_flag = 1;
    int mpfr_input_flag = 0;
    int real_inputs_flag = 0;
    int segments = 100;
    int samples = 1000;
    mpfr_prec_t prec = 100;
    char *out_name = NULL;

    struct option long_options[] = {
        // Single precision mode
        {"single", no_argument, &double_precision_flag, 0},
        // Inputs are assumed to be real numbers
        {"real", no_argument, &real_inputs_flag, 1},
        // Use an MPFR function for computing low-precision values
        {"mpfr_input", no_argument, &mpfr_input_flag, 1},
        // Exponential scale
        {"exp", no_argument, &exp_scale_flag, 1},
        // Output file
        {"out", required_argument, 0, 'o'},
        // MPFR precision
        {"prec", required_argument, 0, 'p'},
        // Number of segements
        {"segments", required_argument, 0, 'n'},
        // Number of random samples
        {"samples", required_argument, 0, 's'},
        {0, 0, 0, 0}
    };

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "frmeo:p:n:s:", 
                            long_options, &option_index);

        if (c == -1) {
            break;
        }

        switch (c) {
            case 0: break;
            case 'f': double_precision_flag = 0; break;
            case 'r': real_inputs_flag = 1; break;
            case 'm': mpfr_input_flag = 1; break;
            case 'e': exp_scale_flag = 1; break;
            case 'o': out_name = optarg; break;
            case 'p': prec = atoi(optarg); break;
            case 'n': segments = atoi(optarg); break;
            case 's': samples = atoi(optarg); break;
            case '?': break;
            default: abort();
        }
    }

    if (prec < MPFR_PREC_MIN || prec > MPFR_PREC_MAX / 2) {
        fprintf(stderr, "Bad MPFR precision: %ld\n", prec);
        exit(2);
    }

    if (segments < 1) {
        fprintf(stderr, "Bad number of segments: %d\n", segments);
        exit(2);
    }

    if (samples < 1) {
        fprintf(stderr, "Bad number of samples: %d\n", samples);
        exit(2);
    }

    if (!out_name) {
        fprintf(stderr, "[WARNING] Output file is not specified\n");
        out_name = "data.txt";
    }

    FILE *out = fopen(out_name, "w");
    if (!out) {
        fprintf(stderr, "Bad output file: %s\n", out_name);
        exit(2);
    }

    if (f_name && f_name[0]) {
        fprintf(out, "%s\n", f_name);
    }
    
    search_args args = {
        .a = low[0],
        .b = high[0],
        .scale = exp_scale_flag ? SCALE_EXP : SCALE_UNIFORM,
        .segments = segments,
        .samples = samples
    };

    mpfr_set_default_prec(prec);
    f_init();

    if (double_precision_flag) {
        if (real_inputs_flag) {
            #ifdef USE_MPFI
                random_test_64_real(out, f_64, f_high, args);
            #else
                if (mpfr_input_flag) {
                    random_test_64_real_mpfr(out, f_low, f_high, args);
                } else {
                    random_test_64_real(out, f_64, f_high, args);
                }
            #endif
        } else {
            #ifdef USE_MPFI
                random_test_64(out, f_64, f_high, args);
            #else
                if (mpfr_input_flag) {
                    random_test_64_mpfr(out, f_low, f_high, args);
                } else {
                    random_test_64(out, f_64, f_high, args);
                }
            #endif
        }
    } else {
        #ifdef USE_MPFI
            exhaustive_test_32_mpfi(out, f_32, f_high, args);
        #else
            exhaustive_test_32(out, f_32, f_64, args);
        #endif
    }

    f_clear();
    fclose(out);

    return 0;
}