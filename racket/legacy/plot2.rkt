#lang racket

(require plot)
(require math)
(require "interval.rkt")
(require "data.rkt")

(define-values (abs rel ulp)
  (load-data "kahan_exp.txt"))

(define eps (make-interval (bfexp2 (bf -24))))

(define (mk-f f t2)
  (lambda (x) (i+ (i* eps (f x)) t2)))

; initial

(define total2-abs-simple (make-interval 2.573446e-12))

(define (fptaylor-abs-simple x-var)
	(i+ (iabs (i* (i/ (make-interval 1/1) x-var) (i* (make-interval 2/1) (iexp x-var)))) (i+ (iabs (i* (i/ (make-interval 1/1) x-var) (i- (iexp x-var) (make-interval 1/1)))) (iabs (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var))))))

(define total2-abs (make-interval 3.538238e-36))

(define (fptaylor-abs x-var)
	(i+ (iabs (i* (i/ (make-interval 1/1) x-var) (i* (make-interval 2/1) (ifloor-pow2 (i+ (iexp x-var) (sym-interval (make-interval 0/1))))))) (i+ (iabs (i* (i/ (make-interval 1/1) x-var) (ifloor-pow2 (i+ (i- (iexp x-var) (make-interval 1/1)) (sym-interval (make-interval 4503599627370497/37778931862957161709568)))))) (iabs (ifloor-pow2 (i+ (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var)) (sym-interval (make-interval 2199023255552001/147573952589676412928))))))))

; exp 1.2 coefficient

(define total2-abs-simple2 (make-interval 1.636256e-12))

(define (fptaylor-abs-simple2 x-var)
	(i+ (iabs (i* (i/ (make-interval 1/1) x-var) (i* (make-interval 5404319552844595/4503599627370496) (iexp x-var)))) (i+ (iabs (i* (i/ (make-interval 1/1) x-var) (i- (iexp x-var) (make-interval 1/1)))) (iabs (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var))))))

(define total2-abs2 (make-interval 2.597843e-36))

(define (fptaylor-abs2 x-var)
	(i+ (iabs (i* (i/ (make-interval 1/1) x-var) (i* (make-interval 5404319552844595/4503599627370496) (ifloor-pow2 (i+ (iexp x-var) (sym-interval (make-interval 0/1))))))) (i+ (iabs (i* (i/ (make-interval 1/1) x-var) (ifloor-pow2 (i+ (i- (iexp x-var) (make-interval 1/1)) (sym-interval (make-interval 1351079888211149/18889465931478580854784)))))) (iabs (ifloor-pow2 (i+ (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var)) (sym-interval (make-interval 5981343255101443/590295810358705651712))))))))

; subtraction (no rounding)

(define total2-abs-simple3 (make-interval 7.028922e-13))

(define (fptaylor-abs-simple3 x-var)
	(i+ (iabs (i* (i/ (make-interval 1/1) x-var) (i* (make-interval 5404319552844595/4503599627370496) (iexp x-var)))) (iabs (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var)))))

(define total2-abs3 (make-interval 1.422348e-36))

(define (fptaylor-abs3 x-var)
	(i+ (iabs (i* (i/ (make-interval 1/1) x-var) (i* (make-interval 5404319552844595/4503599627370496) (ifloor-pow2 (i+ (iexp x-var) (sym-interval (make-interval 0/1))))))) (iabs (ifloor-pow2 (i+ (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var)) (sym-interval (make-interval 2111062325329921/295147905179352825856)))))))

; plot

(plot (list
       (interval-function (mk-f fptaylor-abs-simple3 total2-abs-simple3) 0.01 0.5
                          #:samples 1000
                          #:width 2)
       (interval-function (mk-f fptaylor-abs3 total2-abs3) 0.01 0.5
                          #:samples 1000
                          #:width 2
                          #:line-color 3)
       (lines abs
              #:color 2
              #:width 2))
      #:y-min 0
      #:width 600)

;(define total2-rel-simple (make-interval 4.521060e-12))

;(define (fptaylor-rel-simple x-var)
;	(i+ (iabs (i/ (i* (i/ (make-interval 1/1) x-var) (i* (make-interval 2/1) (iexp x-var))) (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var)))) (i+ (iabs (i/ (i* (i/ (make-interval 1/1) x-var) (i- (iexp x-var) (make-interval 1/1))) (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var)))) (iabs (i/ (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var)) (i* (i- (iexp x-var) (make-interval 1/1)) (i/ (make-interval 1/1) x-var)))))))

;(plot (list
;       (interval-function (mk-f fptaylor-rel total2-rel) 0.01 0.5
;                          #:samples 1000
;                          #:width 2)
;       (lines rel
;              #:color 2
;              #:width 2))
;      #:y-min 0
;      #:width 600)