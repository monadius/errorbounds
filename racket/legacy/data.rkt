#lang racket

(require plot)

(provide (contract-out
          [load-data (path-string? . -> . (values number? number?
                                                  (listof (listof vector?))))] 
          [load-data-2d (path-string? . -> . (values ivl? ivl?
                                                (listof vector?)
                                                (listof vector?)
                                                (listof vector?)))])
         get-box)

(define (extract str)
  (define pat0 #rx"[0-9.e+-]+")
  (define pat #rx"(\\[[0-9.e+-]+,[0-9.e+-]+\\])|([0-9.e+-]+)")
  (for/list ([s (regexp-match* pat str)])
    (cond
      [(string-prefix? s "[")
       (match-define (list s1 s2) (regexp-match* pat0 s))
       (cons (string->number s1) (string->number s2))]
      [else (string->number s)])))

(define (unzip3/callback lst k)
  (match lst
    ['() (k '() '() '())]
    [(cons (list a b c) tl)
     (unzip3/callback tl (lambda (as bs cs)
                          (k (cons a as) (cons b bs) (cons c cs))))]))

(define (unzip3-values lst)
  (match lst
    ['() (values '() '() '())]
    [(cons (list a b c) tl)
     (define-values (as bs cs) (unzip3-values tl))
     (values (cons a as) (cons b bs) (cons c cs))]))

(define (transpose data)
  (cond
    [(null? data) data]
    [else
     (map reverse
          (for/fold ([result (map list (car data))])
                    ([row (cdr data)])
            (map cons row result)))]))

(define (load-data path)
  (define lines (file->lines path))
  (define data (map extract lines))
  (match-define (list n a b) (car data))
  (define pts
    (map (match-lambda 
           [(list i p vals ...)
            (map (curry vector p) vals)])
         (cdr data)))
  (values a b (transpose pts)))

(define (load-data-2d fname)
  (define lines (file->lines fname))
  (define data (map extract lines))
  (match-define (list nx x0 x1 ny y0 y1) (cadr data))
  (define pts (map (match-lambda
                     [(list x0 x1 y0 y1 abs rel ulp)
                      (define x (ivl x0 x1))
                      (define y (ivl y0 y1))
                      (list (vector x y (ivl 0 abs))
                            (vector x y (ivl 0 rel))
                            (vector x y (ivl 0 ulp)))])
                   (cdddr data)))
  (define-values (abs rel ulp) (unzip3-values pts))
  (values (ivl x0 x1) (ivl y0 y1) abs rel ulp))

(define (get-box rs nx ny)
  (define vs (list->vector rs))
  (match-define (vector (ivl x-min _) (ivl y-min _) _) (car rs))
  (match-define (vector (ivl _ x-max) (ivl _ y-max) _) 
    (list-ref rs (- (* nx ny) 1)))
  (define w (/ (- x-max x-min) nx))
  (define h (/ (- y-max y-min) ny))
  (λ (x y)
    (define i (min (- nx 1) (max 0 (floor (/ (- x x-min) w)))))
    (define j (min (- ny 1) (max 0 (floor (/ (- y y-min) h)))))
    (define index (inexact->exact (+ (* i nx) j)))
    (define z (vector-ref vs index))
    (ivl-max (vector-ref z 2))))
