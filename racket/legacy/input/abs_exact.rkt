(define eps (make-interval (bfexp2 (bf -53))))

(define total2 (make-interval 8791694975699897/680564733841876926926749214863536422912))

(define (fptaylor-abs t-var)
	(i+ (iabs (i+ (i* t-var
                  (i- (i/ (ifloor-pow2 t-var)
                          (i* (i+ t-var (make-interval 1))
                              (i+ t-var (make-interval 1))))))
              (i* (i/ (make-interval 1) (i+ t-var (make-interval 1)))
                  (ifloor-pow2 t-var))))
    (i+ (iabs (i* t-var
                  (i- (i/ (ifloor-pow2 (i+ (i+ t-var (make-interval 1))
                                           (make-interval -1/17592186044416
                                                          1/17592186044416)))
                          (i* (i+ t-var (make-interval 1))
                              (i+ t-var (make-interval 1)))))))
        (iabs (ifloor-pow2 (i+ (i* t-var
                                   (i/ (make-interval 1)
                                       (i+ t-var (make-interval 1))))
                               (make-interval -1098961871962237/9671406556917033397649408
                                              1098961871962237/9671406556917033397649408)))))))