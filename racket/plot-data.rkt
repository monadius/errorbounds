#lang racket

(require plot plot/utils "data.rkt")


(struct data (points style color name) #:transparent)


(define (load-data-file fname columns
                        #:color [color 1]
                        #:style [style 'rectangles])
  (define input (load-data fname columns))
  (for/list ([d input] [c (in-naturals color)])
    (data (input-data-points d) style c (input-data-name d))))


(define (points->rectangles pts)
  (define-values (low intervals intervals-flag)
    (for/fold ([low '()] [intervals '()] [flag #f])
              ([p pts])
      (match p
        [(vector x0 x1 (cons y0 y1))
         (values (cons (vector (ivl x0 x1) (ivl 0 y0)) low)
                 (cons (vector (ivl x0 x1) (ivl y0 y1)) intervals)
                 #t)]
        [(vector x0 x1 y)
         (values (cons (vector (ivl x0 x1) (ivl 0 y)) low)
                 (cons (vector (ivl x0 x1) (ivl y y)) intervals)
                 flag)])))
  (values (reverse low) (reverse intervals) intervals-flag))


(define (rectangles->lines rects [flag 'top])
  (reverse
   (for/fold ([lines '()]) ([rect rects])
     (match-define (vector (ivl x0 x1) (ivl y0 y1)) rect)
     (define y (case flag
                 [(top) y1]
                 [(bottom) y0]
                 [(center) (* 0.5 (+ y0 y1))]))
     (cons (vector x1 y) (cons (vector x0 y) lines)))))


(define (data-y-bounds d)
  (for/fold ([min-y +inf.0] [max-y -inf.0])
            ([p (data-points d)])
    (match p
      [(vector x0 x1 (cons y0 y1))
       (values (min y0 min-y) (max y1 max-y))]
      [(vector x0 x1 y)
       (values (min y min-y) (max y max-y))])))


(define (data->renderer data)
  (define-values (rects-low rects-interval interval-flag)
    (points->rectangles (data-points data)))
  (case (data-style data)
    [(stack)
     (list
      (rectangles rects-low
                  #:label (data-name data)
                  #:line-style 'transparent
                  #:style 'solid
                  #:color (data-color data))
      (if interval-flag
          (rectangles rects-interval
                      #:label (string-append "[interval]" (data-name data))
                      #:style 'solid
                      #:line-color (data-color data)
                      #:color (->pen-color (data-color data)))
          '()))]
    [(rectangles)
     (rectangles rects-interval
                 #:label (data-name data)
                 #:style 'solid
                 #:line-color (data-color data)
                 #:color (data-color data))]
    [(lines) (lines (rectangles->lines rects-low)
                    #:label (data-name data)
                    #:color (data-color data))]))


(define (create-plot ds
                     #:out-file [out-file #f]
                     #:title [title #f]
                     #:y-min [y-min #f]
                     #:y-max [y-max #f])
  (define data-renderers (map data->renderer ds))
  (define y-max*
    (if y-max y-max
        (* 1.1 (apply max (for/list ([d ds])
                            (define-values (min max) (data-y-bounds d))
                            max)))))
  (plot data-renderers
        #:y-min (if y-min y-min 0)
        #:y-max y-max*
        #:title title
        #:out-file out-file))


(module+ test
  (plot-width 600)
  (define test-data
    (append
     (load-data-file "test_data/data_mpfi.txt"
                     (list 2 3 (cons 4 5))
                     #:color 1
                     #:style 'rectangles)
     (load-data-file "test_data/search_mpfi_data.txt"
                     '(2 3 6)
                     #:color 3
                     #:style 'stack)))
  (create-plot test-data
               #:title "Test title")
  )
  

(module+ main
  (require racket/cmdline)
  (define out-file (make-parameter #f))
  (define title (make-parameter #f))
  (define y-min (make-parameter #f))
  (define y-max (make-parameter #f))
  (define single-data (make-parameter #f))
  (define input-data (make-parameter '()))
  (plot-width 800)
  (plot-height 600)
  (define (decode-cols cols)
    (define pat0 #rx"[0-9]+")
    (define pat #rx"([0-9]+\\.[0-9]+)|([0-9]+)")
    (for/list ([s (regexp-match* pat cols)])
      (cond
        [(string-contains? s ".")
         (match-define (list s1 s2) (regexp-match* pat0 s))
         (cons (string->number s1) (string->number s2))]
        [else (string->number s)])))
  (command-line
   #:program "plot-fptaylor.rkt"
   #:multi
   ["--error-data" fname err style "Input mp_search results with error type and style"
                   (define cols (case err
                                  [("abs") '(2 3 4)]
                                  [("rel") '(2 3 5)]
                                  [("ulp") '(2 3 6)]))
                   (input-data (cons (list fname cols (string->symbol style)) (input-data)))]
   ["--model-data" fname "Input data-mpfi results"
                   (input-data (cons (list fname (list 2 3 (cons 4 5)) 'rectangles) (input-data)))]
   ["--data" fname cols-str style "Input data file with column and style parameters"
             (define cols (decode-cols cols-str))
             (input-data (cons (list fname cols (string->symbol style)) (input-data)))]
   #:once-each
   ["--out" fname "Output file name"
            (out-file fname)]
   ["--single-data" "Plot the first dataset only from each input file"
                    (single-data #t)]
   ["--title" str "Plot title"
              (title str)]
   ["--y-min" y "Custom y-min value"
              (y-min (string->number y))]
   ["--y-max" y "Custom y-max value"
              (y-max (string->number y))]
   ["--legend" anchor "Placement of the legend (top-left, bottom-right, etc.)"
               (plot-legend-anchor (string->symbol anchor))]
   ["--width" w "Plot width"
              (plot-width (string->number w))]
   ["--height" h "Plot height"
               (plot-height (string->number h))]
   #:args ()
   (unless (empty? (input-data))
     (define-values (data max-color)
       (for/fold ([data '()] [c 1])
                 ([d (reverse (input-data))])
         (match-define (list fname cols style) d)
         (define ds (load-data-file fname cols #:style style #:color c))
         (if (single-data)
             (values (append data (list (car ds))) (+ c 1))
             (values (append data ds) (+ (data-color (last ds)) 1)))))
     (create-plot data
                  #:out-file (out-file)
                  #:title (title)
                  #:y-min (y-min)
                  #:y-max (y-max)))
   
  ))

