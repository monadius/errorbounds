#include <math.h>
#include <quadmath.h>

/* -------------------------------------------------------------------------- */
/* doppler tests                                                              */
/* -------------------------------------------------------------------------- */

float a12_32(float T)
{
  float r1 = T + 40.1f;
  float r2 = r1 * r1;
  return 1.0f / r2;
}

double a12_64(double T)
{
  double r1 = T + 40.1d;
  double r2 = r1 * r1;
  return 1.0d / r2;
}

quad a12_128(quad T)
{
  quad r1 = T + 40.1q;
  quad r2 = r1 * r1;
  return 1.0q / r2;
}

float a13_32(float u)
{
  return 1.0f / u;
}

double a13_64(double u)
{
  return 1.0d / u;
}

quad a13_128(quad u)
{
  return 1.0q / u;
}

float a14_32(float u)
{
  return 1.1f / u;
}

double a14_64(double u)
{
  return 1.1d / u;
}

quad a14_128(quad u)
{
  return 1.1q / u;
}

float a15_32(float T)
{
  float r1 = T + 40.1f;
  return 1.0f / r1;
}

double a15_64(double T)
{
  double r1 = T + 40.1d;
  return 1.0d / r1;
}

quad a15_128(quad T)
{
  quad r1 = T + 40.1q;
  return 1.0q / r1;
}

float a16_32(float u)
{
  return 40.1f;
}

double a16_64(double u)
{
  return 40.1d;
}

quad a16_128(quad u)
{
  return 40.1q;
}

float a17_32(float T)
{
  return 1.0f / ((T + 40.1f) * (T + 40.1f));
}

double a17_64(double T)
{
  return 1.0d / ((T + 40.1d) * (T + 40.1d));
}

quad a17_128(quad T)
{
  return 1.0q / ((T + 40.1q) * (T + 40.1q));
}

float a18_32(float T)
{
  return 1.0f / (T + 40.1f);
}

double a18_64(double T)
{
  return 1.0d / (T + 40.1d);
}

quad a18_128(quad T)
{
  return 1.0q / (T + 40.1q);
}

float a19_32(float T)
{
  return 1.0f / (T + 40.1f);
}

double a19_64(double T)
{
  return 1.0d / (T + 40.1d);
}

quad a19_128(quad T)
{
  return 1.0q / (T + 40.1q);
}

float a20_32(float T)
{
  return 1.0f / (T + 40.0f);
}

double a20_64(double T)
{
  return 1.0d / (T + 40.0d);
}

quad a20_128(quad T)
{
  return 1.0q / (T + 40.0q);
}

/* -------------------------------------------------------------------------- */
/* px4                                                                        */
/* -------------------------------------------------------------------------- */

float px4_1_32(float xraw_f)
{
  const float _accel_range_scale = 9.80665f / 4096;
  const float _accel_scale_x_offset = 0;
  const float _accel_scale_x_scale = 1;
  const float sample_freq = 1000;
  const float _cutoff_freq = 30;
  const float M_PI_F = 3.14159f;

  float fr = sample_freq / _cutoff_freq;
  float ohm = tanf(M_PI_F / fr);
  float cs = cosf(M_PI_F / 4.0);
  float c = 1.0 + 2.0 * cs * ohm + ohm * ohm;
  float _b0 = ohm * ohm / c;
  float _b1 = 2.0 * _b0;
  float _b2 = _b0;
  float _a1 = 2.0 * (ohm * ohm - 1.0) / c;
  float _a2 = (1.0 - 2.0 * cs * ohm + ohm * ohm) / c;

  float x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  float _delay_element_1 = 0;
  float _delay_element_2 = 0;

  float delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  float output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}

double px4_1_64(double xraw_f)
{
  const double _accel_range_scale = 9.80665f / 4096;
  const double _accel_scale_x_offset = 0;
  const double _accel_scale_x_scale = 1;
  const double sample_freq = 1000;
  const double _cutoff_freq = 30;
  const double M_PI_F = 3.14159f;

  double fr = sample_freq / _cutoff_freq;
  double ohm = tan(M_PI_F / fr);
  double cs = cos(M_PI_F / 4.0);
  double c = 1.0 + 2.0 * cs * ohm + ohm * ohm;
  double _b0 = ohm * ohm / c;
  double _b1 = 2.0 * _b0;
  double _b2 = _b0;
  double _a1 = 2.0 * (ohm * ohm - 1.0) / c;
  double _a2 = (1.0 - 2.0 * cs * ohm + ohm * ohm) / c;

  double x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  double _delay_element_1 = 0;
  double _delay_element_2 = 0;

  double delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  double output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}

double px4_1_math_64(double xraw_f)
{
  const double _accel_range_scale = 9.80665d / 4096;
  const double _accel_scale_x_offset = 0;
  const double _accel_scale_x_scale = 1;
  const double sample_freq = 1000;
  const double _cutoff_freq = 30;
  const double M_PI_F = 3.141592653589793238462643383279d;

  double fr = sample_freq / _cutoff_freq;
  double ohm = tan(M_PI_F / fr);
  double cs = cos(M_PI_F / 4.0);
  double c = 1.0 + 2.0 * cs * ohm + ohm * ohm;
  double _b0 = ohm * ohm / c;
  double _b1 = 2.0 * _b0;
  double _b2 = _b0;
  double _a1 = 2.0 * (ohm * ohm - 1.0) / c;
  double _a2 = (1.0 - 2.0 * cs * ohm + ohm * ohm) / c;

  double x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  double _delay_element_1 = 0;
  double _delay_element_2 = 0;

  double delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  double output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}

float px4_1_int_32(float xraw_f)
{
  xraw_f = (int)xraw_f;
  const float _accel_range_scale = 9.80665f / 4096;
  const float _accel_scale_x_offset = 0;
  const float _accel_scale_x_scale = 1;
  const float sample_freq = 1000;
  const float _cutoff_freq = 30;
  const float M_PI_F = 3.14159f;

  float fr = sample_freq / _cutoff_freq;
  float ohm = tanf(M_PI_F / fr);
  float cs = cosf(M_PI_F / 4.0);
  float c = 1.0 + 2.0 * cs * ohm + ohm * ohm;
  float _b0 = ohm * ohm / c;
  float _b1 = 2.0 * _b0;
  float _b2 = _b0;
  float _a1 = 2.0 * (ohm * ohm - 1.0) / c;
  float _a2 = (1.0 - 2.0 * cs * ohm + ohm * ohm) / c;

  float x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  float _delay_element_1 = 0;
  float _delay_element_2 = 0;

  float delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  float output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}

double px4_1_int_64(double xraw_f)
{
  xraw_f = (int)xraw_f;
  const double _accel_range_scale = 9.80665f / 4096;
  const double _accel_scale_x_offset = 0;
  const double _accel_scale_x_scale = 1;
  const double sample_freq = 1000;
  const double _cutoff_freq = 30;
  const double M_PI_F = 3.14159f;

  double fr = sample_freq / _cutoff_freq;
  double ohm = tan(M_PI_F / fr);
  double cs = cos(M_PI_F / 4.0);
  double c = 1.0 + 2.0 * cs * ohm + ohm * ohm;
  double _b0 = ohm * ohm / c;
  double _b1 = 2.0 * _b0;
  double _b2 = _b0;
  double _a1 = 2.0 * (ohm * ohm - 1.0) / c;
  double _a2 = (1.0 - 2.0 * cs * ohm + ohm * ohm) / c;

  double x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  double _delay_element_1 = 0;
  double _delay_element_2 = 0;

  double delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  double output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}

float px4_1_const_32(float xraw_f)
{
  const float _accel_range_scale = 9.80665f / 4096;
  const float _accel_scale_x_offset = 0;
  const float _accel_scale_x_scale = 1;
  const float sample_freq = 1000;
  const float _cutoff_freq = 30;
  const float M_PI_F = 3.14159f;

  const float _b0 = 0x1.00408ep-7;
  const float _b1 = 0x1.00408ep-6;
  const float _b2 = 0x1.00408ep-7;
  const float _a1 = -0x1.bc16fep+0;
  const float _a2 = 0x1.883204p-1;

  float x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  float _delay_element_1 = 0;
  float _delay_element_2 = 0;

  float delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  float output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}

double px4_1_const_64(double xraw_f)
{
  const double _accel_range_scale = 9.80665f / 4096;
  const double _accel_scale_x_offset = 0;
  const double _accel_scale_x_scale = 1;
  const double sample_freq = 1000;
  const double _cutoff_freq = 30;
  const double M_PI_F = 3.14159f;

  const double _b0 = 0x1.00408ep-7;
  const double _b1 = 0x1.00408ep-6;
  const double _b2 = 0x1.00408ep-7;
  const double _a1 = -0x1.bc16fep+0;
  const double _a2 = 0x1.883204p-1;

  double x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  double _delay_element_1 = 0;
  double _delay_element_2 = 0;

  double delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  double output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}

float px4_1_const_int_32(float xraw_f)
{
  xraw_f = (int)xraw_f;
  const float _accel_range_scale = 9.80665f / 4096;
  const float _accel_scale_x_offset = 0;
  const float _accel_scale_x_scale = 1;
  const float sample_freq = 1000;
  const float _cutoff_freq = 30;
  const float M_PI_F = 3.14159f;

  const float _b0 = 0x1.00408ep-7;
  const float _b1 = 0x1.00408ep-6;
  const float _b2 = 0x1.00408ep-7;
  const float _a1 = -0x1.bc16fep+0;
  const float _a2 = 0x1.883204p-1;

  float x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  float _delay_element_1 = 0;
  float _delay_element_2 = 0;

  float delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  float output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}

double px4_1_const_int_64(double xraw_f)
{
  xraw_f = (int)xraw_f;
  const double _accel_range_scale = 9.80665f / 4096;
  const double _accel_scale_x_offset = 0;
  const double _accel_scale_x_scale = 1;
  const double sample_freq = 1000;
  const double _cutoff_freq = 30;
  const double M_PI_F = 3.14159f;

  const double _b0 = 0x1.00408ep-7;
  const double _b1 = 0x1.00408ep-6;
  const double _b2 = 0x1.00408ep-7;
  const double _a1 = -0x1.bc16fep+0;
  const double _a2 = 0x1.883204p-1;

  double x_in_new = ((xraw_f * _accel_range_scale) - _accel_scale_x_offset) * _accel_scale_x_scale;
  double _delay_element_1 = 0;
  double _delay_element_2 = 0;

  double delay_element_0 = x_in_new - _delay_element_1 * _a1 - _delay_element_2 * _a2;
  double output_1 = delay_element_0 * _b0 + _delay_element_1 * _b1 + _delay_element_2 * _b2;

  return output_1;
}



/* -------------------------------------------------------------------------- */
/* k_sin1                                                                     */
/* -------------------------------------------------------------------------- */

/* __kernel_sin( x, y, iy)
 * kernel sin function on ~[-pi/4, pi/4] (except on -0), pi/4 ~ 0.7854
 * Input x is assumed to be bounded by ~pi/4 in magnitude.
 * Input y is the tail of x.
 * Input iy indicates whether y is 0. (if iy=0, y assume to be 0). 
 *
 * Algorithm
 *	1. Since sin(-x) = -sin(x), we need only to consider positive x. 
 *	2. Callers must return sin(-0) = -0 without calling here since our
 *	   odd polynomial is not evaluated in a way that preserves -0.
 *	   Callers may do the optimization sin(x) ~ x for tiny x.
 *	3. sin(x) is approximated by a polynomial of degree 13 on
 *	   [0,pi/4]
 *		  	         3            13
 *	   	sin(x) ~ x + S1*x + ... + S6*x
 *	   where
 *	
 * 	|sin(x)         2     4     6     8     10     12  |     -58
 * 	|----- - (1+S1*x +S2*x +S3*x +S4*x +S5*x  +S6*x   )| <= 2
 * 	|  x 					           | 
 * 
 *	4. sin(x+y) = sin(x) + sin'(x')*y
 *		    ~ sin(x) + (1-x*x/2)*y
 *	   For better accuracy, let 
 *		     3      2      2      2      2
 *		r = x *(S2+x *(S3+x *(S4+x *(S5+x *S6))))
 *	   then                   3    2
 *		sin(x) = x + (S1*x + (x *(r-y/2)+y))
 */

static const double
half =  5.00000000000000000000e-01, /* 0x3FE00000, 0x00000000 */
S1  = -1.66666666666666324348e-01, /* 0xBFC55555, 0x55555549 */
S2  =  8.33333333332248946124e-03, /* 0x3F811111, 0x1110F8A6 */
S3  = -1.98412698298579493134e-04, /* 0xBF2A01A0, 0x19C161D5 */
S4  =  2.75573137070700676789e-06, /* 0x3EC71DE3, 0x57B1FE7D */
S5  = -2.50507602534068634195e-08, /* 0xBE5AE5E6, 0x8A2B9CEB */
S6  =  1.58969099521155010221e-10; /* 0x3DE5D93A, 0x5ACFD57C */

double k_sin1_64(double x)
{
  double z,r,v,w;

  z	=  x*x;
  w	=  z*z;
  r	=  S2+z*(S3+z*S4) + z*w*(S5+z*S6);
  v	=  z*x;
  return x+v*(S1+z*r);
}

quad k_sin1_128(quad x)
{
  quad z,r,v,w;

  z	=  x*x;
  w	=  z*z;
  r	=  S2+z*(S3+z*S4) + z*w*(S5+z*S6);
  v	=  z*x;
  return x+v*(S1+z*r);
}

/* -------------------------------------------------------------------------- */
/* k_log1pf                                                                   */
/* -------------------------------------------------------------------------- */

/* |(log(1+s)-log(1-s))/s - Lg(s)| < 2**-34.24 (~[-4.95e-11, 4.97e-11]). */

static const float
Lg1 =      0xaaaaaa.0p-24,	/* 0.66666662693 */
Lg2 =      0xccce13.0p-25,	/* 0.40000972152 */
Lg3 =      0x91e9ee.0p-25,	/* 0.28498786688 */
Lg4 =      0xf89e26.0p-26;	/* 0.24279078841 */

float k_log1pf_32(float f)
{
  float hfsq,s,z,R,w,t1,t2;

  s = f/((float)2.0+f);
  z = s*s;
  w = z*z;
  t1= w*(Lg2+w*Lg4);
  t2= z*(Lg1+w*Lg3);
  R = t2+t1;
  hfsq=(float)0.5*f*f;
  return s*(hfsq+R);
}

double k_log1pf_64(double f)
{
  double hfsq,s,z,R,w,t1,t2;

  s = f/((double)2.0+f);
  z = s*s;
  w = z*z;
  t1= w*(Lg2+w*Lg4);
  t2= z*(Lg1+w*Lg3);
  R = t2+t1;
  hfsq=(double)0.5*f*f;
  return s*(hfsq+R);
}

/* -------------------------------------------------------------------------- */
/* sub_sub                                                                    */
/* -------------------------------------------------------------------------- */

float sub_sub_32(float h)
{
  float w = 1.0f - h;
  return (1 - w) - h;
}

double sub_sub_64(double h)
{
  double w = 1.0d - h;
  return (1 - w) - h;
}

quad sub_sub_128(quad h)
{
  quad w = 1.0q - h;
  return (1 - w) - h;
}


/* -------------------------------------------------------------------------- */
/* predPrey                                                                   */
/* -------------------------------------------------------------------------- */

float predPrey_32(float x)
{
  return 4 * x * x / (1 + ((x / 1.11f) * (x / 1.11f)));
}

double predPrey_64(double x)
{
  return 4 * x * x / (1 + ((x / 1.11d) * (x / 1.11d)));
}

quad predPrey_128(quad x)
{
  return 4 * x * x / (1 + ((x / 1.11q) * (x / 1.11q)));
}

/* -------------------------------------------------------------------------- */
/* sineOrder3                                                                 */
/* -------------------------------------------------------------------------- */

float sine3_32(float x)
{
  return 0.954929658551372f * x -  0.12900613773279798f * (x * x * x);
}

double sine3_64(double x)
{
  return 0.954929658551372 * x -  0.12900613773279798 * (x * x * x);
}

quad sine3_128(quad x)
{
  return 0.954929658551372q * x -  0.12900613773279798q * (x * x * x);
}

// term1

float sine3_term1_32(float x)
{
  return 0.954929658551372f * x;
}

double sine3_term1_64(double x)
{
  return 0.954929658551372 * x;
}

quad sine3_term1_128(quad x)
{
  return 0.954929658551372q * x;
}

// term2

float sine3_term2_32(float x)
{
  return 0.12900613773279798f * (x * x * x);
}

double sine3_term2_64(double x)
{
  return 0.12900613773279798 * (x * x * x);
}

quad sine3_term2_128(quad x)
{
  return 0.12900613773279798q * (x * x * x);
}

/* -------------------------------------------------------------------------- */
/* sqroot                                                                     */
/* -------------------------------------------------------------------------- */

float sqroot_32(float x)
{
  return 1.0f + 0.5f*x - (0.125f*x)*x + ((0.0625f*x)*x)*x - (((0.0390625f*x)*x)*x)*x;
}

double sqroot_64(double x)
{
  return 1.0 + 0.5*x - (0.125*x)*x + ((0.0625*x)*x)*x - (((0.0390625*x)*x)*x)*x;
}

quad sqroot_128(quad x)
{
  return 1.0q + 0.5q*x - (0.125q*x)*x + ((0.0625q*x)*x)*x - (((0.0390625q*x)*x)*x)*x;
}


/* -------------------------------------------------------------------------- */
/* add1                                                                        */
/* -------------------------------------------------------------------------- */

float add1_32(float x)
{
  return x + 1.0f;
}

double add1_64(double x)
{
  return x + 1.0;
}

quad add1_128(quad x)
{
  return x + 1.0q;
}

/* -------------------------------------------------------------------------- */
/* add11                                                                       */
/* -------------------------------------------------------------------------- */

float add11_32(float x)
{
  return x + 1.1f;
}

double add11_64(double x)
{
  return x + 1.1;
}

quad add11_128(quad x)
{
  return x + 1.1q;
}


/* -------------------------------------------------------------------------- */
/* nonlin1                                                                        */
/* -------------------------------------------------------------------------- */

float nonlin1_32(float x)
{
  return x / (x + 1.0f);
}

double nonlin1_64(double x)
{
  return x / (x + 1.0);
}

quad nonlin1_128(quad x)
{
  return x / (x + 1.0q);
}

/* -------------------------------------------------------------------------- */
/* exp                                                                        */
/* -------------------------------------------------------------------------- */

float exp_32(float x)
{
  return expf(x);
}

double exp_64(double x)
{
  return exp(x);
}

quad exp_128(quad x)
{
  return expq(x);
}

/* -------------------------------------------------------------------------- */
/* log                                                                        */
/* -------------------------------------------------------------------------- */

float log_32(float x)
{
  return logf(x);
}

double log_64(double x)
{
  return log(x);
}

quad log_128(quad x)
{
  return logq(x);
}

/* -------------------------------------------------------------------------- */
/* sin                                                                        */
/* -------------------------------------------------------------------------- */

float sin_32(float x)
{
  return sinf(x);
}

double sin_64(double x)
{
  return sin(x);
}

quad sin_128(quad x)
{
  return sinq(x);
}

/* -------------------------------------------------------------------------- */
/* cos                                                                        */
/* -------------------------------------------------------------------------- */

float cos_32(float x)
{
  return cosf(x);
}

double cos_64(double x)
{
  return cos(x);
}

quad cos_128(quad x)
{
  return cosq(x);
}

/* -------------------------------------------------------------------------- */
/* sqrt                                                                        */
/* -------------------------------------------------------------------------- */

float sqrt_32(float x)
{
  return sqrtf(x);
}

double sqrt_64(double x)
{
  return sqrt(x);
}

quad sqrt_128(quad x)
{
  return sqrtq(x);
}

float sqrt_1_32(float x)
{
  return sqrtf(x + 1.0f);
}

double sqrt_1_64(double x)
{
  return sqrt(x + 1.0);
}

quad sqrt_1_128(quad x)
{
  return sqrtq(x + 1.0q);
}

/* -------------------------------------------------------------------------- */
/* kahan_exp                                                                  */
/* -------------------------------------------------------------------------- */

float kahan_exp_32(float x)
{
  float e = expf(x);
  float e1 = e - 1.0f;
  float r = e1 / x;
  return r;
}

double kahan_exp_64(double x)
{
  return (exp(x) - 1.0) / x;
}

quad kahan_exp_128(quad x)
{
  return (expq(x) - 1.0q) / x;
}

float kahan_exp_log_32(float x)
{
  float e = expf(x);
  float e1 = e - 1.0f;
  float l = logf(e);
  float r = e1 / l;
  return r;
}

double kahan_exp_log_64(double x)
{
  return (exp(x) - 1.0) / log(exp(x));
}

quad kahan_exp_log_128(quad x)
{
  return (expq(x) - 1.0q) / logq(expq(x));
}

/* -------------------------------------------------------------------------- */
/* kahan_exp_series                                                           */
/* -------------------------------------------------------------------------- */

float kahan_exp_series_3_32(float x)
{
  float r1 = x / 6.0f;
  float r2 = 0.5f + r1;
  float r3 = x * r2;
  float r = 1.0f + r3;
  return r;
}

double kahan_exp_series_3_64(double x)
{
  return 1.0 + x * (0.5 + x / 6.0);
}

quad kahan_exp_series_3_128(quad x)
{
  return 1.0q + x * (0.5q + x / 6.0q);
}

float kahan_exp_series_4_32(float x)
{
  float r1 = x / 4.0f;
  float r2 = 1.0f + r1;
  float r3 = r2 / 6.0f;
  float r4 = x * r3;
  float r5 = 0.5f + r4;
  float r6 = x * r5;
  float r = 1.0f + r6;
  return r;
}

double kahan_exp_series_4_64(double x)
{
  return 1.0 + x * (0.5 + x * ((1.0 + x / 4.0) / 6.0));
}

quad kahan_exp_series_4_128(quad x)
{
  return 1.0q + x * (0.5q + x * ((1.0q + x / 4.0q) / 6.0q));
}

/* -------------------------------------------------------------------------- */
/* sqrt_sub                                                                   */
/* -------------------------------------------------------------------------- */

float sqrt_sub_32(float x)
{
  float x1 = 1.0f + x;
  float r1 = sqrtf(x1);
  float r2 = sqrtf(x);
  float r = r1 - r2;
  return r;
}

double sqrt_sub_64(double x)
{
  return sqrt(1.0 + x) - sqrt(x);
}

quad sqrt_sub_128(quad x)
{
  return sqrtq(1.0q + x) - sqrtq(x);
}

float sqrt_add_32(float x)
{
  float x1 = 1.0f + x;
  float r1 = sqrtf(x1);
  float r2 = sqrtf(x);
  float r3 = r1 + r2;
  float r = 1.0f / r3;
  return r;
}

double sqrt_add_64(double x)
{
  return 1.0 / (sqrt(1.0 + x) + sqrt(x));
}

quad sqrt_add_128(quad x)
{
  return 1.0q / (sqrtq(1.0q + x) + sqrtq(x));
}

float sqrt_sub11_32(float x)
{
  float x1 = 1.1f + x;
  float r1 = sqrtf(x1);
  float r2 = sqrtf(x);
  float r = r1 - r2;
  return r;
}

double sqrt_sub11_64(double x)
{
  return sqrt(1.1 + x) - sqrt(x);
}

quad sqrt_sub11_128(quad x)
{
  return sqrtq(1.1q + x) - sqrtq(x);
}
