#include <getopt.h>
#include <stdlib.h>
#include "search.h"

extern const double low, high;

float f_32(float);
double f_64(double);
real f_real(real);

int main(int argc, char *argv[])
{

    int exp_scale_flag = 0;
    int double_precision_flag = 0;
    int real_inputs_flag = 0;
    int segments = 0;
    int samples = 0;
    char *out_name = NULL;

    struct option long_options[] = {
        // Double precision mode
        {"double", no_argument, &double_precision_flag, 1},
        // Inputs are assumed to be real numbers
        {"real", no_argument, &real_inputs_flag, 1},
        // Exponential scale
        {"exp", no_argument, &exp_scale_flag, 1},
        // Output file
        {"out", required_argument, 0, 'o'},
        // Number of segements
        {"segments", required_argument, 0, 'n'},
        // Number of random samples
        {"samples", required_argument, 0, 's'},
        {0, 0, 0, 0}
    };

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "dreo:n:s:", 
                            long_options, &option_index);

        if (c == -1) {
            break;
        }

        switch (c) {
            case 0:
                break;

            case 'd':
                double_precision_flag = 1;
                break;

            case 'r':
                real_inputs_flag = 1;
                break;

            case 'e':
                exp_scale_flag = 1;
                break;
            
            case 'o':
                out_name = optarg;
                break;

            case 'n':
                segments = atoi(optarg);
                break;

            case 's':
                samples = atoi(optarg);
                break;

            case '?':
                break;

            default:
                abort();
        }
    }

    if (segments < 1) {
        fprintf(stderr, "Bad number of segments: %d\n", segments);
        exit(2);
    }

    if (samples < 1) {
        fprintf(stderr, "Bad number of samples: %d\n", samples);
        exit(2);
    }

    if (!out_name) {
        fprintf(stderr, "[WARNING] Output file is not specified\n");
        out_name = "data.txt";
    }

    FILE *out = fopen(out_name, "w");
    if (!out) {
        fprintf(stderr, "Bad output file: %s\n", out_name);
        exit(2);
    }
    
    search_args args = {
        .a = low,
        .b = high,
        .scale = exp_scale_flag ? SCALE_EXP : SCALE_UNIFORM,
        .segments = segments,
        .samples = samples
    };

    if (double_precision_flag) {
        if (real_inputs_flag) {
            random_test_64_real(out, f_64, f_real, args);
        }
        else {
            random_test_64(out, f_64, f_real, args);
        }
    }
    else {
        exhaustive_test_32(out, f_32, f_64, args);
    }

    fclose(out);
    return 0;
}