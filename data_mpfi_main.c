#include <getopt.h>
#include <stdlib.h>

#include "data_mpfi.h"

extern const double low[], high[];
extern const F_MPFI funcs[];
extern const char *f_names[];
extern const int n_funcs;

void f_init(void);
void f_clear(void);

int g_debug_flag = 0;

int main(int argc, char *argv[])
{
    int adaptive_flag = 0;
    int segments = 100;
    mpfr_prec_t prec = 100;
    char *out_name = NULL;

    struct option long_options[] = {
        // Print debug information
        {"debug", no_argument, &g_debug_flag, 'd'},
        // Use the adaptive algorithm
        {"adaptive", no_argument, &adaptive_flag, 'a'},
        // Output file
        {"out", required_argument, 0, 'o'},
        // MPFR precision
        {"prec", required_argument, 0, 'p'},
        // Number of segements
        {"segments", required_argument, 0, 'n'},
        {0, 0, 0, 0}
    };

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "dao:p:n:", 
                            long_options, &option_index);

        if (c == -1) {
            break;
        }

        switch (c) {
            case 0:
                break;

            case 'd':
                g_debug_flag = 1;
                break;

            case 'a':
                adaptive_flag = 1;
                break;

            case 'o':
                out_name = optarg;
                break;

            case 'p':
                prec = atoi(optarg);
                break;

            case 'n':
                segments = atoi(optarg);
                break;

            case '?':
                break;

            default:
                abort();
        }
    }

    if (prec < MPFR_PREC_MIN || prec > MPFR_PREC_MAX / 2) {
        fprintf(stderr, "Bad MPFR precision: %ld\n", prec);
        exit(2);
    }

    if (segments < 1) {
        fprintf(stderr, "Bad number of segments: %d\n", segments);
        exit(2);
    }

    if (!out_name) {
        fprintf(stderr, "[WARNING] Output file is not specified\n");
        out_name = "data.txt";
    }

    FILE *out = fopen(out_name, "w");
    if (!out) {
        fprintf(stderr, "Bad output file: %s\n", out_name);
        exit(2);
    }
    
    data_args args = {
        .a = low[0],
        .b = high[0],
        .segments = segments
    };

    mpfr_set_default_prec(prec);
    f_init();

    for (int i = 0; i < n_funcs; i++) {
        if (f_names[i]) {
            fprintf(out, "%s\n", f_names[i]);
            printf("Processing %d: %s\n", i, f_names[i]);
        }
        else {
            printf("Processing %d\n", i);
        }

        if (!funcs[i]) {
            fprintf(stderr, "funcs[%d] == NULL\n", i);
            continue;
        }

        if (adaptive_flag) {
            data_adaptive(out, funcs[i], args);
        }
        else {
            data_simple(out, funcs[i], args);
        }

        if (i + 1 < n_funcs) {
            fprintf(out, "\n\n");
        }
    }

    f_clear();
    fclose(out);

    return 0;
}