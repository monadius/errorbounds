#ifndef __SEARCH__
#define __SEARCH__

#include <stdio.h>

typedef long double real;

typedef float (* F32)(float);
typedef double (* F64)(double);
typedef real (* FREAL)(real);

typedef enum {
  SCALE_UNIFORM,
  SCALE_EXP
} scale_type;

typedef struct t_search_args {
    double a, b;
    scale_type scale;
    int segments;
    int samples;
} search_args;

void exhaustive_test_32(FILE *out, const F32 f_32, const F64 f_64,
                        const search_args args);

void random_test_64(FILE *out, const F64 f_64, const FREAL f_real,
                    const search_args args);

void random_test_64_real(FILE *out, const F64 f_64, const FREAL f_real,
                         const search_args args);

#endif