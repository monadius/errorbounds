#include "../fp.h"

float max_error_arg = -1000.0f;
uint64_t count_errors = 0;

void __VERIFIER_error(float x)
{
  if (x > max_error_arg) {
    max_error_arg = x;
  }

  count_errors += 1;
}

void reset_report()
{
  max_error_arg = -1000.0f;
  count_errors = 0;
}

void print_report(FILE *out)
{
  if (count_errors == 0) {
    fprintf(out, "\nNO ERRORS\n\n");
  }
  else {
    fprintf(out, "\nERRORS DETECTED\n\n");
    fprintf(out, "errors = %d\n", (int)count_errors);
    fprintf(out, "max_error_arg = %e (%a)\n\n", max_error_arg, max_error_arg);
  }
}

/*
extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
*/

#ifndef NR
//#error PLEASE DEFINE THE NR OF THE BENCHMARK (FLAG -DNR=[1,8])
#endif

#if NR == 1
#define VAL 0.2f
#elif NR == 2
#define VAL 0.4f
#elif NR == 3
#define VAL 0.6f
#elif NR == 4
#define VAL 0.8f
#elif NR == 5
#define VAL 1.0f
#elif NR == 6
#define VAL 1.2f
#elif NR == 7
#define VAL 1.4f
#elif NR == 8
#define VAL 2.0f
#endif

//#ifndef ITERATIONS
//#error please set number of iterations (between 2 and 3) 
//#endif 

//#if !(ITERATIONS >= 1 && ITERATIONS <= 3)
//#error Number of iterations must be between 1 and 3
//#endif 

float f(float x)
{
  return x - (x*x*x)/6.0f + (x*x*x*x*x)/120.0f + (x*x*x*x*x*x*x)/5040.0f;
}

float fp(float x)
{
  return 1 - (x*x)/2.0f + (x*x*x*x)/24.0f + (x*x*x*x*x*x)/720.0f;
}

void result(float IN, int ITERATIONS)
{
  //  float IN;
  //  __VERIFIER_assume(IN > -VAL && IN < VAL);

  float x = IN - f(IN)/fp(IN);
  if (ITERATIONS > 1) {
    x = x - f(x)/fp(x);
    if (ITERATIONS > 2) {
      x = x - f(x)/fp(x);
    }
  }

  if(!(x < 0.1))
    __VERIFIER_error(IN);

  //  return 0;
}

int main()
{
  fprintf(stdout, "VAL = %f\n", VAL);

  // +1 because IN > -VAL
  uint32_t start = to_ordinal_32(-VAL) + 1;
  // -1 because IN < VAL
  uint32_t end = to_ordinal_32(VAL) - 1;

  uint32_t k = (end - start) / 1000;
  if (k <= 0) {
    k = 1;
  }

  uint32_t segments = (end - start) / k;

  for (int iter = 1; iter <= 3; iter++) {
    uint32_t next = start;
    uint32_t c = 0;

    fprintf(stdout, "\n\nITERATIONS = %d\n", iter);

    for (uint32_t i = start; i <= end; i++) {
      float x = from_ordinal_32(i);
      result(x, iter);

      if (i >= next) {
	c++;
	next = i + k;
	fprintf(stderr, "%d / %d       \r", (int)c, (int)segments);
      }
    }

    print_report(stdout);
    reset_report();
  }

  return 0;
}

