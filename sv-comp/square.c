#include "../fp.h"

float max_error_arg = -1000.0f;
uint64_t count_errors = 0;

void __VERIFIER_error(float x)
{
  if (x > max_error_arg) {
    max_error_arg = x;
  }

  count_errors += 1;
}

void print_report(FILE *out)
{
  if (count_errors == 0) {
    fprintf(out, "\nNO ERRORS\n\n");
  }
  else {
    fprintf(out, "\nERRORS DETECTED\n\n");
    fprintf(out, "errors = %d\n", (int)count_errors);
    fprintf(out, "max_error_arg = %e (%a)\n\n", max_error_arg, max_error_arg);
  }
}

/*
extern void __VERIFIER_assume(int);
extern void __VERIFIER_error(void);
*/
//APPROXIMATES sqroot(1+x)

#ifndef NR
#error PLEASE DEFINE THE NR OF THE BENCHMARK (FLAG -DNR=[1,8])
#endif


#if NR == 1
#define VAL 1.39f
#elif NR == 2
#define VAL 1.398f
#elif NR == 3
#define VAL 1.39843f
#elif NR == 4
#define VAL 1.39844f
#elif NR == 5
#define VAL 1.3985f
#elif NR == 6
#define VAL 1.399f
#elif NR == 7
#define VAL 1.4f
#elif NR == 8
#define VAL 1.5f
#endif

void f(float IN)
{
  //  float IN;
  //  __VERIFIER_assume(IN >= 0.0f && IN < 1.0f);

  float x = IN;
  
  float result = 
    1.0f + 0.5f*x - 0.125f*x*x + 0.0625f*x*x*x - 0.0390625f*x*x*x*x;

  if(!(result >= 0.0f && result < VAL))
    __VERIFIER_error(x);
  
  //  return 0;
}


int main()
{
  uint32_t start = to_ordinal_32(0.0f);
  // -1 because IN < 1.0f
  uint32_t end = to_ordinal_32(1.0f) - 1;

  uint32_t k = (end - start) / 1000;
  if (k <= 0) {
    k = 1;
  }

  uint32_t segments = (end - start) / k;
  uint32_t next = start;
  uint32_t c = 0;

  fprintf(stdout, "VAL = %f\n", VAL);

  for (uint32_t i = start; i <= end; i++) {
    float x = from_ordinal_32(i);
    f(x);

    if (i >= next) {
      c++;
      next = i + k;
      fprintf(stderr, "%d / %d       \r", (int)c, (int)segments);
    }
  }

  print_report(stdout);

  return 0;
}

