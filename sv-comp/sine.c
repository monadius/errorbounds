#include "../fp.h"

float max_error_arg = -1000.0f;
uint64_t count_errors = 0;

void __VERIFIER_error(float x)
{
  if (x > max_error_arg) {
    max_error_arg = x;
  }

  count_errors += 1;
}

void print_report(FILE *out)
{
  if (count_errors == 0) {
    fprintf(out, "\nNO ERRORS\n\n");
  }
  else {
    fprintf(out, "\nERRORS DETECTED\n\n");
    fprintf(out, "errors = %d\n", (int)count_errors);
    fprintf(out, "max_error_arg = %e (%a)\n\n", max_error_arg, max_error_arg);
  }
}


/*
extern void __VERIFIER_error(void);
extern void __VERIFIER_assume(int);
*/
#define HALFPI 1.57079632679f

#ifndef NR
#error PLEASE DEFINE THE NR OF THE BENCHMARK (FLAG -DNR=[1,8])
#endif

#if NR == 1
#define VAL 0.99
#elif NR == 2
#define VAL 1.0f
#elif NR == 3
#define VAL 1.001f
#elif NR == 4
#define VAL 1.01f
#elif NR == 5
#define VAL 1.1f
#elif NR == 6
#define VAL 1.2f
#elif NR == 7
#define VAL 1.5f
#elif NR == 8
#define VAL 2.0f
#endif

void f(float IN)
{
  //  float IN;
  //  __VERIFIER_assume(IN > -HALFPI && IN < HALFPI);

  float x = IN;
  
  float result = x - (x*x*x)/6.0f + (x*x*x*x*x)/120.0f + (x*x*x*x*x*x*x)/5040.0f;

  if(!(result <= VAL && result >= -VAL))
    __VERIFIER_error(x);
}



int main()
{
  // +1 because IN > -HALFPI
  uint32_t start = to_ordinal_32(-HALFPI) + 1;
  // -1 because IN < HALFPI
  uint32_t end = to_ordinal_32(HALFPI) - 1;

  uint32_t k = (end - start) / 1000;
  if (k <= 0) {
    k = 1;
  }

  uint32_t segments = (end - start) / k;
  uint32_t next = start;
  uint32_t c = 0;

  fprintf(stdout, "VAL = %f\n", VAL);

  for (uint32_t i = start; i <= end; i++) {
    float x = from_ordinal_32(i);
    f(x);

    if (i >= next) {
      c++;
      next = i + k;
      fprintf(stderr, "%d / %d       \r", (int)c, (int)segments);
    }
  }

  print_report(stdout);

  return 0;
}
