#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <error.h>
#include <argp.h>
#include <stdbool.h>
#include <math.h>

typedef __float128 quad;

#include "functions.h"
#include "functions2d.h"

typedef float (* F32)(float);
typedef float (* F32_2D)(float, float);

typedef double (* F64)(double);
typedef double (* F64_2D)(double, double);

typedef quad (* F128)(quad);
typedef quad (* F128_2D)(quad, quad);

typedef enum {
  SCALE_UNIFORM,
  SCALE_EXP
} scale_type;

/* -------------------------------------------------------------------------- */
/* Copied from 
   http://stackoverflow.com/questions/20175148/64bit-random-number-between-a-range */
/* -------------------------------------------------------------------------- */

// TODO: a better random number generator is required
uint64_t urand64() 
{
  uint64_t hi = rand();
  uint64_t md = rand();
  uint64_t lo = rand();
  return (hi << 42) + (md << 21) + lo;
}

bool is_power_of_2(uint64_t x) 
{
  return x == x & -x;
}

uint64_t unsigned_uniform_random(uint64_t low, uint64_t high) {
  static const uint64_t M = ~(uint64_t)0; 
  uint64_t range = high - low;
  uint64_t to_exclude = is_power_of_2(range) ? 0
                                             : M % range + 1;
  uint64_t res;
  // Eliminate 'to_exclude' possible values from consideration.
  while ((res = urand64()) < to_exclude) {}
  return low + res % range;
}

// Requires b > a
uint64_t urand64_range(uint64_t a, uint64_t b)
{
  uint64_t range = b - a;
  return a + urand64() % range;
}


uint32_t urand32()
{
  uint32_t hi = rand() & 0xFFFF;
  uint32_t lo = rand() & 0xFFFF;
  return (hi << 16) + lo;
}

// Requires b > a
uint32_t urand32_range(uint32_t a, uint32_t b)
{
  uint32_t range = b - a;
  return a + urand32() % range;
}

// TODO: a better random number generator is required
quad rand_quad()
{
  return (quad)rand() / (RAND_MAX + 1.0q);
}

quad rand_quad_range(quad a, quad b)
{
  return a + rand_quad() * (b - a);
}


/* -------------------------------------------------------------------------- */
/* Float <-> ordinal conversions (32 bits)                                    */
/* (Strictly speaking, not portable due to the strict aliasing rule)          */
/* -------------------------------------------------------------------------- */

typedef union {
  float f;
  uint32_t i;
} Float32;

// Converts a floating-point number to the corresponding raw ordinal
// (does not preserve order between positive and negative numbers)
uint32_t to_raw_ordinal_32(float f)
{
  Float32 v = { .f = f };
  return v.i;
}

// Converts a floating-point number to the corresponding ordinal
uint32_t to_ordinal_32(float f)
{
  Float32 v = { .f = f };
  uint32_t i = v.i;

  if (i & 0x80000000) {
    return 0xFFFFFFFF - i;
  }
  else {
    return i | 0x80000000;
  }
}

// Converts a raw ordinal to the corresponding floating-point number
float from_raw_ordinal_32(uint32_t i)
{
  Float32 v = { .i = i };
  return v.f;
}

// Converts an ordinal to the corresponding floating-point number
float from_ordinal_32(uint32_t i)
{
  if (i & 0x80000000) {
    Float32 v = { .i = i & ~0x80000000 };
    return v.f;
  }
  else {
    Float32 v = { .i = 0xFFFFFFFF - i };
    return v.f;
  }
}

// ULP
float ulp_32(float x)
{
  uint32_t r;
  Float32 v = { .f = x };
  
  uint32_t exp = (v.i >> 23) & 0xFF;

  if (exp > 23) {
    r = (exp - 23) << 23;
  }
  else if (exp == 0) {
    return 0x1p-149f;
  }
  else {
    uint32_t t = exp - 1;
    r = 1 << t;
  }

  Float32 out = { .i = r };
  return out.f;
}

/* -------------------------------------------------------------------------- */
/* Float <-> ordinal conversions (64 bits)                                    */
/* (Strictly speaking, not portable due to the strict aliasing rule)          */
/* -------------------------------------------------------------------------- */

typedef union {
  double f;
  uint64_t i;
} Float64;

// Converts a floating-point number to the corresponding raw ordinal
// (does not preserve order between positive and negative numbers)
uint64_t to_raw_ordinal_64(double f)
{
  Float64 v = { .f = f };
  return v.i;
}

// Converts a floating-point number to the corresponding ordinal
uint64_t to_ordinal_64(double f)
{
  Float64 v = { .f = f };
  uint64_t i = v.i;

  if (i & 0x8000000000000000ull) {
    return 0xFFFFFFFFFFFFFFFFull - i;
  }
  else {
    return i | 0x8000000000000000ull;
  }
}

// Converts a raw ordinal to the corresponding floating-point number
double from_raw_ordinal_64(uint64_t i)
{
  Float64 v = { .i = i };
  return v.f;
}

// Converts an ordinal to the corresponding floating-point number
double from_ordinal_64(uint64_t i)
{
  if (i & 0x8000000000000000ull) {
    Float64 v = { .i = i & ~0x8000000000000000ull };
    return v.f;
  }
  else {
    Float64 v = { .i = 0xFFFFFFFFFFFFFFFFull - i };
    return v.f;
  }
}

// ULP
double ulp_64(double x)
{
  uint64_t r;
  Float64 v = { .f = x };
  
  uint64_t exp = (v.i >> 52) & 0x7FF;

  if (exp > 52) {
    r = (exp - 52) << 52;
  }
  else if (exp == 0) {
    return 0x1p-1074;
  }
  else {
    uint64_t t = exp - 1;
    //    printf("t = %d\n", (int)t);
    r = 1ull << t;
    //printf("r = %lld\n", (long long)r);
  }

  Float64 out = { .i = r };
  return out.f;
}

/* -------------------------------------------------------------------------- */
/* Error computation                                                          */
/* -------------------------------------------------------------------------- */


// Absolute error 32
double abs_error_32(float low, double high)
{
  double r = (double)low;
  return fabs(r - high);
}

// Relative error 32
double rel_error_32(float low, double high)
{
  double r = (double)low;
  double d = r - high;

  if (d == 0.0) {
    return 0.0;
  }
  else {
    return fabs(d / high);
  }
}

// ULP error 32
double ulp_error_32(float low, double high)
{
  double r = (double)low;
  double d = r - high;

  if (d == 0.0) {
    return 0.0;
  }
  else {
    return fabs(d / (double)ulp_32((float)high));
  }
}

// Absolute error 64
quad abs_error_64(double low, quad high)
{
  quad r = (quad)low;
  return fabsq(r - high);
}

// Relative error 64
quad rel_error_64(double low, quad high)
{
  quad r = (quad)low;
  quad d = r - high;

  if (d == 0.0q) {
    return 0.0q;
  }
  else {
    return fabsq(d / high);
  }
}

// ULP error 64
quad ulp_error_64(double low, quad high)
{
  quad r = (quad)low;
  quad d = r - high;

  if (d == 0.0q) {
    return 0.0q;
  }
  else {
    return fabsq(d / (quad)ulp_64((double)high));
  }
}

/* -------------------------------------------------------------------------- */
/* Exhaustive test for 32-bit values                                          */
/* -------------------------------------------------------------------------- */

void exhaustive_test_32(FILE *out, 
			const F32 f_32, const F64 f_64, 
			const float a, const float b, const scale_type scale,
			const int segments)
{
  assert (segments > 0);
  assert (b >= a);
  assert (f_32 && f_64);

  const uint32_t ia = to_ordinal_32(a);
  const uint32_t ib = to_ordinal_32(b);
  const float step = (b - a) / segments;
  uint32_t k = (ib - ia) / segments;

  if (k == 0) {
    k = 1;
  }

  double total_max_abs = -INFINITY;
  double total_max_rel = -INFINITY;
  double total_max_ulp = -INFINITY;
  float arg_total_abs = 0.0;
  float arg_total_rel = 0.0;
  float arg_total_ulp = 0.0;

  printf("exhaustive_test_32: testing from %f to %f...\n", a, b);
  printf("  segments = %d\n", segments);

  if (scale == SCALE_EXP) {
    printf("  exponential scale\n");
  }
  else {
    printf("  uniform scale\n");
  }
  
  fprintf(out, "%d, %e, %e\n", segments, a, b);

  uint32_t start = ia;

  for (int i = 0; i < segments && start <= ib; i++) {
    double max_abs = -INFINITY;
    double max_rel = -INFINITY;
    double max_ulp = -INFINITY;
    float arg_abs = 0.0;
    float arg_rel = 0.0;
    float arg_ulp = 0.0;

    // TODO: make sure that no overflows occur
    uint32_t end;
    if (scale == SCALE_EXP) {
      end = start + k;
    }
    else {
      float t = a + (i + 1) * step;
      end = to_ordinal_32(t) + 1;
    }

    if (i == segments - 1) {
      end = ib + 1;
    }

    if (end > ib + 1 || end <= start) {
      end = ib + 1;
    }

    for (uint32_t p = start; p < end; p++) {
      float x = from_ordinal_32(p);
      float low = f_32(x);
      double high = f_64((double)x);

      double abs = abs_error_32(low, high);
      double rel = rel_error_32(low, high);
      double ulp = ulp_error_32(low, high);
    
      if (abs > max_abs) {
	max_abs = abs;
	arg_abs = x;
      }

      if (rel > max_rel) {
	max_rel = rel;
	arg_rel = x;
      }

      if (ulp > max_ulp) {
	max_ulp = ulp;
	arg_ulp = x;
      }
    }

    fprintf(out, "%d, %e, %e, %e, %e\n", 
	    i + 1, from_ordinal_32(end - 1), 
	    (double)max_abs, (double)max_rel, (double)max_ulp);

    fprintf(stderr, "\r%d/%d         ", i + 1, segments);

    if (max_abs > total_max_abs) {
      total_max_abs = max_abs;
      arg_total_abs = arg_abs;
    }

    if (max_rel > total_max_rel) {
      total_max_rel = max_rel;
      arg_total_rel = arg_rel;
    }

    if (max_ulp > total_max_ulp) {
      total_max_ulp = max_ulp;
      arg_total_ulp = arg_ulp;
    }

    start = end;
  }

  printf("done\n\n");

  printf("max_abs = %e [%a]\t at %e [%a]\n", 
	 total_max_abs, total_max_abs, arg_total_abs, arg_total_abs);
  printf("max_rel = %e [%a]\t at %e [%a]\n", 
	 total_max_rel, total_max_rel, arg_total_rel, arg_total_rel);
  printf("max_ulp = %e [%a]\t at %e [%a]\n\n", 
	 total_max_ulp, total_max_ulp, arg_total_ulp, arg_total_ulp);  
}

/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values                                              */
/* -------------------------------------------------------------------------- */

void random_test_64(FILE *out,
		    const F64 f_64, const F128 f_128, 
		    const double a, const double b, const scale_type scale,
		    const int segments, const int samples)
{
  assert (segments > 0);
  assert (samples > 0);
  assert (b >= a);
  assert (f_64 && f_128);

  const uint64_t ia = to_ordinal_64(a);
  const uint64_t ib = to_ordinal_64(b);
  uint64_t k = (ib - ia) / segments;

  const double step = (b - a) / segments;

  if (k == 0) {
    k = 1;
  }

  const bool exhaustive_flag = (k <= samples);

  quad total_max_abs = FLT128_MIN;
  quad total_max_rel = FLT128_MIN;
  quad total_max_ulp = FLT128_MIN;
  double arg_total_abs = 0.0;
  double arg_total_rel = 0.0;
  double arg_total_ulp = 0.0;

  printf("random_test_64: testing from %f to %f...\n", a, b);
  printf("  segments = %d\n  samples = %d\n", segments, samples);

  if (exhaustive_flag) {
    printf(" performing an exhaustive search\n");
  }

  if (scale == SCALE_EXP) {
    printf("  exponential scale\n");
  }
  else {
    printf("  uniform scale\n");
  }
  
  fprintf(out, "%d, %e, %e\n", segments, a, b);

  uint64_t start = ia;

  for (int i = 0; i < segments && start <= ib; i++) {
    quad max_abs = FLT128_MIN;
    quad max_rel = FLT128_MIN;
    quad max_ulp = FLT128_MIN;
    double arg_abs = 0.0;
    double arg_rel = 0.0;
    double arg_ulp = 0.0;

    // TODO: make sure that no overflows occur
    uint64_t end;
    if (scale == SCALE_EXP) {
      end = start + k;
    }
    else {
      double t = a + (i + 1) * step;
      end = to_ordinal_64(t) + 1;
    }

    if (i == segments - 1) {
      end = ib + 1;
    }

    if (end > ib + 1 || end <= start) {
      end = ib + 1;
    }

    if (exhaustive_flag) {
      // Exhaustive search
      for (uint64_t p = start; p < end; p++) {
	double x = from_ordinal_64(p);
	double low = f_64(x);
	quad high = f_128((quad)x);

	quad abs = abs_error_64(low, high);
	quad rel = rel_error_64(low, high);
	quad ulp = ulp_error_64(low, high);

	if (abs > max_abs) {
	  max_abs = abs;
	  arg_abs = x;
	}

	if (rel > max_rel) {
	  max_rel = rel;
	  arg_rel = x;
	}

	if (ulp > max_ulp) {
	  max_ulp = ulp;
	  arg_ulp = x;
	}
      }
    }
    else {
      // Random testing
      for (int t = 0; t < samples; t++) {
	uint64_t r = urand64_range(start, end);

	double x = from_ordinal_64(r);
	double low = f_64(x);
	quad high = f_128((quad)x);

	quad abs = abs_error_64(low, high);
	quad rel = rel_error_64(low, high);
	quad ulp = ulp_error_64(low, high);

	if (abs > max_abs) {
	  max_abs = abs;
	  arg_abs = x;
	}

	if (rel > max_rel) {
	  max_rel = rel;
	  arg_rel = x;
	}

	if (ulp > max_ulp) {
	  max_ulp = ulp;
	  arg_ulp = x;
	}
      }
    }

    fprintf(out, "%d, %e, %e, %e, %e\n", 
	    i + 1, from_ordinal_64(end - 1), 
	    (double)max_abs, (double)max_rel, (double)max_ulp);

    fprintf(stderr, "\r%d/%d         ", i + 1, segments);

    if (max_abs > total_max_abs) {
      total_max_abs = max_abs;
      arg_total_abs = arg_abs;
    }

    if (max_rel > total_max_rel) {
      total_max_rel = max_rel;
      arg_total_rel = arg_rel;
    }

    if (max_ulp > total_max_ulp) {
      total_max_ulp = max_ulp;
      arg_total_ulp = arg_ulp;
    }

    start = end;
  }

  printf("done\n\n");

  printf("max_abs = %e [%a]\t at %e [%a]\n", 
	 (double)total_max_abs, (double)total_max_abs, arg_total_abs, arg_total_abs);
  printf("max_rel = %e [%a]\t at %e [%a]\n", 
	 (double)total_max_rel, (double)total_max_rel, arg_total_rel, arg_total_rel);
  printf("max_ulp = %e [%a]\t at %e [%a]\n\n", 
	 (double)total_max_ulp, (double)total_max_ulp, arg_total_ulp, arg_total_ulp);
}

/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values with "real" input arguments                  */
/* -------------------------------------------------------------------------- */

void random_test_64_real(FILE *out,
		    const F64 f_64, const F128 f_128, 
		    const double a, const double b, const scale_type scale,
		    const int segments, const int samples)
{
  assert (segments > 0);
  assert (samples > 0);
  assert (b >= a);
  assert (f_64 && f_128);

  const uint64_t ia = to_ordinal_64(a);
  const uint64_t ib = to_ordinal_64(b);
  uint64_t k = (ib - ia) / segments;

  const double step = (b - a) / segments;

  if (k == 0) {
    k = 1;
  }

  quad total_max_abs = FLT128_MIN;
  quad total_max_rel = FLT128_MIN;
  quad total_max_ulp = FLT128_MIN;
  quad arg_total_abs = 0.0;
  quad arg_total_rel = 0.0;
  quad arg_total_ulp = 0.0;

  printf("random_test_64_real: testing from %f to %f...\n", a, b);
  printf("  segments = %d\n  samples = %d\n", segments, samples);

  if (scale == SCALE_EXP) {
    printf("  exponential scale\n");
  }
  else {
    printf("  uniform scale\n");
  }
  
  fprintf(out, "%d, %e, %e\n", segments, a, b);

  uint64_t start = ia;

  for (int i = 0; i < segments && start <= ib; i++) {
    quad max_abs = FLT128_MIN;
    quad max_rel = FLT128_MIN;
    quad max_ulp = FLT128_MIN;
    quad arg_abs = 0.0;
    quad arg_rel = 0.0;
    quad arg_ulp = 0.0;

    // TODO: make sure that no overflows occur
    uint64_t end;
    if (scale == SCALE_EXP) {
      end = start + k;
    }
    else {
      double t = a + (i + 1) * step;
      end = to_ordinal_64(t) + 1;
    }

    if (i == segments - 1) {
      end = ib + 1;
    }

    if (end > ib + 1 || end <= start) {
      end = ib + 1;
    }

    // Random testing
    quad start_val = (quad)from_ordinal_64(start);
    quad end_val = (quad)from_ordinal_64(end);

    for (int t = 0; t < samples; t++) {
      quad x = rand_quad_range(start_val, end_val);

      double low = f_64((double)x);
      quad high = f_128(x);

      quad abs = abs_error_64(low, high);
      quad rel = rel_error_64(low, high);
      quad ulp = ulp_error_64(low, high);

      if (abs > max_abs) {
	max_abs = abs;
	arg_abs = x;
      }

      if (rel > max_rel) {
	max_rel = rel;
	arg_rel = x;
      }

      if (ulp > max_ulp) {
	max_ulp = ulp;
	arg_ulp = x;
      }
    }

    fprintf(out, "%d, %e, %e, %e, %e\n", 
	    i + 1, from_ordinal_64(end - 1), 
	    (double)max_abs, (double)max_rel, (double)max_ulp);
    
    fprintf(stderr, "\r%d/%d         ", i + 1, segments);
  
    if (max_abs > total_max_abs) {
      total_max_abs = max_abs;
      arg_total_abs = arg_abs;
    }

    if (max_rel > total_max_rel) {
      total_max_rel = max_rel;
      arg_total_rel = arg_rel;
    }

    if (max_ulp > total_max_ulp) {
      total_max_ulp = max_ulp;
      arg_total_ulp = arg_ulp;
    }

    start = end;
  }

  printf("done\n\n");

  char buf[1000];

  quadmath_snprintf(buf, sizeof(buf), "%Qa", arg_total_abs);
  printf("max_abs = %e [%a]\t at %e [%s]\n", 
	 (double)total_max_abs, (double)total_max_abs, (double)arg_total_abs, buf);

  quadmath_snprintf(buf, sizeof(buf), "%Qa", arg_total_rel);
  printf("max_rel = %e [%a]\t at %e [%s]\n", 
	 (double)total_max_rel, (double)total_max_rel, (double)arg_total_rel, buf);

  quadmath_snprintf(buf, sizeof(buf), "%Qa", arg_total_ulp);
  printf("max_ulp = %e [%a]\t at %e [%s]\n\n", 
	 (double)total_max_ulp, (double)total_max_ulp, (double)arg_total_ulp, buf);
}


/* -------------------------------------------------------------------------- */
/* Random test for 32-bit values (2d)                                         */
/* -------------------------------------------------------------------------- */

void random_test_32_2d(FILE *out,
		       const F32_2D f_low, const F64_2D f_high,
		       const float x0, const float x1, 
		       const float y0, const float y1,
		       const scale_type scale,
		       const int segments_x, const int segments_y,
		       const int samples)
{
  assert (segments_x > 0 && segments_y > 0);
  assert (samples > 0);
  assert (x1 >= x0 && y1 >= y0);
  assert (f_low && f_high);

  const uint32_t ix0 = to_ordinal_32(x0);
  const uint32_t ix1 = to_ordinal_32(x1);
  const uint32_t iy0 = to_ordinal_32(y0);
  const uint32_t iy1 = to_ordinal_32(y1);
  uint32_t kx = (ix1 - ix0) / segments_x;
  uint32_t ky = (iy1 - iy0) / segments_y;

  const float step_x = (x1 - x0) / segments_x;
  const float step_y = (y1 - y0) / segments_y;

  if (kx <= 0) {
    kx = 1;
  }

  if (ky <= 0) {
    ky = 1;
  }

  double total_max_abs = -INFINITY;
  double total_max_rel = -INFINITY;
  double total_max_ulp = -INFINITY;
  float arg_total_abs[2] = {0.0, 0.0};
  float arg_total_rel[2] = {0.0, 0.0};
  float arg_total_ulp[2] = {0.0, 0.0};

  printf("random_test_32_2d: testing from [%f, %f] to [%f, %f]...\n", 
	 x0, y0, x1, y1);
  printf("  segments = %d, %d\n  samples = %d\n", 
	 segments_x, segments_y, samples);

  if (scale == SCALE_EXP) {
    printf("  exponential scale\n");
  }
  else {
    printf("  uniform scale\n");
  }
  
  fprintf(out, "nx, x0, x1, ny, y0, y1\n");
  fprintf(out, "%d, %e, %e, %d, %e, %e\n", 
	  segments_x, x0, x1,
	  segments_y, y0, y1);

  fprintf(out, "x0, x1, y0, y1, abs, rel, ulp\n");

  int counter = 0;
  const int total = segments_x * segments_y;

  uint32_t start_x = ix0;
  for (int i = 0; i < segments_x && start_x <= ix1; i++) {
    uint32_t end_x;
    if (scale == SCALE_EXP) {
      end_x = start_x + kx;
    }
    else {
      float t = x0 + (i + 1) * step_x;
      end_x = to_ordinal_32(t) + 1;
    }

    if (i == segments_x - 1) {
      end_x = ix1 + 1;
    }

    if (end_x > ix1 + 1 || end_x <= start_x) {
      end_x = ix1 + 1;
    }

    uint32_t start_y = iy0;
    for (int j = 0; j < segments_y && start_y <= iy1; j++) {
      double max_abs = -INFINITY;
      double max_rel = -INFINITY;
      double max_ulp = -INFINITY;

      float arg_abs[2] = {0.0, 0.0};
      float arg_rel[2] = {0.0, 0.0};
      float arg_ulp[2] = {0.0, 0.0};

      uint32_t end_y;
      if (scale == SCALE_EXP) {
	end_y = start_y + ky;
      }
      else {
	float t = y0 + (j + 1) * step_y;
	end_y = to_ordinal_32(t) + 1;
      }

      if (j == segments_y - 1) {
	end_y = iy1 + 1;
      }

      if (end_y > iy1 + 1 || end_y <= start_y) {
	end_y = iy1 + 1;
      }

      // Random testing
      for (int t = 0; t < samples; t++) {
	uint32_t rx = urand32_range(start_x, end_x);
	uint32_t ry = urand32_range(start_y, end_y);

	float x = from_ordinal_32(rx);
	float y = from_ordinal_32(ry);

	float low = f_low(x, y);
	double high = f_high((double)x, (double)y);

	double abs = abs_error_32(low, high);
	double rel = rel_error_32(low, high);
	double ulp = ulp_error_32(low, high);

	if (abs > max_abs) {
	  max_abs = abs;
	  arg_abs[0] = x;
	  arg_abs[1] = y;
	}

	if (rel > max_rel) {
	  max_rel = rel;
	  arg_rel[0] = x;
	  arg_rel[1] = y;
	}

	if (ulp > max_ulp) {
	  max_ulp = ulp;
	  arg_ulp[0] = x;
	  arg_ulp[1] = y;
	}
      }

      fprintf(out, "%e, %e, %e, %e, %e, %e, %e\n", 
	      from_ordinal_32(start_x), from_ordinal_32(end_x - 1),
	      from_ordinal_32(start_y), from_ordinal_32(end_y - 1),
	      (double)max_abs, (double)max_rel, (double)max_ulp);

      counter++;
      fprintf(stderr, "\r%d/%d         ", counter, total);

      if (max_abs > total_max_abs) {
	total_max_abs = max_abs;
	arg_total_abs[0] = arg_abs[0];
	arg_total_abs[1] = arg_abs[1];
      }

      if (max_rel > total_max_rel) {
	total_max_rel = max_rel;
	arg_total_rel[0] = arg_rel[0];
	arg_total_rel[1] = arg_rel[1];
      }

      if (max_ulp > total_max_ulp) {
	total_max_ulp = max_ulp;
	arg_total_ulp[0] = arg_ulp[0];
	arg_total_ulp[1] = arg_ulp[1];
      }

      start_y = end_y;
    }

    start_x = end_x;
  }

  printf("done\n\n");

  printf("max_abs = %e [%a]\t at (%e, %e) [%a, %a]\n", 
	 (double)total_max_abs, (double)total_max_abs, 
	 arg_total_abs[0], arg_total_abs[1],
  	 arg_total_abs[0], arg_total_abs[1]);
  printf("max_rel = %e [%a]\t at (%e, %e) [%a, %a]\n", 
	 (double)total_max_rel, (double)total_max_rel, 
	 arg_total_rel[0], arg_total_rel[1],
  	 arg_total_rel[0], arg_total_rel[1]);
  printf("max_ulp = %e [%a]\t at (%e, %e) [%a, %a]\n\n", 
	 (double)total_max_ulp, (double)total_max_ulp, 
	 arg_total_ulp[0], arg_total_ulp[1],
	 arg_total_ulp[0], arg_total_ulp[1]);
}


/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values (2d)                                         */
/* -------------------------------------------------------------------------- */

void random_test_64_2d(FILE *out,
		       const F64_2D f_low, const F128_2D f_high,
		       const double x0, const double x1, 
		       const double y0, const double y1,
		       const scale_type scale,
		       const int segments_x, const int segments_y,
		       const int samples)
{
  assert (segments_x > 0 && segments_y > 0);
  assert (samples > 0);
  assert (x1 >= x0 && y1 >= y0);
  assert (f_low && f_high);

  const uint64_t ix0 = to_ordinal_64(x0);
  const uint64_t ix1 = to_ordinal_64(x1);
  const uint64_t iy0 = to_ordinal_64(y0);
  const uint64_t iy1 = to_ordinal_64(y1);
  uint64_t kx = (ix1 - ix0) / segments_x;
  uint64_t ky = (iy1 - iy0) / segments_y;

  const double step_x = (x1 - x0) / segments_x;
  const double step_y = (y1 - y0) / segments_y;

  if (kx <= 0) {
    kx = 1;
  }

  if (ky <= 0) {
    ky = 1;
  }

  quad total_max_abs = FLT128_MIN;
  quad total_max_rel = FLT128_MIN;
  quad total_max_ulp = FLT128_MIN;
  double arg_total_abs[2] = {0.0, 0.0};
  double arg_total_rel[2] = {0.0, 0.0};
  double arg_total_ulp[2] = {0.0, 0.0};

  printf("random_test_64_2d: testing from [%f, %f] to [%f, %f]...\n", 
	 x0, y0, x1, y1);
  printf("  segments = %d, %d\n  samples = %d\n", 
	 segments_x, segments_y, samples);

  if (scale == SCALE_EXP) {
    printf("  exponential scale\n");
  }
  else {
    printf("  uniform scale\n");
  }
  
  fprintf(out, "nx, x0, x1, ny, y0, y1\n");
  fprintf(out, "%d, %e, %e, %d, %e, %e\n", 
	  segments_x, x0, x1,
	  segments_y, y0, y1);

  fprintf(out, "x0, x1, y0, y1, abs, rel, ulp\n");

  int counter = 0;
  const int total = segments_x * segments_y;

  uint64_t start_x = ix0;
  for (int i = 0; i < segments_x && start_x <= ix1; i++) {
    uint64_t end_x;
    if (scale == SCALE_EXP) {
      end_x = start_x + kx;
    }
    else {
      double t = x0 + (i + 1) * step_x;
      end_x = to_ordinal_64(t) + 1;
    }

    if (i == segments_x - 1) {
      end_x = ix1 + 1;
    }

    if (end_x > ix1 + 1 || end_x <= start_x) {
      end_x = ix1 + 1;
    }

    uint64_t start_y = iy0;
    for (int j = 0; j < segments_y && start_y <= iy1; j++) {
      quad max_abs = FLT128_MIN;
      quad max_rel = FLT128_MIN;
      quad max_ulp = FLT128_MIN;

      double arg_abs[2] = {0.0, 0.0};
      double arg_rel[2] = {0.0, 0.0};
      double arg_ulp[2] = {0.0, 0.0};

      uint64_t end_y;
      if (scale == SCALE_EXP) {
	end_y = start_y + ky;
      }
      else {
	double t = y0 + (j + 1) * step_y;
	end_y = to_ordinal_64(t) + 1;
      }

      if (j == segments_y - 1) {
	end_y = iy1 + 1;
      }

      if (end_y > iy1 + 1 || end_y <= start_y) {
	end_y = iy1 + 1;
      }

      // Random testing
      for (int t = 0; t < samples; t++) {
	uint64_t rx = urand64_range(start_x, end_x);
	uint64_t ry = urand64_range(start_y, end_y);

	double x = from_ordinal_64(rx);
	double y = from_ordinal_64(ry);

	double low = f_low(x, y);
	quad high = f_high((quad)x, (quad)y);

	quad abs = abs_error_64(low, high);
	quad rel = rel_error_64(low, high);
	quad ulp = ulp_error_64(low, high);

	if (abs > max_abs) {
	  max_abs = abs;
	  arg_abs[0] = x;
	  arg_abs[1] = y;
	}

	if (rel > max_rel) {
	  max_rel = rel;
	  arg_rel[0] = x;
	  arg_rel[1] = y;
	}

	if (ulp > max_ulp) {
	  max_ulp = ulp;
	  arg_ulp[0] = x;
	  arg_ulp[1] = y;
	}
      }

      fprintf(out, "%e, %e, %e, %e, %e, %e, %e\n", 
	      from_ordinal_64(start_x), from_ordinal_64(end_x - 1),
	      from_ordinal_64(start_y), from_ordinal_64(end_y - 1),
	      (double)max_abs, (double)max_rel, (double)max_ulp);

      counter++;
      fprintf(stderr, "\r%d/%d         ", counter, total);

      if (max_abs > total_max_abs) {
	total_max_abs = max_abs;
	arg_total_abs[0] = arg_abs[0];
	arg_total_abs[1] = arg_abs[1];
      }

      if (max_rel > total_max_rel) {
	total_max_rel = max_rel;
	arg_total_rel[0] = arg_rel[0];
	arg_total_rel[1] = arg_rel[1];
      }

      if (max_ulp > total_max_ulp) {
	total_max_ulp = max_ulp;
	arg_total_ulp[0] = arg_ulp[0];
	arg_total_ulp[1] = arg_ulp[1];
      }

      start_y = end_y;
    }

    start_x = end_x;
  }

  printf("done\n\n");

  printf("max_abs = %e [%a]\t at (%e, %e) [%a, %a]\n", 
	 (double)total_max_abs, (double)total_max_abs, 
	 arg_total_abs[0], arg_total_abs[1],
	 arg_total_abs[0], arg_total_abs[1]);
  printf("max_rel = %e [%a]\t at (%e, %e) [%a, %a]\n", 
	 (double)total_max_rel, (double)total_max_rel, 
	 arg_total_rel[0], arg_total_rel[1],
  	 arg_total_rel[0], arg_total_rel[1]);
  printf("max_ulp = %e [%a]\t at (%e, %e) [%a, %a]\n\n", 
	 (double)total_max_ulp, (double)total_max_ulp, 
	 arg_total_ulp[0], arg_total_ulp[1],
  	 arg_total_ulp[0], arg_total_ulp[1]);
}

/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values (2d) with "real" inputs                      */
/* -------------------------------------------------------------------------- */

void random_test_64_2d_real(FILE *out,
		       const F64_2D f_low, const F128_2D f_high,
		       const double x0, const double x1, 
		       const double y0, const double y1,
		       const scale_type scale,
		       const int segments_x, const int segments_y,
		       const int samples)
{
  assert (segments_x > 0 && segments_y > 0);
  assert (samples > 0);
  assert (x1 >= x0 && y1 >= y0);
  assert (f_low && f_high);

  const uint64_t ix0 = to_ordinal_64(x0);
  const uint64_t ix1 = to_ordinal_64(x1);
  const uint64_t iy0 = to_ordinal_64(y0);
  const uint64_t iy1 = to_ordinal_64(y1);
  uint64_t kx = (ix1 - ix0) / segments_x;
  uint64_t ky = (iy1 - iy0) / segments_y;

  const double step_x = (x1 - x0) / segments_x;
  const double step_y = (y1 - y0) / segments_y;

  if (kx <= 0) {
    kx = 1;
  }

  if (ky <= 0) {
    ky = 1;
  }

  quad total_max_abs = FLT128_MIN;
  quad total_max_rel = FLT128_MIN;
  quad total_max_ulp = FLT128_MIN;
  quad arg_total_abs[2] = {0.0, 0.0};
  quad arg_total_rel[2] = {0.0, 0.0};
  quad arg_total_ulp[2] = {0.0, 0.0};

  printf("random_test_64_2d_real: testing from [%f, %f] to [%f, %f]...\n", 
	 x0, y0, x1, y1);
  printf("  segments = %d, %d\n  samples = %d\n", 
	 segments_x, segments_y, samples);

  if (scale == SCALE_EXP) {
    printf("  exponential scale\n");
  }
  else {
    printf("  uniform scale\n");
  }
  
  fprintf(out, "nx, x0, x1, ny, y0, y1\n");
  fprintf(out, "%d, %e, %e, %d, %e, %e\n", 
	  segments_x, x0, x1,
	  segments_y, y0, y1);

  fprintf(out, "x0, x1, y0, y1, abs, rel, ulp\n");

  int counter = 0;
  const int total = segments_x * segments_y;

  uint64_t start_x = ix0;
  for (int i = 0; i < segments_x && start_x <= ix1; i++) {
    uint64_t end_x;
    if (scale == SCALE_EXP) {
      end_x = start_x + kx;
    }
    else {
      double t = x0 + (i + 1) * step_x;
      end_x = to_ordinal_64(t) + 1;
    }

    if (i == segments_x - 1) {
      end_x = ix1 + 1;
    }

    if (end_x > ix1 + 1 || end_x <= start_x) {
      end_x = ix1 + 1;
    }

    uint64_t start_y = iy0;
    for (int j = 0; j < segments_y && start_y <= iy1; j++) {
      quad max_abs = FLT128_MIN;
      quad max_rel = FLT128_MIN;
      quad max_ulp = FLT128_MIN;

      quad arg_abs[2] = {0.0, 0.0};
      quad arg_rel[2] = {0.0, 0.0};
      quad arg_ulp[2] = {0.0, 0.0};

      uint64_t end_y;
      if (scale == SCALE_EXP) {
	end_y = start_y + ky;
      }
      else {
	double t = y0 + (j + 1) * step_y;
	end_y = to_ordinal_64(t) + 1;
      }

      if (j == segments_y - 1) {
	end_y = iy1 + 1;
      }

      if (end_y > iy1 + 1 || end_y <= start_y) {
	end_y = iy1 + 1;
      }

      quad start_val_x = (quad)from_ordinal_64(start_x);
      quad end_val_x = (quad)from_ordinal_64(end_x);
      quad start_val_y = (quad)from_ordinal_64(start_y);
      quad end_val_y = (quad)from_ordinal_64(end_y);

      // Random testing
      for (int t = 0; t < samples; t++) {
	quad x = rand_quad_range(start_val_x, end_val_x);
	quad y = rand_quad_range(start_val_y, end_val_y);

	double low = f_low((double)x, (double)y);
	quad high = f_high(x, y);

	quad abs = abs_error_64(low, high);
	quad rel = rel_error_64(low, high);
	quad ulp = ulp_error_64(low, high);

	if (abs > max_abs) {
	  max_abs = abs;
	  arg_abs[0] = x;
	  arg_abs[1] = y;
	}

	if (rel > max_rel) {
	  max_rel = rel;
	  arg_rel[0] = x;
	  arg_rel[1] = y;
	}

	if (ulp > max_ulp) {
	  max_ulp = ulp;
	  arg_ulp[0] = x;
	  arg_ulp[1] = y;
	}
      }

      fprintf(out, "%e, %e, %e, %e, %e, %e, %e\n", 
	      from_ordinal_64(start_x), from_ordinal_64(end_x - 1),
	      from_ordinal_64(start_y), from_ordinal_64(end_y - 1),
	      (double)max_abs, (double)max_rel, (double)max_ulp);

      counter++;
      fprintf(stderr, "\r%d/%d         ", counter, total);

      if (max_abs > total_max_abs) {
	total_max_abs = max_abs;
	arg_total_abs[0] = arg_abs[0];
	arg_total_abs[1] = arg_abs[1];
      }

      if (max_rel > total_max_rel) {
	total_max_rel = max_rel;
	arg_total_rel[0] = arg_rel[0];
	arg_total_rel[1] = arg_rel[1];
      }

      if (max_ulp > total_max_ulp) {
	total_max_ulp = max_ulp;
	arg_total_ulp[0] = arg_ulp[0];
	arg_total_ulp[1] = arg_ulp[1];
      }

      start_y = end_y;
    }

    start_x = end_x;
  }

  printf("done\n\n");

  char buf1[1000], buf2[1000];

  quadmath_snprintf(buf1, sizeof(buf1), "%Qa", arg_total_abs[0]);
  quadmath_snprintf(buf2, sizeof(buf2), "%Qa", arg_total_abs[1]);
  printf("max_abs = %e [%a]\t at (%e, %e) [%s, %s]\n", 
	 (double)total_max_abs, (double)total_max_abs, 
	 (double)arg_total_abs[0], (double)arg_total_abs[1],
  	 buf1, buf2);
  
  quadmath_snprintf(buf1, sizeof(buf1), "%Qa", arg_total_rel[0]);
  quadmath_snprintf(buf2, sizeof(buf2), "%Qa", arg_total_rel[1]);
  printf("max_rel = %e [%a]\t at (%e, %e) [%s, %s]\n", 
	 (double)total_max_rel, (double)total_max_rel, 
	 (double)arg_total_rel[0], (double)arg_total_rel[1],
  	 buf1, buf2);
  
  quadmath_snprintf(buf1, sizeof(buf1), "%Qa", arg_total_ulp[0]);
  quadmath_snprintf(buf2, sizeof(buf2), "%Qa", arg_total_ulp[1]);
  printf("max_ulp = %e [%a]\t at (%e, %e) [%s, %s]\n\n", 
	 (double)total_max_ulp, (double)total_max_ulp, 
	 (double)arg_total_ulp[0], (double)arg_total_ulp[1],
  	 buf1, buf2);
}



/* -------------------------------------------------------------------------- */
/* main                                                                       */
/* -------------------------------------------------------------------------- */

const char *argp_program_version =
  "bound 0.2";
const char *argp_program_bug_address =
  "<monad@cs.utah.edu>";

// Documentation
static char doc[] =
"bound -- a program for testing error bounds of simple\
 floating-point functions.";

// Arguments (documentation)
static char args_doc[] = 
"function_name (exp, log, sin, cos, sqrt, kahan_exp, kahan_exp_log,\
 kahan_exp_series, sqrt_sub, sqrt_add, sub, hypot, jet)";

// Options
static struct argp_option options[] = {
  {"out",      'o',  "OUTFILE", 0,
   "Output to OUTFILE (default data.txt)" },
  {"double",   'd', 0,    0,
   "Double precision mode (random sampling)" },
  {"real",     'r', 0,    0,
   "Inputs are assumed to be real numbers" },
  {"scale",    'c', "exp,uniform",    0,
   "Defines the scale type (default is exp)" },
  {"start",    'a', "A",  0, 
   "Start point of the test interval [A,B] (A is a pair x,y for 2d functions)" },
  {"end",      'b', "B",  0, 
   "End point of the test interval [A,B] (B is a pair x,y for 2d functions)" },
  {"segments", 'n', "N",  0, 
   "Number(N) of segments in which the interval [A,B] is divided \
(N is a pair Nx,Ny for 2d functions)" },
  {"samples",  's', "S", 0,
   "Number(S) of samples for random values in each segment" },
  { 0 }
};

typedef struct {
  // functions
  F32 f_32;
  F64 f_64;
  F128 f_128;

  F32_2D f_32_2d;
  F64_2D f_64_2d;
  F128_2D f_128_2d;

  // double precision flag
  bool double_precision_flag;
  bool real_input_flag;
  scale_type scale;

  // interval
  double a[2], b[2];
  int segments[2];
  int samples;

  // output
  char *outfile;
} arguments;

void parse_double_pair(const char *str, double pair[2])
{
  char *ptr = strchr(str, ',');

  if (ptr == NULL) {
    pair[0] = atof(str);
    pair[1] = 0.0;
  }
  else {
    char tmp[100];
    pair[1] = atof(ptr + 1);

    int pos = ptr - str;
    if (pos < 100) {
      memcpy(tmp, str, pos);
      tmp[pos] = '\0';
      pair[0] = atof(tmp);
    }
    else {
      pair[0] = 0.0;
    }
  }
}

void parse_int_pair(const char *str, int pair[2])
{
  char *ptr = strchr(str, ',');

  if (ptr == NULL) {
    pair[0] = atoi(str);
    pair[1] = 0;
  }
  else {
    char tmp[100];
    pair[1] = atoi(ptr + 1);

    int pos = ptr - str;
    if (pos < 100) {
      memcpy(tmp, str, pos);
      tmp[pos] = '\0';
      pair[0] = atoi(tmp);
    }
    else {
      pair[0] = 0;
    }
  }
}

void find_scale(char *name, arguments *args)
{
  if (strcmp(name, "exp") == 0) {
    args->scale = SCALE_EXP;
  }
  else if (strcmp(name, "uniform") == 0) {
    args->scale = SCALE_UNIFORM;
  }
  else {
    fprintf(stderr, "Bad scale name: %s\n", name);
  }
}

// Sets function arguments
void find_function(char *name, arguments *args)
{
  if (strcmp(name, "exp") == 0) {
    args->f_32 = exp_32;
    args->f_64 = exp_64;
    args->f_128 = exp_128;
  }
  else if (strcmp(name, "log") == 0) {
    args->f_32 = log_32;
    args->f_64 = log_64;
    args->f_128 = log_128;
  }
  else if (strcmp(name, "sin") == 0) {
    args->f_32 = sin_32;
    args->f_64 = sin_64;
    args->f_128 = sin_128;
  }
  else if (strcmp(name, "cos") == 0) {
    args->f_32 = cos_32;
    args->f_64 = cos_64;
    args->f_128 = cos_128;
  }
  else if (strcmp(name, "add1") == 0) {
    args->f_32 = add1_32;
    args->f_64 = add1_64;
    args->f_128 = add1_128;
  }
  else if (strcmp(name, "add11") == 0) {
    args->f_32 = add11_32;
    args->f_64 = add11_64;
    args->f_128 = add11_128;
  }
  else if (strcmp(name, "nonlin1") == 0) {
    args->f_32 = nonlin1_32;
    args->f_64 = nonlin1_64;
    args->f_128 = nonlin1_128;
  }
  else if (strcmp(name, "sqroot") == 0) {
    args->f_32 = sqroot_32;
    args->f_64 = sqroot_64;
    args->f_128 = sqroot_128;
  }
  else if (strcmp(name, "predPrey") == 0) {
    args->f_32 = predPrey_32;
    args->f_64 = predPrey_64;
    args->f_128 = predPrey_128;
  }
  else if (strcmp(name, "sine3") == 0) {
    args->f_32 = sine3_32;
    args->f_64 = sine3_64;
    args->f_128 = sine3_128;
  }
  else if (strcmp(name, "sine3_term1") == 0) {
    args->f_32 = sine3_term1_32;
    args->f_64 = sine3_term1_64;
    args->f_128 = sine3_term1_128;
  }
  else if (strcmp(name, "sine3_term2") == 0) {
    args->f_32 = sine3_term2_32;
    args->f_64 = sine3_term2_64;
    args->f_128 = sine3_term2_128;
  }
  else if (strcmp(name, "sqrt") == 0) {
    args->f_32 = sqrt_32;
    args->f_64 = sqrt_64;
    args->f_128 = sqrt_128;
  }
  else if (strcmp(name, "sqrt_1") == 0) {
    args->f_32 = sqrt_1_32;
    args->f_64 = sqrt_1_64;
    args->f_128 = sqrt_1_128;
  }
  else if (strcmp(name, "kahan_exp") == 0) {
    args->f_32 = kahan_exp_32;
    args->f_64 = kahan_exp_64;
    args->f_128 = kahan_exp_128;
  }
  else if (strcmp(name, "kahan_exp_log") == 0) {
    args->f_32 = kahan_exp_log_32;
    args->f_64 = kahan_exp_log_64;
    args->f_128 = kahan_exp_log_128;
  }
  else if (strcmp(name, "kahan_exp_series") == 0) {
    args->f_32 = kahan_exp_series_3_32;
    args->f_64 = kahan_exp_64;
    fprintf(stderr, "Comparing series_3_32 with kahan_exp_64\n");
  }
  else if (strcmp(name, "kahan_exp_series_4") == 0) {
    args->f_32 = kahan_exp_series_4_32;
    args->f_64 = kahan_exp_64;
    fprintf(stderr, "Comparing series_4_32 with kahan_exp_64\n");
  }
  else if (strcmp(name, "kahan_exp_series_3_cmp") == 0) {
    args->f_32 = kahan_exp_series_3_32;
    args->f_64 = kahan_exp_series_3_64;
    args->f_128 = kahan_exp_series_3_128;
  }
  else if (strcmp(name, "kahan_exp_series_4_cmp") == 0) {
    args->f_32 = kahan_exp_series_4_32;
    args->f_64 = kahan_exp_series_4_64;
    args->f_128 = kahan_exp_series_4_128;
  }
  else if (strcmp(name, "k_sin1") == 0) {
    args->f_32 = NULL;
    args->f_64 = k_sin1_64;
    args->f_128 = k_sin1_128;
  }
  else if (strcmp(name, "k_log1pf") == 0) {
    args->f_32 = k_log1pf_32;
    args->f_64 = k_log1pf_64;
    args->f_128 = NULL;
  }
  else if (strcmp(name, "sub_sub") == 0) {
    args->f_32 = sub_sub_32;
    args->f_64 = sub_sub_64;
    args->f_128 = sub_sub_128;
  }
  else if (strcmp(name, "sqrt_sub") == 0) {
    args->f_32 = sqrt_sub_32;
    args->f_64 = sqrt_sub_64;
    args->f_128 = sqrt_sub_128;
  }
  else if (strcmp(name, "sqrt_sub11") == 0) {
    args->f_32 = sqrt_sub11_32;
    args->f_64 = sqrt_sub11_64;
    args->f_128 = sqrt_sub11_128;
  }
  else if (strcmp(name, "sqrt_add") == 0) {
    args->f_32 = sqrt_add_32;
    args->f_64 = sqrt_add_64;
    args->f_128 = sqrt_add_128;
  }
  else if (strcmp(name, "pid1") == 0) {
    args->f_32_2d = pid1_32;
    args->f_64_2d = pid1_64;
  }
  else if (strcmp(name, "pid2") == 0) {
    args->f_32_2d = pid2_32;
    args->f_64_2d = pid2_64;
  }
  else if (strcmp(name, "sub") == 0) {
    args->f_32_2d = sub_32;
    args->f_64_2d = sub_64;
    args->f_128_2d = sub_128;
  }
  else if (strcmp(name, "hypot") == 0) {
    args->f_32_2d = hypot_32;
    args->f_64_2d = hypot_64;
    args->f_128_2d = hypot_128;
  }
  else if (strcmp(name, "jet") == 0) {
    args->f_32_2d = jet_32;
    args->f_64_2d = jet_64;
    args->f_128_2d = jet_128;
  }
  else if (strcmp(name, "px4_1") == 0) {
    args->f_32 = px4_1_32;
    args->f_64 = px4_1_64;
  }
  else if (strcmp(name, "px4_1_math") == 0) {
    args->f_32 = px4_1_32;
    args->f_64 = px4_1_math_64;
  }
  else if (strcmp(name, "px4_1_int") == 0) {
    args->f_32 = px4_1_int_32;
    args->f_64 = px4_1_int_64;
  }
  else if (strcmp(name, "px4_1_const") == 0) {
    args->f_32 = px4_1_const_32;
    args->f_64 = px4_1_const_64;
  }
  else if (strcmp(name, "px4_1_const_int") == 0) {
    args->f_32 = px4_1_const_int_32;
    args->f_64 = px4_1_const_int_64;
  }
  else if (strcmp(name, "a11") == 0) {
    args->f_32_2d = a11_32;
    args->f_64_2d = a11_64;
    args->f_128_2d = a11_128;
  }
  else if (strcmp(name, "a12") == 0) {
    args->f_32 = a12_32;
    args->f_64 = a12_64;
    args->f_128 = a12_128;
  }
  else if (strcmp(name, "a13") == 0) {
    args->f_32 = a13_32;
    args->f_64 = a13_64;
    args->f_128 = a13_128;
  }
  else if (strcmp(name, "a14") == 0) {
    args->f_32 = a14_32;
    args->f_64 = a14_64;
    args->f_128 = a14_128;
  }
  else if (strcmp(name, "a15") == 0) {
    args->f_32 = a15_32;
    args->f_64 = a15_64;
    args->f_128 = a15_128;
  }
  else if (strcmp(name, "a16") == 0) {
    args->f_32 = a16_32;
    args->f_64 = a16_64;
    args->f_128 = a16_128;
  }
  else if (strcmp(name, "a17") == 0) {
    args->f_32 = a17_32;
    args->f_64 = a17_64;
    args->f_128 = a17_128;
  }
  else if (strcmp(name, "a18") == 0) {
    args->f_32 = a18_32;
    args->f_64 = a18_64;
    args->f_128 = a18_128;
  }
  else if (strcmp(name, "a19") == 0) {
    args->f_32 = a19_32;
    args->f_64 = a19_64;
    args->f_128 = a19_128;
  }
  else if (strcmp(name, "a20") == 0) {
    args->f_32 = a20_32;
    args->f_64 = a20_64;
    args->f_128 = a20_128;
  }
  else {
    fprintf(stderr, "Undefined function: %s\n", name);
  }
}

// Parses a single options
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
  arguments *args = state->input;

  switch (key) {
    // output
  case 'o':
    args->outfile = arg;
    break;

    // double precision
  case 'd':
    args->double_precision_flag = true;
    break;
    
    // real inputs
  case 'r':
    args->real_input_flag = true;
    break;

    // scale type
  case 'c':
    find_scale(arg, args);
    break;

    // start
  case 'a':
    parse_double_pair(arg, args->a);
    break;

    // end
  case 'b':
    parse_double_pair(arg, args->b);
    break;

    // segments
  case 'n':
    parse_int_pair(arg, args->segments);
    break;

  case 's':
    args->samples = atoi(arg);
    break;

  case ARGP_KEY_NO_ARGS:
    argp_usage(state);

    // function
  case ARGP_KEY_ARG:
    find_function(arg, args);
    state->next = state->argc;
    break;

  default:
    return ARGP_ERR_UNKNOWN;
  }

  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

void test(float x)
{
  uint32_t r = to_raw_ordinal_32(x);
  uint32_t i = to_ordinal_32(x);
  printf("%f: (raw = %u (%X), ord = %u (%X))\n", x, r, r, i, i);

  float y = from_raw_ordinal_32(r);
  float z = from_ordinal_32(i);
  printf("%f, %f\n", y, z);
}

void test2(float x)
{
  float u = ulp_32(x);
  printf("%a: %a (%f)\n", x, u, u);
}

void test_64(double x)
{
  uint64_t r = to_raw_ordinal_64(x);
  uint64_t i = to_ordinal_64(x);
  printf("%f: (raw = %ju (%jx), ord = %ju (%jx))\n", 
	 x, (uintmax_t)r, (uintmax_t)r, 
	 (uintmax_t)i, (uintmax_t)i);

  double y = from_raw_ordinal_64(r);
  double z = from_ordinal_64(i);
  printf("%f, %f\n", y, z);
}

void test2_64(double x)
{
  double u = ulp_64(x);
  printf("%a: %a (%f)\n", x, u, u);
}



int main(int argc, char *argv[])
{
  // Make sure that all types have correct sizes
  assert (sizeof(uint32_t) == 4);
  assert (sizeof(uint64_t) == 8);
  assert (sizeof(float) == 4);
  assert (sizeof(double) == 8);

  arguments args = { 0 };

  // Default arguments
  args.f_32 = NULL;
  args.f_64 = NULL;
  args.f_128 = NULL;
  args.f_32_2d = NULL;
  args.f_64_2d = NULL;
  args.f_128_2d = NULL;
  args.a[0] = args.a[1] = 1.0f;
  args.b[0] = args.b[1] = 2.0f;
  args.double_precision_flag = false;
  args.real_input_flag = false;
  args.scale = SCALE_EXP;
  args.segments[0] = args.segments[1] = 100;
  args.samples = 10000;
  args.outfile = "data.txt";

  // Parse arguments
  argp_parse(&argp, argc, argv, 0, 0, &args);

  bool flag2d = false;
  if (args.f_32_2d || args.f_64_2d || args.f_128_2d) {
    flag2d = true;
  }

  // Validate arguments
  if (flag2d) {
    // 2d
    if (args.double_precision_flag) {
      if (!args.f_64_2d || !args.f_128_2d) {
	fprintf(stderr, "Undefined 2d test function(s) (64 or 128 bits)\n");
	exit(1);
      }
    }
    else {
      if (!args.f_32_2d || !args.f_64_2d) {
	fprintf(stderr, "Undefined 2d test function(s) (32 or 64 bits)\n");
	exit(1);
      }
    }

    if (args.a[0] > args.b[0] || args.a[1] > args.b[1]) {
      fprintf(stderr, "Bad input interval(s): x = [%e, %e], y = [%e, %e]\n", 
	      args.a[0], args.b[0], args.a[1], args.b[1]);
      exit(2);
    }

    if (args.segments[0] < 1 || args.segments[1] < 1) {
      fprintf(stderr, "Bad number of segments: %d, %d\n", 
	      args.segments[0], args.segments[1]);
      exit(2);
    }
  }
  else {
    // 1d
    if (args.double_precision_flag) {
      if (!args.f_64 || !args.f_128) {
	fprintf(stderr, "Undefined test function(s) (64 or 128 bits)\n");
	exit(1);
      }
    }
    else {
      if (!args.f_32 || !args.f_64) {
	fprintf(stderr, "Undefined test function(s) (32 or 64 bits)\n");
	exit(1);
      }
    }

    if (args.a[0] > args.b[0]) {
      fprintf(stderr, "Bad input interval: [%e, %e]\n", args.a[0], args.b[0]);
      exit(2);
    }

    if (args.segments[0] < 1) {
      fprintf(stderr, "Bad number of segments: %d\n", args.segments[0]);
      exit(2);
    }
  }

  if (args.samples < 1) {
    fprintf(stderr, "Bad number of samples: %d\n", args.samples);
    exit(2);
  }

  FILE *out = fopen(args.outfile, "w");
  if (!out) {
    fprintf(stderr, "Bad output file: %s\n", args.outfile);
    exit(3);
  }

  // Run the test
  if (flag2d) {
    // 2d
    if (args.double_precision_flag) {
      if (args.real_input_flag) {
	// "real" inputs
	random_test_64_2d_real(out,
			  args.f_64_2d, args.f_128_2d,
			  args.a[0], args.b[0], args.a[1], args.b[1], args.scale,
			  args.segments[0], args.segments[1], args.samples);
      }
      else {
	// double inputs
	random_test_64_2d(out,
			  args.f_64_2d, args.f_128_2d,
			  args.a[0], args.b[0], args.a[1], args.b[1], args.scale,
			  args.segments[0], args.segments[1], args.samples);
      }
    }
    else {
      random_test_32_2d(out,
			args.f_32_2d, args.f_64_2d,
			args.a[0], args.b[0], args.a[1], args.b[1], args.scale,
			args.segments[0], args.segments[1], args.samples);
    }
  }
  else {
    // 1d
    if (args.double_precision_flag) {
      if (args.real_input_flag) {
	// "real" inputs
	random_test_64_real(out, 
		       args.f_64, args.f_128, 
		       args.a[0], args.b[0], args.scale,
		       args.segments[0], args.samples);
      }
      else {
	// double inputs
	random_test_64(out, 
		       args.f_64, args.f_128, 
		       args.a[0], args.b[0], args.scale,
		       args.segments[0], args.samples);
      }
    }
    else {
      exhaustive_test_32(out, 
			 args.f_32, args.f_64, 
			 (float)args.a[0], (float)args.b[0], args.scale,
			 args.segments[0]);
    }
  }

  fclose(out);

  return 0;
}

