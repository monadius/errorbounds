#include "search.h"

double low = 0;
double high = 999;

float f_32(float t)
{
    float r1 = t + 1.0f;
    float r2 = t / r1;
    return r2;
}

double f_64(double t)
{
    double r1 = t + 1.0;
    double r2 = t / r1;
    return r2;
}

real f_real(real t)
{
    real r1 = t + (real)1.0;
    real r2 = t / r1;
    return r2;
}