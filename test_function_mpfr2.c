#include "search_mpfr.h"

double low[] = {0.9};
double high[] = {1.0};
const char *f_name = "Test mpfr error data (2)";

static mpfr_t _r1;
static float f_c1;
static double d_c1;
static mpfr_t c1;

void f_init()
{
    // Init all constants and temporary variables here
    mpfr_init(_r1);
    mpfr_init(c1);
    init_constants("1/10", MPFR_RNDN, &f_c1, &d_c1, c1);
}

void f_clear()
{
    // Clear all constants and temporary variables here
    mpfr_clear(_r1);
    mpfr_clear(c1);
}

float f_32(float t)
{
    float r1 = t * f_c1;
    return r1;
}

double f_64(double t)
{
    double r1 = t * d_c1;
    return r1;
}

void f_high(mpfr_t rop, mpfr_t op)
{
    mpfr_mul(_r1, op, c1, MPFR_RNDN);
    mpfr_set(rop, _r1, MPFR_RNDN);
}