#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "search_mpfi.h"
#include <mpfi_io.h>

/* -------------------------------------------------------------------------- */
/* Copied from 
   http://stackoverflow.com/questions/20175148/64bit-random-number-between-a-range */
/* -------------------------------------------------------------------------- */

// TODO: a better random number generator is required
uint64_t urand64()
{
    uint64_t hi = rand();
    uint64_t md = rand();
    uint64_t lo = rand();
    return (hi << 42) + (md << 21) + lo;
}

int is_power_of_2(uint64_t x)
{
    return x == (x & -x);
}

uint64_t unsigned_uniform_random(uint64_t low, uint64_t high)
{
    static const uint64_t M = ~(uint64_t)0;
    uint64_t range = high - low;
    uint64_t to_exclude = is_power_of_2(range) ? 0
                                               : M % range + 1;
    uint64_t res;
    // Eliminate 'to_exclude' possible values from consideration.
    while ((res = urand64()) < to_exclude)
    {
    }
    return low + res % range;
}

// Requires b > a
uint64_t urand64_range(uint64_t a, uint64_t b)
{
    uint64_t range = b - a;
    return a + urand64() % range;
}

uint32_t urand32()
{
    uint32_t hi = rand() & 0xFFFF;
    uint32_t lo = rand() & 0xFFFF;
    return (hi << 16) + lo;
}

// Requires b > a
uint32_t urand32_range(uint32_t a, uint32_t b)
{
    uint32_t range = b - a;
    return a + urand32() % range;
}

// TODO: a better random number generator is required
// real rand_real()
// {
//     return (real)rand() / REAL_RAND_MAX;
// }

// real rand_real_range(real a, real b)
// {
//     return a + rand_real() * (b - a);
// }
inline void rand_range_mpfr(gmp_randstate_t state, mpfr_ptr rop, mpfr_srcptr a, mpfr_srcptr d)
{
    mpfr_urandom(rop, state, MPFR_RNDN);
    mpfr_mul(rop, rop, d, MPFR_RNDN);
    mpfr_add(rop, rop, a, MPFR_RNDN);
}

/* -------------------------------------------------------------------------- */
/* Float <-> ordinal conversions (32 bits)                                    */
/* (Strictly speaking, not portable due to the strict aliasing rule)          */
/* -------------------------------------------------------------------------- */

typedef union {
    float f;
    uint32_t i;
} Float32;

// Converts a floating-point number to the corresponding raw ordinal
// (does not preserve order between positive and negative numbers)
uint32_t to_raw_ordinal_32(float f)
{
    Float32 v = {.f = f};
    return v.i;
}

// Converts a floating-point number to the corresponding ordinal
uint32_t to_ordinal_32(float f)
{
    Float32 v = {.f = f};
    uint32_t i = v.i;

    if (i & 0x80000000)
    {
        return 0xFFFFFFFF - i;
    }
    else
    {
        return i | 0x80000000;
    }
}

// Converts a raw ordinal to the corresponding floating-point number
float from_raw_ordinal_32(uint32_t i)
{
    Float32 v = {.i = i};
    return v.f;
}

// Converts an ordinal to the corresponding floating-point number
float from_ordinal_32(uint32_t i)
{
    if (i & 0x80000000)
    {
        Float32 v = {.i = i & ~0x80000000};
        return v.f;
    }
    else
    {
        Float32 v = {.i = 0xFFFFFFFF - i};
        return v.f;
    }
}

// ULP
float ulp_32(float x)
{
    uint32_t r;
    Float32 v = {.f = x};

    uint32_t exp = (v.i >> 23) & 0xFF;

    if (exp > 23)
    {
        r = (exp - 23) << 23;
    }
    else if (exp == 0)
    {
        return 0x1p-149f;
    }
    else
    {
        uint32_t t = exp - 1;
        r = 1 << t;
    }

    Float32 out = {.i = r};
    return out.f;
}

/* -------------------------------------------------------------------------- */
/* Float <-> ordinal conversions (64 bits)                                    */
/* (Strictly speaking, not portable due to the strict aliasing rule)          */
/* -------------------------------------------------------------------------- */

typedef union {
    double f;
    uint64_t i;
} Float64;

// Converts a floating-point number to the corresponding raw ordinal
// (does not preserve order between positive and negative numbers)
uint64_t to_raw_ordinal_64(double f)
{
    Float64 v = {.f = f};
    return v.i;
}

// Converts a floating-point number to the corresponding ordinal
uint64_t to_ordinal_64(double f)
{
    Float64 v = {.f = f};
    uint64_t i = v.i;

    if (i & 0x8000000000000000ull)
    {
        return 0xFFFFFFFFFFFFFFFFull - i;
    }
    else
    {
        return i | 0x8000000000000000ull;
    }
}

// Converts a raw ordinal to the corresponding floating-point number
double from_raw_ordinal_64(uint64_t i)
{
    Float64 v = {.i = i};
    return v.f;
}

// Converts an ordinal to the corresponding floating-point number
double from_ordinal_64(uint64_t i)
{
    if (i & 0x8000000000000000ull)
    {
        Float64 v = {.i = i & ~0x8000000000000000ull};
        return v.f;
    }
    else
    {
        Float64 v = {.i = 0xFFFFFFFFFFFFFFFFull - i};
        return v.f;
    }
}

// ULP
double ulp_64(double x)
{
    uint64_t r;
    Float64 v = {.f = x};

    uint64_t exp = (v.i >> 52) & 0x7FF;

    if (exp > 52)
    {
        r = (exp - 52) << 52;
    }
    else if (exp == 0)
    {
        return 0x1p-1074;
    }
    else
    {
        uint64_t t = exp - 1;
        //    printf("t = %d\n", (int)t);
        r = 1ull << t;
        //printf("r = %lld\n", (long long)r);
    }

    Float64 out = {.i = r};
    return out.f;
}

/* -------------------------------------------------------------------------- */
/* Error computation                                                          */
/* -------------------------------------------------------------------------- */

// Absolute error
inline int abs_error_mpfi(mpfi_ptr rop, double low, mpfi_srcptr high)
{
    int t = mpfi_sub_d(rop, high, low);
    mpfi_abs(rop, rop);
    return t;
}

// Relative error
inline int rel_error_mpfi(mpfi_ptr rop, double low, mpfi_srcptr high)
{
    int t = mpfi_sub_d(rop, high, low);

    if (mpfi_is_zero(rop) <= 0)
    {
        // non-zero rop
        mpfi_div(rop, rop, high);
    }

    mpfi_abs(rop, rop);
    return t;
}

// Goldberg's ULP-64 error MPFI
inline int goldberg_ulp64_error_mpfi(mpfi_ptr rop, double low, mpfi_srcptr high)
{
    int t = mpfi_sub_d(rop, high, low);

    if (mpfi_is_zero(rop) <= 0)
    {
        // non-zero rop
        mpfi_div_d(rop, rop, ulp_64(low));
    }

    mpfi_abs(rop, rop);
    return t;
}

// ULP-64 error MPFR
inline int ulp64_error_mpfi(mpfi_ptr rop, double low, mpfi_srcptr high)
{
    // TODO: implement
    return goldberg_ulp64_error_mpfi(rop, low, high);
    // int t = mpfi_sub_d(rop, high, low);

    // if (mpfr_zero_p(rop) == 0)
    // {
    //     // non-zero rop
    //     mpfr_exp_t k = -1074;
    //     if (mpfr_zero_p(high) == 0)
    //     {
    //         // non-zero high
    //         mpfr_exp_t e = mpfr_get_exp(high) - 1;
    //         k = (e >= -1022 ? e : -1022) - 52;
    //     }

    //     mpfr_set_exp(rop, mpfr_get_exp(rop) - k);
    // }

    // mpfi_abs(rop, rop);
    // return t;
}

// Goldberg's ULP-32 error MPFI
inline int goldberg_ulp32_error_mpfi(mpfi_ptr rop, float low, mpfi_srcptr high)
{
    int t = mpfi_sub_d(rop, high, (double)low);

    if (mpfi_is_zero(rop) <= 0)
    {
        // non-zero rop
        mpfi_div_d(rop, rop, (double)ulp_32(low));
    }

    mpfi_abs(rop, rop);
    return t;
}

// ULP-32 error MPFR
inline int ulp32_error_mpfi(mpfi_ptr rop, float low, mpfi_srcptr high)
{
    // TODO: implement
    return goldberg_ulp32_error_mpfi(rop, low, high);
}

inline void combine_errors_mpfi(mpfi_ptr rop, mpfi_srcptr op1, mpfi_srcptr op2)
{
    mpfr_max(&(rop->left), &(op1->left), &(op2->left), MPFR_RNDD);
    mpfr_max(&(rop->right), &(op1->right), &(op2->right), MPFR_RNDU);
}

/* -------------------------------------------------------------------------- */
/* Exhaustive test for 32-bit values                                          */
/* -------------------------------------------------------------------------- */

void exhaustive_test_32_mpfi(FILE *out, const F_32 f_32, const F_MPFI f_high,
                             const search_args args)
{
    const float a = (float)args.a;
    const float b = (float)args.b;

    assert(args.segments > 0);
    assert(b >= a);
    assert(f_32 && f_high);

    const uint32_t ia = to_ordinal_32(a);
    const uint32_t ib = to_ordinal_32(b);
    const float step = (b - a) / args.segments;
    uint32_t k = (ib - ia) / args.segments;

    if (k == 0)
    {
        k = 1;
    }

    mpfi_t f_op, f_rop;
    mpfi_t r_abs, max_abs, total_max_abs;
    mpfi_t r_rel, max_rel, total_max_rel;
    mpfi_t r_ulp, max_ulp, total_max_ulp;
    mpfi_init(f_op); mpfi_init(f_rop);
    mpfi_init(r_abs); mpfi_init(max_abs); mpfi_init(total_max_abs);
    mpfi_init(r_rel); mpfi_init(max_rel); mpfi_init(total_max_rel);
    mpfi_init(r_ulp); mpfi_init(max_ulp); mpfi_init(total_max_ulp);

    mpfi_set_d(total_max_abs, 0);
    mpfi_set_d(total_max_rel, 0);
    mpfi_set_d(total_max_ulp, 0);

    printf("MPFI exhaustive_test_32: testing from %f to %f...\n", a, b);
    printf("  segments = %d\n", args.segments);

    if (args.scale == SCALE_EXP)
    {
        printf("  exponential scale\n");
    }
    else
    {
        printf("  uniform scale\n");
    }

    uint32_t start = ia;

    for (int i = 0; i < args.segments && start <= ib; i++)
    {
        mpfi_set_d(max_abs, 0);
        mpfi_set_d(max_rel, 0);
        mpfi_set_d(max_ulp, 0);

        // TODO: make sure that no overflows occur
        uint32_t end;
        if (args.scale == SCALE_EXP)
        {
            end = start + k;
        }
        else
        {
            float t = a + (i + 1) * step;
            end = to_ordinal_32(t) + 1;
        }

        if (i == args.segments - 1)
        {
            end = ib + 1;
        }

        if (end > ib + 1 || end <= start)
        {
            end = ib + 1;
        }

        for (uint32_t p = start; p < end; p++)
        {
            float x = from_ordinal_32(p);
            float low = f_32(x);
            mpfi_set_d(f_op, x);
            f_high(f_rop, f_op);

            abs_error_mpfi(r_abs, low, f_rop);
            rel_error_mpfi(r_rel, low, f_rop);
            ulp32_error_mpfi(r_ulp, low, f_rop);

            combine_errors_mpfi(max_abs, max_abs, r_abs);
            combine_errors_mpfi(max_rel, max_rel, r_rel);
            combine_errors_mpfi(max_ulp, max_ulp, r_ulp);
        }

        fprintf(out, "%d, %e, %e, ", i + 1, 
                from_ordinal_32(start), from_ordinal_32(end - 1));
        mpfi_out_str(out, 10, 10, max_abs);
        fprintf(out, ", ");
        mpfi_out_str(out, 10, 10, max_rel);
        fprintf(out, ", ");
        mpfi_out_str(out, 10, 10, max_ulp);
        fprintf(out, "\n");

        fprintf(stderr, "\r%d/%d         ", i + 1, args.segments);

        combine_errors_mpfi(total_max_abs, total_max_abs, max_abs);
        combine_errors_mpfi(total_max_rel, total_max_rel, max_rel);
        combine_errors_mpfi(total_max_ulp, total_max_ulp, max_ulp);

        start = end;
    }

    printf("done\n\n");

    mpfr_printf("max_abs = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_abs->left), &(total_max_abs->right),
                &(total_max_abs->left));
    mpfr_printf("max_rel = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_rel->left), &(total_max_rel->right),
                &(total_max_rel->left));
    mpfr_printf("max_ulp = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_ulp->left), &(total_max_ulp->right),
                &(total_max_ulp->left));

    mpfi_clear(f_op); mpfi_clear(f_rop);
    mpfi_clear(r_abs); mpfi_clear(max_abs); mpfi_clear(total_max_abs);
    mpfi_clear(r_rel); mpfi_clear(max_rel); mpfi_clear(total_max_rel);
    mpfi_clear(r_ulp); mpfi_clear(max_ulp); mpfi_clear(total_max_ulp);
}

/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values                                              */
/* -------------------------------------------------------------------------- */

void random_test_64(FILE *out, const F_64 f_64, const F_MPFI f_high,
                    const search_args args)
{
    const double a = args.a;
    const double b = args.b;

    assert(args.segments > 0);
    assert(args.samples > 0);
    assert(b >= a);
    assert(f_64 && f_high);

    const uint64_t ia = to_ordinal_64(a);
    const uint64_t ib = to_ordinal_64(b);
    uint64_t k = (ib - ia) / args.segments;

    const double step = (b - a) / args.segments;

    if (k == 0)
    {
        k = 1;
    }

    const int exhaustive_flag = (k <= args.samples);

    mpfi_t f_op, f_rop;
    mpfi_t r_abs, max_abs, total_max_abs;
    mpfi_t r_rel, max_rel, total_max_rel;
    mpfi_t r_ulp, max_ulp, total_max_ulp;
    mpfi_init(f_op); mpfi_init(f_rop);
    mpfi_init(r_abs); mpfi_init(max_abs); mpfi_init(total_max_abs);
    mpfi_init(r_rel); mpfi_init(max_rel); mpfi_init(total_max_rel);
    mpfi_init(r_ulp); mpfi_init(max_ulp); mpfi_init(total_max_ulp);

    mpfi_set_d(total_max_abs, 0);
    mpfi_set_d(total_max_rel, 0);
    mpfi_set_d(total_max_ulp, 0);

    printf("MPFI random_test_64: testing from %f to %f...\n", a, b);
    printf("  segments = %d\n  samples = %d\n", args.segments, args.samples);

    if (exhaustive_flag)
    {
        printf(" performing an exhaustive search\n");
    }

    if (args.scale == SCALE_EXP)
    {
        printf("  exponential scale\n");
    }
    else
    {
        printf("  uniform scale\n");
    }

    uint64_t start = ia;

    for (int i = 0; i < args.segments && start <= ib; i++)
    {
        mpfi_set_d(max_abs, 0);
        mpfi_set_d(max_rel, 0);
        mpfi_set_d(max_ulp, 0);

        // TODO: make sure that no overflows occur
        uint64_t end;
        if (args.scale == SCALE_EXP)
        {
            end = start + k;
        }
        else
        {
            double t = a + (i + 1) * step;
            end = to_ordinal_64(t) + 1;
        }

        if (i == args.segments - 1)
        {
            end = ib + 1;
        }

        if (end > ib + 1 || end <= start)
        {
            end = ib + 1;
        }

        if (exhaustive_flag)
        {
            // Exhaustive search
            for (uint64_t p = start; p < end; p++)
            {
                double x = from_ordinal_64(p);
                double low = f_64(x);
                mpfi_set_d(f_op, x);
                f_high(f_rop, f_op);

                abs_error_mpfi(r_abs, low, f_rop);
                rel_error_mpfi(r_rel, low, f_rop);
                ulp64_error_mpfi(r_ulp, low, f_rop);

                combine_errors_mpfi(max_abs, max_abs, r_abs);
                combine_errors_mpfi(max_rel, max_rel, r_rel);
                combine_errors_mpfi(max_ulp, max_ulp, r_ulp);
            }
        }
        else
        {
            // Random testing
            for (int t = 0; t < args.samples; t++)
            {
                uint64_t r = urand64_range(start, end);

                double x = from_ordinal_64(r);
                double low = f_64(x);
                mpfi_set_d(f_op, x);
                f_high(f_rop, f_op);

                abs_error_mpfi(r_abs, low, f_rop);
                rel_error_mpfi(r_rel, low, f_rop);
                ulp64_error_mpfi(r_ulp, low, f_rop);

                combine_errors_mpfi(max_abs, max_abs, r_abs);
                combine_errors_mpfi(max_rel, max_rel, r_rel);
                combine_errors_mpfi(max_ulp, max_ulp, r_ulp);
            }
        }

        fprintf(out, "%d, %e, %e, ", i + 1, 
                from_ordinal_64(start), from_ordinal_64(end - 1));
        mpfi_out_str(out, 10, 10, max_abs);
        fprintf(out, ", ");
        mpfi_out_str(out, 10, 10, max_rel);
        fprintf(out, ", ");
        mpfi_out_str(out, 10, 10, max_ulp);
        fprintf(out, "\n");

        fprintf(stderr, "\r%d/%d         ", i + 1, args.segments);

        combine_errors_mpfi(total_max_abs, total_max_abs, max_abs);
        combine_errors_mpfi(total_max_rel, total_max_rel, max_rel);
        combine_errors_mpfi(total_max_ulp, total_max_ulp, max_ulp);

        start = end;
    }

    printf("done\n\n");

    mpfr_printf("max_abs = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_abs->left), &(total_max_abs->right),
                &(total_max_abs->left));
    mpfr_printf("max_rel = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_rel->left), &(total_max_rel->right),
                &(total_max_rel->left));
    mpfr_printf("max_ulp = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_ulp->left), &(total_max_ulp->right),
                &(total_max_ulp->left));

    mpfi_clear(f_op); mpfi_clear(f_rop);
    mpfi_clear(r_abs); mpfi_clear(max_abs); mpfi_clear(total_max_abs);
    mpfi_clear(r_rel); mpfi_clear(max_rel); mpfi_clear(total_max_rel);
    mpfi_clear(r_ulp); mpfi_clear(max_ulp); mpfi_clear(total_max_ulp);
}


/* -------------------------------------------------------------------------- */
/* Random test for 64-bit values with "real" input arguments                  */
/* -------------------------------------------------------------------------- */

void random_test_64_real(FILE *out, const F_64 f_64, const F_MPFI f_high,
                         const search_args args)
{
    const double a = args.a;
    const double b = args.b;

    assert(args.segments > 0);
    assert(args.samples > 0);
    assert(b >= a);
    assert(f_64 && f_high);

    const uint64_t ia = to_ordinal_64(a);
    const uint64_t ib = to_ordinal_64(b);
    uint64_t k = (ib - ia) / args.segments;

    const double step = (b - a) / args.segments;

    if (k == 0)
    {
        k = 1;
    }

    gmp_randstate_t r_state;
    mpfr_t op, start_val, diff_val;

    gmp_randinit_default(r_state);
    mpfr_inits(op, start_val, diff_val, NULL);

    mpfi_t f_op, f_rop;
    mpfi_t r_abs, max_abs, total_max_abs;
    mpfi_t r_rel, max_rel, total_max_rel;
    mpfi_t r_ulp, max_ulp, total_max_ulp;
    mpfi_init(f_op); mpfi_init(f_rop);
    mpfi_init(r_abs); mpfi_init(max_abs); mpfi_init(total_max_abs);
    mpfi_init(r_rel); mpfi_init(max_rel); mpfi_init(total_max_rel);
    mpfi_init(r_ulp); mpfi_init(max_ulp); mpfi_init(total_max_ulp);

    mpfi_set_d(total_max_abs, 0);
    mpfi_set_d(total_max_rel, 0);
    mpfi_set_d(total_max_ulp, 0);

    printf("MPFI random_test_64_real: testing from %f to %f...\n", a, b);
    printf("  segments = %d\n  samples = %d\n", args.segments, args.samples);

    if (args.scale == SCALE_EXP)
    {
        printf("  exponential scale\n");
    }
    else
    {
        printf("  uniform scale\n");
    }

    uint64_t start = ia;

    for (int i = 0; i < args.segments && start <= ib; i++)
    {
        mpfi_set_d(max_abs, 0);
        mpfi_set_d(max_rel, 0);
        mpfi_set_d(max_ulp, 0);

        // TODO: make sure that no overflows occur
        uint64_t end;
        if (args.scale == SCALE_EXP)
        {
            end = start + k;
        }
        else
        {
            double t = a + (i + 1) * step;
            end = to_ordinal_64(t) + 1;
        }

        if (i == args.segments - 1)
        {
            end = ib + 1;
        }

        if (end > ib + 1 || end <= start)
        {
            end = ib + 1;
        }

        // Random testing
        mpfr_set_d(start_val, from_ordinal_64(start), MPFR_RNDU);
        mpfr_d_sub(diff_val, from_ordinal_64(end), start_val, MPFR_RNDN);

        for (int t = 0; t < args.samples; t++)
        {
            rand_range_mpfr(r_state, op, start_val, diff_val);

            double low = f_64(mpfr_get_d(op, MPFR_RNDN));
            mpfi_set_fr(f_op, op);
            f_high(f_rop, f_op);

            abs_error_mpfi(r_abs, low, f_rop);
            rel_error_mpfi(r_rel, low, f_rop);
            ulp64_error_mpfi(r_ulp, low, f_rop);

            combine_errors_mpfi(max_abs, max_abs, r_abs);
            combine_errors_mpfi(max_rel, max_rel, r_rel);
            combine_errors_mpfi(max_ulp, max_ulp, r_ulp);
        }

        fprintf(out, "%d, %e, %e, ", i + 1, 
                from_ordinal_64(start), from_ordinal_64(end - 1));
        mpfi_out_str(out, 10, 10, max_abs);
        fprintf(out, ", ");
        mpfi_out_str(out, 10, 10, max_rel);
        fprintf(out, ", ");
        mpfi_out_str(out, 10, 10, max_ulp);
        fprintf(out, "\n");

        fprintf(stderr, "\r%d/%d         ", i + 1, args.segments);

        combine_errors_mpfi(total_max_abs, total_max_abs, max_abs);
        combine_errors_mpfi(total_max_rel, total_max_rel, max_rel);
        combine_errors_mpfi(total_max_ulp, total_max_ulp, max_ulp);

        start = end;
    }

    printf("done\n\n");

    mpfr_printf("max_abs = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_abs->left), &(total_max_abs->right),
                &(total_max_abs->left));
    mpfr_printf("max_rel = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_rel->left), &(total_max_rel->right),
                &(total_max_rel->left));
    mpfr_printf("max_ulp = [%.5RDe, %.5RUe] {low = %Ra}\n",
                &(total_max_ulp->left), &(total_max_ulp->right),
                &(total_max_ulp->left));

    gmp_randclear(r_state);
    mpfr_clears(op, start_val, diff_val, NULL);
    mpfi_clear(f_op); mpfi_clear(f_rop);
    mpfi_clear(r_abs); mpfi_clear(max_abs); mpfi_clear(total_max_abs);
    mpfi_clear(r_rel); mpfi_clear(max_rel); mpfi_clear(total_max_rel);
    mpfi_clear(r_ulp); mpfi_clear(max_ulp); mpfi_clear(total_max_ulp);
}