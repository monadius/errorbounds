#include "mp_common.h"

void init_constants(const char *rat_const_str,
                    mpfr_rnd_t rnd,
                    float *float_const, 
                    double *double_const,
                    mpfr_ptr mpfr_const,
                    mpfi_ptr mpfi_const)
{
    mpfr_exp_t old_emin, old_emax;

    old_emin = mpfr_get_emin();
    old_emax = mpfr_get_emax();

    mpq_t q;
    mpq_init(q);
    mpq_set_str(q, rat_const_str, 10);
    mpq_canonicalize(q);

    // mpfr
    if (mpfr_const) {
        mpfr_set_q(mpfr_const, q, rnd);
    }

    // mpfi
    if (mpfi_const) {   
        mpfi_set_q(mpfi_const, q);
    }

    // double
    if (double_const) {
        MPFR_DECL_INIT(d_tmp, 53);
        mpfr_set_emin(-1073);
        mpfr_set_emax(1024);

        int t = mpfr_set_q(d_tmp, q, rnd);
        mpfr_subnormalize(d_tmp, t, rnd);
        *double_const = mpfr_get_d(d_tmp, rnd);
    }

    // single
    if (float_const) {
        MPFR_DECL_INIT(s_tmp, 24);
        mpfr_set_emin(-148);
        mpfr_set_emax(128);

        int t = mpfr_set_q(s_tmp, q, rnd);
        mpfr_subnormalize(s_tmp, t, rnd);
        *float_const = mpfr_get_flt(s_tmp, rnd);
    }

    // clean up
    mpq_clear(q);
    mpfr_set_emin(old_emin);
    mpfr_set_emax(old_emax);
}
