#include "func.h"

int mpfr_floor_power2(mpfr_ptr rop, mpfr_srcptr op)
{
    if (!mpfr_regular_p(op)) {
        return mpfr_set(rop, op, MPFR_RNDN);
    }

    int one = (mpfr_sgn(op) >= 0 ? 1 : -1);
    mpfr_exp_t e = mpfr_get_exp(op);

    int t = mpfr_set_si_2exp(rop, one, e - 1, MPFR_RNDN);

    if (mpfr_cmp(rop, op) == 0) {
        t = mpfr_div_2ui(rop, rop, 1, MPFR_RNDN);
    }

    return t;
}

int mpfr_goldberg_ulp(mpfr_ptr rop, int prec, int e_min, mpfr_srcptr op)
{
    if (!mpfr_regular_p(op)) {
        return mpfr_set(rop, op, MPFR_RNDN);
    }

    int one = (mpfr_sgn(op) >= 0 ? 1 : -1);
    mpfr_exp_t e = mpfr_get_exp(op);

    e_min += 1;
    e = (e >= e_min ? e : e_min) - prec;

    return mpfr_set_si_2exp(rop, one, e, MPFR_RNDN);
}

int mpfr_sub2(mpfr_ptr rop, mpfr_srcptr op1, mpfr_srcptr op2, mpfr_rnd_t rnd)
{
    if (mpfr_sgn(op2) >= 0) {
        mpfr_div_2ui(rop, op1, 1, rnd);
        if (mpfr_cmp(rop, op2) > 0) {
            return mpfr_sub(rop, op1, op2, rnd);
        }

        mpfr_mul_2ui(rop, op1, 1, rnd);
        if (mpfr_cmp(op2, rop) > 0) {
            return mpfr_sub(rop, op1, op2, rnd);
        }
    }
    else {
        mpfr_mul_2ui(rop, op1, 1, rnd);
        if (mpfr_cmp(rop, op2) > 0) {
            return mpfr_sub(rop, op1, op2, rnd);
        }

        mpfr_div_2ui(rop, op1, 1, rnd);
        if (mpfr_cmp(op2, rop) > 0) {
            return mpfr_sub(rop, op1, op2, rnd);
        }
    }

    mpfr_set_zero(rop, 1);
    return 0;
}

void mpfi_floor_power2(mpfi_ptr rop, mpfi_srcptr op)
{
    mpfr_floor_power2(&(rop->left), &(op->left));
    mpfr_floor_power2(&(rop->right), &(op->right));
}

void mpfi_goldberg_ulp(mpfi_ptr rop, int prec, int e_min, mpfi_srcptr op)
{
    mpfr_goldberg_ulp(&(rop->left), prec, e_min, &(op->left));
    mpfr_goldberg_ulp(&(rop->right), prec, e_min, &(op->right));
}

void mpfi_max(mpfi_ptr rop, mpfi_srcptr op1, mpfi_srcptr op2)
{
    mpfr_max(&(rop->left), &(op1->left), &(op2->left), MPFR_RNDD);
    mpfr_max(&(rop->right), &(op1->right), &(op2->right), MPFR_RNDU);
}

void mpfi_min(mpfi_ptr rop, mpfi_srcptr op1, mpfi_srcptr op2)
{
    mpfr_min(&(rop->left), &(op1->left), &(op2->left), MPFR_RNDD);
    mpfr_min(&(rop->right), &(op1->right), &(op2->right), MPFR_RNDU);
}

void mpfi_sub2(mpfi_ptr rop, mpfi_srcptr op1, mpfi_srcptr op2)
{
    mpfr_sub2(&(rop->left), &(op1->left), &(op2->right), MPFR_RNDD);
    mpfr_sub2(&(rop->right), &(op1->right), &(op2->left), MPFR_RNDU);

    /* do not allow -0 as lower bound */
    if (mpfr_zero_p(&(rop->left)) && mpfr_signbit(&(rop->left))) {
        mpfr_neg(&(rop->left), &(rop->left), MPFR_RNDU);
    }
    /* do not allow +0 as upper bound */
    if (mpfr_zero_p(&(rop->right)) && !mpfr_signbit(&(rop->right))) {
        mpfr_neg(&(rop->right), &(rop->right), MPFR_RNDD);
    }
}

void mpfi_pow_ui(mpfi_ptr rop, mpfi_srcptr op1, unsigned long int op2)
{
    if ((op2 & 1) || mpfr_sgn(&(op1->left)) >= 0) {
        mpfr_pow_ui(&(rop->left), &(op1->left), op2, MPFR_RNDD);
        mpfr_pow_ui(&(rop->right), &(op1->right), op2, MPFR_RNDU);
    }
    else if (mpfr_sgn(&(op1->right)) <= 0) {
        // Need to be careful when rop = op1
        mpfr_t tmp;
        mpfr_init2(tmp, mpfr_get_prec(&(op1->right)));
        mpfr_pow_ui(tmp, &(op1->left), op2, MPFR_RNDU);
        mpfr_pow_ui(&(rop->left), &(op1->right), op2, MPFR_RNDD);
        mpfr_set(&(rop->right), tmp, MPFR_RNDU);
    }
    else {
        if (mpfr_cmp_abs(&(op1->left), &(op1->right)) <= 0) {
            mpfr_pow_ui(&(rop->right), &(op1->right), op2, MPFR_RNDU);
        }
        else {
            mpfr_pow_ui(&(rop->right), &(op1->left), op2, MPFR_RNDU);
        }
        mpfr_set_si(&(rop->left), 0L, MPFR_RNDD);
    }

    /* do not allow -0 as lower bound */
    if (mpfr_zero_p(&(rop->left)) && mpfr_signbit(&(rop->left))) {
        mpfr_neg(&(rop->left), &(rop->left), MPFR_RNDU);
    }
    /* do not allow +0 as upper bound */
    if (mpfr_zero_p(&(rop->right)) && !mpfr_signbit(&(rop->right))) {
        mpfr_neg(&(rop->right), &(rop->right), MPFR_RNDD);
    }
}