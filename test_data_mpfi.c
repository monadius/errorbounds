#include "data_mpfi.h"
#include "func.h"

double low[] = {-4.0};
double high[] = {4.0};

static mpfi_t _r1;
static mpfi_t c1;

void f_init()
{
    // Init all constants and temporary variables here
    mpfi_init(_r1);
    mpfi_init(c1);
    init_constants("1/10", MPFR_RNDN, NULL, NULL, c1);
}

void f_clear()
{
    // Clear all constants and temporary variables here
    mpfi_clear(_r1);
    mpfi_clear(c1);
}

void f_high(mpfi_ptr rop, mpfi_srcptr op)
{
    mpfi_sin(_r1, op);
    mpfi_mul(_r1, _r1, _r1);
    mpfi_add_d(_r1, _r1, 0.01);
    mpfi_inv(_r1, _r1);
    mpfi_mul(rop, _r1, c1);
}

void f_high2(mpfi_ptr rop, mpfi_srcptr op)
{
    mpfi_set(rop, c1);
}

char *f_names[] = {"test", "c1"};
int n_funcs = 2;
F_HIGH funcs[] = {f_high, f_high2};

