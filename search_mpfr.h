#ifndef __SEARCH_MPFR__
#define __SEARCH_MPFR__

#include "mp_common.h"

typedef enum {
  SCALE_UNIFORM,
  SCALE_EXP
} scale_type;

typedef struct t_search_args {
    double a, b;
    scale_type scale;
    int segments;
    int samples;
} search_args;

void exhaustive_test_32(FILE *out, 
                        const F_32 f_32, 
                        const F_64 f_64,
                        const search_args args);

void random_test_64(FILE *out, 
                    const F_64 f_64, 
                    const F_MPFR f_high,
                    const search_args args);

void random_test_64_mpfr(FILE *out, 
                         const F_MPFR f_low, 
                         const F_MPFR f_high,
                         const search_args args);

void random_test_64_real(FILE *out, 
                         const F_64 f_64, 
                         const F_MPFR f_high,
                         const search_args args);

void random_test_64_real_mpfr(FILE *out, 
                              const F_MPFR f_low, 
                              const F_MPFR f_high,
                              const search_args args);

#endif